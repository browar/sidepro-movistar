﻿using Sidepro.Entities;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace Sidepro.Data
{
    public class CanalData
    {
        public List<Canal> getCanales(){
            List<Canal> miLista = new List<Canal>();
            Canal miCanal;


            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_canales_ABM");
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miCanal = new Canal();
                    miCanal.Codigo = Convert.ToInt32(miDataReader["Codigo"]);
                    miCanal.Descripcion = miDataReader["Descripcion"].ToString();
                    miLista.Add(miCanal);
                }
            }

            miConn.Close();
            return miLista;
        }
        public string agregarCanal(string Descripcion)
        {

            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[1];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_desc";
                miParametro.Value = Descripcion;
                misParametros[0] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_canal", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miDataReader.Close();
                miConn.Close();
                

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }
        public string modificarCanal(int cod_canal, string Descripcion)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_canal";
                miParametro.Value = cod_canal;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_desc";
                miParametro.Value = Descripcion;
                misParametros[1] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_modificar_canal", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miDataReader.Close();                
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }

    }
}