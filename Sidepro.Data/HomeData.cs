﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using System.Data.OleDb;
namespace Sidepro.Data
{
    public class HomeData
    {
        public Usuario getUser(string user, string pass)
        {
            Usuario miUsuario = new Usuario();
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[2];
            OleDbParameter miParametro = new OleDbParameter();

            miParametro.OleDbType = OleDbType.VarChar;
            miParametro.ParameterName = "@txt_user";
            miParametro.Value = user;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.VarChar;
            miParametro.ParameterName = "@txt_pass";
            miParametro.Value = pass;
            misParametros[1] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_usuario", misParametros);

            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                miUsuario.Id_usuario = Convert.ToInt32(miDataReader["Usuario"]);
                miUsuario.Apellido = miDataReader["Apellido"].ToString();
                miUsuario.Nombre = miDataReader["Nombre"].ToString();
                miUsuario.URL_imagen = miDataReader["URL_Imagen"].ToString();
                miUsuario.Cod_tipo_usuario = Convert.ToInt32(miDataReader["cod_tipo_usuario"]);
                miUsuario.Mensaje = miDataReader["Mensaje"].ToString();
                miUsuario.Id_agencia = Convert.ToInt32( miDataReader["ID_Agencia"]);
                
            }
            miDataReader.Close();
            miConn.Close();
            return miUsuario;
        }
        public List<Certification> getCertificaciones(int id_usuario)
        {
            List<Certification> miLista = new List<Certification>();
            Certification miCertification;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();

            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;
            
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_certificaciones_usuario", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read()) {
                    miCertification = new Certification();

                    miCertification.Id_certification = Convert.ToInt32(miDataReader["Certification"]);
                    miCertification.Nombre = miDataReader["Nombre"].ToString();
                    miCertification.Nota = Convert.ToInt32(miDataReader["Nota"]);
                    miLista.Add(miCertification);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public List<Destacado> getDestacados(int id_usuario)
        {
            List<Destacado> miLista = new List<Destacado>();
            Destacado miDestacado;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();

            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_destacados", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miDestacado = new Destacado();
                    
                    miDestacado.TipoDestacado = Convert.ToInt32(miDataReader["TipoDestacado"]);
                    miDestacado.Orden = Convert.ToInt32(miDataReader["TipoDestacado"]);
                    miDestacado.Imagen = miDataReader["Imagen"].ToString();
                    miDestacado.URL = miDataReader["URL"].ToString();
                    miDestacado.Titulo = miDataReader["Titulo"].ToString();
                    miDestacado.Descripcion = miDataReader["Descripcion"].ToString();
                    miLista.Add(miDestacado);

                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
    }
}