﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Net.Mail;
using System.Net;
namespace Sidepro.Data.Helpers
{
    public class HelpersData
    {
        public static OleDbDataReader getDataReader(OleDbConnection miConn, string query)
        {
            OleDbCommand miCommand = new OleDbCommand();
            OleDbDataReader miDataReader;

            miCommand.Connection = miConn;
            miCommand.CommandType = CommandType.StoredProcedure;
            miCommand.CommandText = query;
            miCommand.CommandTimeout = 120;
            miDataReader = miCommand.ExecuteReader();
            return miDataReader;
        }

        public static OleDbDataReader getDataReader(OleDbConnection miConn, string query, OleDbParameter[] misParametros) {
            OleDbCommand miCommand = new OleDbCommand();
            OleDbDataReader miDataReader;

            miCommand.Connection = miConn;
            miCommand.CommandType = CommandType.StoredProcedure;
            miCommand.CommandText = query;
            miCommand.CommandTimeout = 120;
            
            foreach (OleDbParameter miParam in misParametros)
                miCommand.Parameters.Add(miParam);
            
            miDataReader = miCommand.ExecuteReader();
            return miDataReader;
        }
        public static OleDbConnection getConnection() {
            OleDbConnection miConexion = new OleDbConnection();
            miConexion.ConnectionString = ConfigurationManager.ConnectionStrings["Sidepro-Movistar"].ConnectionString;
            miConexion.Open();
            return miConexion;
         
        }
        public bool enviarMail(int id_usuario, string From, string To, string ReplyTo, string Subject, string Message)
        {

            try {
                string pepe = SendUserMail(From, To, Message, From, Subject);
                OleDbConnection miConn = getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_usuario";
                miParametro.Value = id_usuario;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_consulta";
                miParametro.Value = Message;
                misParametros[1] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_enviar_consulta", misParametros);
                miDataReader.Close();
                miConn.Close();

                return true;
            }catch{
                return false;
            }
            
        }
        public string saveFile(int id_modulo, int id_curso, int id_leccion, int tipo_archivo, string nombre)
        {
            string resultado = "";
            try
            {
                OleDbConnection miConn = getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[5];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@tipo_archivo";
                miParametro.Value = tipo_archivo;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@original_file_name";
                miParametro.Value = nombre;
                misParametros[4] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_subir_archivo", misParametros);

                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    resultado = miDataReader["fileName"].ToString();
                }
                
                miDataReader.Close();
                miConn.Close();

                return resultado;
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }        
        public string SendUserMail(string fromad, string toad, string body, string header, string subjectcontent)
        {
            string result = "";
            //MailMessage usermail = Mailbodplain(fromad, toad, body, header, subjectcontent);
            //SmtpClient client = new SmtpClient();
            //Add the Creddentials- use your own email id and password
            //client.Credentials = new System.Net.NetworkCredential("noreplysidepro@gmail.com", "Agus221#"); ;

            //client.Host = "smtp.gmail.com";// "s184-168-147-58.secureserver.net";// "smtp.gmail.com";
            //client.Port = 25;
            //client.EnableSsl = true;
            try
            {
                MailAddress to = new MailAddress(toad); // new MailAddress(Console.ReadLine());
                MailAddress from = new MailAddress("no-reply@sidepro.com.ar");//new MailAddress("noreplysidepro@gmail.com");
                MailMessage mail = new MailMessage(from, to);
                mail.Subject = "Consulta";
                mail.Body = body;//"Testeo de Mails puerto 25 9.36 intento unico";// Console.ReadLine();
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "s184-168-147-58.secureserver.net";
                smtp.Port = 25;
                smtp.Credentials = new NetworkCredential("no-reply@sidepro.com.ar", "Agus221#");
                smtp.EnableSsl = false;
                smtp.Send(mail);

                result = "todoOk";
            }
            catch (Exception ex)
            {
                result = ex.Message;
            } // end try


            return result;

        }
        public MailMessage Mailbodplain(string fromad, string toad, string body, string header, string subjectcontent)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            try
            {
                string from = fromad;
                string to = toad;
                mail.To.Add(to);
                mail.From = new MailAddress(from, header, System.Text.Encoding.UTF8);
                mail.Subject = subjectcontent;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;
                mail.Body = body;
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.IsBodyHtml = true;
                mail.Priority = MailPriority.High;
            }
            catch (Exception ex)
            {
                throw;
            }
            return mail;
        }

        public string enviarMailGmail(string to, string title, string body)
        {

            using (MailMessage mailMessage =
            new MailMessage(new MailAddress(@"no-reply@amedia.com.ar"),
            new MailAddress(@"no-reply@amedia.com.ar")))
            {
                mailMessage.Body = "body";
                mailMessage.Subject = "subject";
                try
                {
                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                    SmtpServer.Credentials =
                        new System.Net.NetworkCredential(@"no-reply@amedia.com.ar", "sotelo21");
                    SmtpServer.Port = 587;
                    SmtpServer.Host = "smtp.gmail.com";
                    SmtpServer.EnableSsl = true;
                    var mail = new MailMessage();
                    String[] addr = to.Split(','); // toemail is a string which contains many email address separated by comma
                    mail.From = new MailAddress(@"no-reply@amedia.com.ar");
                    Byte i;
                    for (i = 0; i < addr.Length; i++)
                        mail.To.Add(addr[i]);
                    mail.Subject = title;
                    mail.Body = body;// "cuerpo browar";
                    mail.IsBodyHtml = true;
                    mail.DeliveryNotificationOptions =
                        DeliveryNotificationOptions.OnFailure;
                    //   mail.ReplyTo = new MailAddress(toemail);
                    mail.ReplyToList.Add(@"no-reply@amedia.com.ar");
                    SmtpServer.Send(mail);
                    return "";
                }
                catch (Exception ex)
                {
                    string exp = ex.ToString();

                    //Console.WriteLine("Mail Not Sent ... and ther error is " + exp);
                    return exp;
                }
            }
        }
    }
}