﻿using Sidepro.Entities;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace Sidepro.Data
{
    public class NoticiaData
    {
        public Noticia GetNoticia(int id_modulo,int id_curso, int cod_usuario)
        {
            Noticia miNoticia = new Noticia();

            OleDbParameter[] misParametros = new OleDbParameter[3];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = cod_usuario;
            misParametros[2] = miParametro;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_noticia", misParametros);

            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                
                miNoticia.FileName= miDataReader["FileName"].ToString();
                miNoticia.Path = miDataReader["Path"].ToString();
            }
            return miNoticia;
        }
        public List<ReporteNovedades> GetReporteNovedades(string usuario)
        {
            List<ReporteNovedades> miLista = new List<ReporteNovedades>();
            ReporteNovedades miReporte;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.VarChar;
            miParametro.ParameterName = "@txt_user";
            miParametro.Value = usuario;
            misParametros[0] = miParametro;
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_reporte_agenda_novedades", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read()) {
                    miReporte = new ReporteNovedades();
                    miReporte.Nombre = miDataReader["Nombre"].ToString();
                    miReporte.Apellido = miDataReader["Apellido"].ToString();
                    miReporte.Novedad = miDataReader["Novedad"].ToString();
                    miReporte.Descargado = miDataReader["Descargado"].ToString();
                    miReporte.Aprobado = miDataReader["Aprobado"].ToString();
                    miReporte.Reprobados = Convert.ToInt32(miDataReader["Reprobados"]);


                    miLista.Add(miReporte);
                }
            }
            return miLista;
        }
        
    }
}