﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using System.Data.OleDb;

namespace Sidepro.Data
{
    public class CursosData
    {
        public Curso get(int id_curso){

            Curso miCurso = new Curso();
            miCurso.Id_curso = id_curso;
            miCurso.Nombre = "Curso";
            miCurso.Descripcion = "Curso " + id_curso.ToString() +" de testeo";
            miCurso.Sn_Activo = -1;

            return miCurso;

        }
        public List<Curso> getCursosDeGrupo(int id_grupo) {
            List<Curso> miLista = new List<Curso>();

            miLista.Add(get(1));
            miLista.Add(get(2));
            return miLista;        
        }
        public List<Curso> getCursosDelHistorial(int id_usuario) {


            List<Curso> miLista = new List<Curso>();

            Curso miCurso;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_historial_usuario", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
               
                    miCurso = new Curso();
                    miCurso.Nombre_Modulo = miDataReader["NombreModulo"].ToString();
                    miCurso.Nombre = miDataReader["NombreCurso"].ToString();
                    miCurso.Id_curso = Convert.ToInt32(miDataReader["Curso"]);
                    miCurso.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miCurso.Controller = miDataReader["Controller"].ToString();
                    miCurso.View = miDataReader["View"].ToString();

                    miCurso.Style = @"history__item history__item--mod-inicial";
                    miCurso.Sn_Activo = -1;
                    miLista.Add(miCurso);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
            /*
            Curso miCurso = new Curso();
            miCurso.Id_curso = 1;
            miCurso.Nombre_Modulo = "Módulo Inicial";

            miCurso.URL = "";
            miCurso.Style = @"history__item history__item--mod-inicial";
            miCurso.Nombre = "Planes";
            miCurso.View = "Module";
            miCurso.Controller = "Module";
            miCurso.Descripcion = "Planes Vigentes - test";
            miCurso.URLImagen = "";
            miCurso.Sn_Activo = -1;

            miLista.Add(miCurso);

            miCurso = new Curso();
            miCurso.Id_curso = 2;
            miCurso.Nombre_Modulo = "Equipo";
            miCurso.URL = @"Home/Course";
            miCurso.Controller = "Course";
            miCurso.View = "Course";
            miCurso.Style = @"history__item history__item--equipos";
            miCurso.Nombre = "Camara";
            miCurso.Descripcion = "Samsung Galaxy S6 - test";
            miCurso.URLImagen = "";
            miCurso.Sn_Activo = -1;

            miLista.Add(miCurso);

            miCurso = new Curso();
            miCurso.Id_curso = 2;
            miCurso.Nombre_Modulo = "OFERTA Y CAMPAÑA";
            miCurso.URL = @"Home/Course";
            miCurso.Controller = "Course";
            miCurso.View = "Course";
            miCurso.Style = @"history__item history__item--oferta";
            miCurso.Nombre = "Campaña permanente";
            miCurso.Descripcion = "Campaña permanente - test";
            miCurso.URLImagen = "";
            miCurso.Sn_Activo = -1;

            miLista.Add(miCurso);


            return miLista;
            */

        }
       public CourseProgress getProgresoReportes(int id_usuario)
       {


            CourseProgress miProgreso = new CourseProgress(); ;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_progress", misParametros);

            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                miProgreso.ModuloInicial = Convert.ToDecimal(miDataReader["ProgresoInicial"]);
                miProgreso.Equipos = Convert.ToDecimal(miDataReader["ProgresoEquipos"]);
                miProgreso.OfertaYCampana = Convert.ToDecimal(miDataReader["ProgresoOferta"]);
                miProgreso.Certificacion = Convert.ToDecimal(miDataReader["ProgresoCertificacion"]);
                miProgreso.Total = Convert.ToDecimal(miDataReader["ProgresoTotal"]);
                miProgreso.CantidadUsuarios = Convert.ToDecimal(miDataReader["CantidadUsuarios"]);
            }
            miDataReader.Close();
            miConn.Close();
            return miProgreso;
        }


        public CourseProgress getProgreso (int id_usuario)
        {


            CourseProgress miProgreso = new CourseProgress();;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_user_progress", misParametros);

            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                miProgreso.ModuloInicial = Convert.ToDecimal(miDataReader["ProgresoInicial"]);
                miProgreso.Equipos = Convert.ToDecimal(miDataReader["ProgresoEquipos"]);
                miProgreso.OfertaYCampana = Convert.ToDecimal(miDataReader["ProgresoOferta"]);
                miProgreso.Certificacion = Convert.ToDecimal(miDataReader["ProgresoCertificacion"]);
                miProgreso.Total = Convert.ToDecimal(miDataReader["ProgresoTotal"]);
            }
            miDataReader.Close();
            miConn.Close();
            return miProgreso;
        }
        public List<Curso> getCursosDelModulo(int id_modulo, int? id_curso, int id_usuario)
        {
            List<Curso> miLista = new List<Curso>();

            Curso miCurso = new Curso();
            
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[3];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;


            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[2] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cursos_del_modulo", misParametros);
            
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miCurso = new Curso();
                    miCurso.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miCurso.Id_curso = Convert.ToInt32(miDataReader["Curso"]);
                    miCurso.Nombre = miDataReader["Nombre"].ToString();
                    miCurso.Nombre_Modulo = miDataReader["NombreModulo"].ToString();
                    miCurso.Descripcion = miDataReader["Descripcion"].ToString();
                    miCurso.Controller = miDataReader["Controller"].ToString();
                    miCurso.View = miDataReader["View"].ToString();
                    miCurso.Style = miDataReader["Style"].ToString();
                    miCurso.URL = "";
                    miCurso.URLImagen = miDataReader["URL_imagen"].ToString();
                    miCurso.Sn_Activo = Convert.ToInt32(miDataReader["sn_Activo"]);
                    miCurso.PuedeRealizar = Convert.ToInt32(miDataReader["PuedeRealizar"]);
                    miLista.Add(miCurso);
                }

            }
            miDataReader.Close();
            miConn.Close();
            /*
            miCurso.Id_curso = 1;
            miCurso.Nombre_Modulo = "Módulo Inicial";
            miCurso.URLImagen = @"../Images/placeholders/s6.jpg";
            miCurso.Style = @"course__pic";
            miCurso.Nombre = "Samsung S6";
            miCurso.View = "Course";
            miCurso.Controller = "Course";
            miCurso.Descripcion = "Cámara, Características Generales, Diseño y Pantalla, Seguridad, Tips para el vendedor.";
            miCurso.Sn_Activo = -2;

            miLista.Add(miCurso);

            miCurso = new Curso();
            miCurso.Id_curso = 2;
            miCurso.Nombre_Modulo = "Equipo";
            miCurso.URLImagen = @"../Images/placeholders/700x500.png";
            miCurso.URL = @"Home/";
            miCurso.Controller = "Course";
            miCurso.View = "Course";
            miCurso.Style = @"course__pic";
            miCurso.Nombre = "Camara";
            miCurso.Descripcion = "Samsung Galaxy S6 - test";
            miCurso.Sn_Activo = -1;

            miLista.Add(miCurso);

            miCurso = new Curso();
            miCurso.Id_curso = 2;
            miCurso.Nombre_Modulo = "OFERTA Y CAMPAÑA";
            miCurso.URLImagen = @"../Images/placeholders/700x500.png";
            miCurso.URL = @"Home/Course";
            miCurso.Controller = "Course";
            miCurso.View = "Course";
            miCurso.Style = @"course__pic";
            miCurso.Nombre = "Campaña permanente";
            miCurso.Descripcion = "Campaña permanente - test";
            miCurso.Sn_Activo = -1;

            miLista.Add(miCurso);
            */

            return miLista;
        }
        public List<Curso> getCursos(int id_modulo, int? id_curso)
        {
            List<Curso> miLista = new List<Curso>();

            Curso miCurso = new Curso();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros;
            if (id_curso == null) {
                misParametros = new OleDbParameter[1];        
            }
            else { 
                misParametros = new OleDbParameter[2];
            }
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            if (id_curso != null)
            {
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Convert.ToInt32(id_curso);
                misParametros[1] = miParametro;
            }

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cursos_abm", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miCurso = new Curso();
                    miCurso.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miCurso.Id_curso = Convert.ToInt32(miDataReader["Curso"]);
                    miCurso.Nombre = miDataReader["NombreCurso"].ToString();
                    miCurso.Nombre_Modulo = miDataReader["Titulo"].ToString();
                    miCurso.Descripcion = miDataReader["DescripcionCurso"].ToString();
                    miCurso.Controller = miDataReader["Controller"].ToString();
                    miCurso.View = miDataReader["View"].ToString();
                    miCurso.Style = miDataReader["Style"].ToString();
                    miCurso.URL = "";
                    miCurso.URLImagen = miDataReader["Imagen"].ToString();
                    miCurso.Sn_Activo = Convert.ToInt32(miDataReader["Activo"]);

                    if (miDataReader["NombrePadre"].ToString()  != "")
                    {
                        miCurso.Nombre_Padre = miDataReader["NombrePadre"].ToString();
                        miCurso.Padre = Convert.ToInt32(miDataReader["Padre"]);
                    }
                    miLista.Add(miCurso);
                }

            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }

        public string InhabilitarCurso(int id_modulo, int id_curso)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;


                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_inhabilitar_curso", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string HabilitarCurso(int id_modulo, int id_curso)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;


                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_habilitar_curso", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }

        public string agregarCurso(int id_modulo, string Nombre, string Descripcion, string Controller, string View, string Style, string Imagen, int? parent)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[8];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Descripcion";
                miParametro.Value = Descripcion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@controller";
                miParametro.Value = Controller;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@view";
                miParametro.Value = View;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@style";
                miParametro.Value = Style;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Imagen";
                miParametro.Value = Imagen;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@parent";
                miParametro.Value = parent;
                misParametros[7] = miParametro;


                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_agregar_curso", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string modificarCurso(int ID_Modulo, int Id_curso, string Nombre, string Descripcion, string Controller, string View, string Style, string Imagen)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[8];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = ID_Modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Descripcion";
                miParametro.Value = Descripcion;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@controller";
                miParametro.Value = Controller;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@view";
                miParametro.Value = View;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@style";
                miParametro.Value = Style;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Imagen";
                miParametro.Value = Imagen;
                misParametros[7] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_modificar_curso", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
            
        }
        
    }
}