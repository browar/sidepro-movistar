﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using System.Data.OleDb;
namespace Sidepro.Data
{
    public class UsuarioData
    {
        public MailUsuario DesbloquearUsuario(int cod_usuario)
        {

            MailUsuario datosEmail = new MailUsuario();
            try 
            {  
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[1];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_usuario";
                miParametro.Value = cod_usuario;
                misParametros[0] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_desbloquear_usuario", misParametros);

                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    
                    datosEmail.Nombre = miDataReader["Nombre"].ToString();
                    datosEmail.Apellido = miDataReader["Apellido"].ToString();
                    datosEmail.User = miDataReader["User"].ToString();
                    datosEmail.Password = miDataReader["Password"].ToString();
                    datosEmail.Mensaje = miDataReader["Mensaje"].ToString();
                    datosEmail.Mail = miDataReader["MailUsuario"].ToString();
                    datosEmail.MailAgencia = miDataReader["MailAgencia"].ToString();
                    datosEmail.Agencia = miDataReader["Agencia"].ToString();
                    datosEmail.cod_usuario = cod_usuario;
                }
                miDataReader.Close();
                miConn.Close();

            }
            catch (Exception ex)
            {
                datosEmail.Mensaje = ex.Message.ToString();
            }

            return datosEmail;
            
        }
        public string getEmail(int cod_usuario)
        {
            string email= "";
            try
            {
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[1];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_usuario";
                miParametro.Value = cod_usuario;
                misParametros[0] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_email", misParametros);

                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    email = miDataReader["Email"].ToString();
                }
                miDataReader.Close();
                miConn.Close();

            }
            catch (Exception ex)
            {
                email = "Error al obtener email";
            }

            return email;
        }
    }
}