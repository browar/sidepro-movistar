﻿using Sidepro.Entities;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Web;

namespace Sidepro.Data
{
    public class RolData
    {
        public List<Rol> getRoles()
        {

            List<Rol> miLista = new List<Rol>();
            Rol miRol;


            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_roles");
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miRol = new Rol();
                    miRol.ID_Rol = Convert.ToInt32(miDataReader["cod_rol"]);
                    miRol.Nombre = miDataReader["rol"].ToString();
                    miRol.cod_canal = Convert.ToInt32(miDataReader["cod_canal"]);
                    miRol.Canal = miDataReader["Canal"].ToString();
                    miLista.Add(miRol);
                }
            }

            miConn.Close();
            return miLista;

        }
        public List<Rol> getRolesDestacados()
        {

            List<Rol> miLista = new List<Rol>();
            Rol miRol;


            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_roles_destacados");
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miRol = new Rol();
                    miRol.ID_Rol = Convert.ToInt32(miDataReader["cod_rol"]);
                    miRol.Nombre = miDataReader["rol"].ToString();
                    miLista.Add(miRol);
                }
            }

            miConn.Close();
            return miLista;

        }

        public string agregarRol(int cod_canal, string rol)
        {

            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_canal";
                miParametro.Value = cod_canal;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_desc";
                miParametro.Value = rol;
                misParametros[1] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_rol", misParametros);
                
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miDataReader.Close();
                miConn.Close();
                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }
        public string modificarRol(int cod_canal, int id_rol, string rol)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[3];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_canal";
                miParametro.Value = cod_canal;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = id_rol;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_desc";
                miParametro.Value = rol;
                misParametros[2] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_upd_rol", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miDataReader.Close();
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }

        public List<ModuloCursos> getRolModulos(int id_rol)
        {

            List<ModuloCursos> miLista = new List<ModuloCursos>();
            ModuloCursos misCursos;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_rol";
            miParametro.Value = id_rol;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_modulo_rol", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    misCursos = new ModuloCursos();
                    misCursos.Id_modulo = Convert.ToInt32(miDataReader["ID_Modulo"]);
                    misCursos.Nombre = miDataReader["Modulo"].ToString();
                    miLista.Add(misCursos);
                }
            }

            miConn.Close();
            return miLista;
        }
        public List<ModuloCursos> getModulos(int id_rol)
        {

            List<ModuloCursos> miLista = new List<ModuloCursos>();
            ModuloCursos misCursos;
            
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_rol";
            miParametro.Value = id_rol;
            misParametros[0] = miParametro;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_modulo_ABM", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    misCursos = new ModuloCursos();
                    misCursos.Id_modulo = Convert.ToInt32(miDataReader["ID_Modulo"]);
                    misCursos.Nombre = miDataReader["Modulo"].ToString();
                    miLista.Add(misCursos);
                }
            }

            miConn.Close();
            return miLista;
        }

        public List<ModuloCursos> getModulosDestacados(int cod_rol)
        {
            List<ModuloCursos> miLista = new List<ModuloCursos>();
            ModuloCursos misCursos;

            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_rol";
            miParametro.Value = cod_rol;
            misParametros[0] = miParametro;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_modulos_destacados", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    misCursos = new ModuloCursos();
                    misCursos.Id_modulo = Convert.ToInt32(miDataReader["ID_Modulo"]);
                    misCursos.Nombre = miDataReader["Nombre"].ToString();
                    miLista.Add(misCursos);
                }
            }

            miConn.Close();
            return miLista;
        }   
        public string agregarRolModulo(int id_rol, int id_modulo)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = id_rol;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[1] = miParametro;


                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_modulo_rol", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string eliminarRolModulo(int id_modulo, int id_rol)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[2];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = id_rol;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[1] = miParametro;


                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_del_modulo_rol", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }

        public List<Curso> getRolCursos(int id_rol, int id_modulo, int? id_curso)
        {
            List<Curso> miLista = new List<Curso>();

            Curso miCurso = new Curso();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros;
            if (id_curso == null){
                misParametros = new OleDbParameter[2];
            }else{
                misParametros = new OleDbParameter[3];
            }
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_rol";
            miParametro.Value = id_rol;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[1] = miParametro;

            if (id_curso != null)
            {
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Convert.ToInt32(id_curso);
                misParametros[2] = miParametro;
            }

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cursos_rol", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miCurso = new Curso();
                    miCurso.Id_modulo = Convert.ToInt32(miDataReader["ID_Modulo"]);
                    miCurso.Id_curso = Convert.ToInt32(miDataReader["ID_Curso"]);
                    miCurso.Nombre = miDataReader["Nombre"].ToString();
                    miCurso.Nombre_Padre = miDataReader["NombrePadre"].ToString();
                    miCurso.Sigue = Convert.ToInt32(miDataReader["Sigue"]);
                    miLista.Add(miCurso);
                }

            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public string getNombrePadre(int id_rol, int id_modulo, int? id_curso)
        {

            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros;
                if (id_curso == null)
                {
                    misParametros = new OleDbParameter[2];
                }
                else
                {
                    misParametros = new OleDbParameter[3];
                }
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_rol";
                miParametro.Value = id_rol;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[1] = miParametro;

                if (id_curso != null)
                {
                    miParametro = new OleDbParameter();
                    miParametro.OleDbType = OleDbType.Integer;
                    miParametro.ParameterName = "@id_curso";
                    miParametro.Value = Convert.ToInt32(id_curso);
                    misParametros[2] = miParametro;
                }

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_obtener_nombre_padre", misParametros);

                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Nombre"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return "";

            }
        }
        public List<Curso> getCursos(int id_rol, int id_modulo, int? id_curso)
        {
            List<Curso> miLista = new List<Curso>();

            Curso miCurso = new Curso();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros;
            if (id_curso == null)
            {
                misParametros = new OleDbParameter[2];
            }
            else
            {
                misParametros = new OleDbParameter[3];
            }
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_rol";
            miParametro.Value = id_rol;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[1] = miParametro;

            if (id_curso != null)
            {
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Convert.ToInt32(id_curso);
                misParametros[2] = miParametro;
            }

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cursos_rol_ABM", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miCurso = new Curso();
                    miCurso.Id_curso = Convert.ToInt32(miDataReader["ID_Curso"]);
                    miCurso.Nombre = miDataReader["Nombre"].ToString();
                    miLista.Add(miCurso);
                }

            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }

        public List<Curso> getCursosDestacados(int cod_rol, int id_modulo, int? id_curso)
        {
            List<Curso> miLista = new List<Curso>();

            Curso miCurso = new Curso();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros;
            if (id_curso == null)
            {
                misParametros = new OleDbParameter[2];
            }
            else
            {
                misParametros = new OleDbParameter[3];
            }
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_rol";
            miParametro.Value = cod_rol;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[1] = miParametro;

            if (id_curso != null)
            {
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@parent";
                miParametro.Value = Convert.ToInt32(id_curso);
                misParametros[2] = miParametro;
            }

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cursos_destacados", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miCurso = new Curso();
                    miCurso.Id_curso = Convert.ToInt32(miDataReader["ID_Curso"]);
                    miCurso.Nombre = miDataReader["Nombre"].ToString();
                    miCurso.Sigue = Convert.ToInt32(miDataReader["Sigue"]);
                    miLista.Add(miCurso);
                }

            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public string agregarRolCurso(int id_rol, int id_modulo, int id_curso, int? parent)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros;
                if (parent == null)
                {
                    misParametros = new OleDbParameter[3];
                }
                else
                {
                    misParametros = new OleDbParameter[4];
                }
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = id_rol;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[2] = miParametro;


                if (parent != null)
                {
                    miParametro = new OleDbParameter();
                    miParametro.OleDbType = OleDbType.Integer;
                    miParametro.ParameterName = "@parent";
                    miParametro.Value = Convert.ToInt32(parent);
                    misParametros[3] = miParametro;
                }


                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_curso_rol", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }

        public string eliminarRolCurso(int id_modulo, int id_curso, int id_rol)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[3];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = id_rol;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[2] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_del_curso_rol", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public List<Destacado> getDestacados()
        {
            List<Destacado> miLista = new List<Destacado>();

            Destacado miDestacado = new Destacado();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_destacados_ABM");

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miDestacado = new Destacado();

                    miDestacado.ID_Destacado = Convert.ToInt32(miDataReader["ID_Destacado"]);
                    miDestacado.ID_Rol = Convert.ToInt32(miDataReader["ID_Rol"]);
                    miDestacado.ID_Modulo = Convert.ToInt32(miDataReader["ID_Modulo"]);
                    miDestacado.ID_Curso = (miDataReader["ID_Curso"] != DBNull.Value) ? (int?)Convert.ToInt32(miDataReader["ID_Curso"]) : null;
                    miDestacado.ID_Leccion = (miDataReader["ID_Leccion"] != DBNull.Value) ? (int?) Convert.ToInt32(miDataReader["ID_Leccion"]) : null ;
                    miDestacado.Orden = Convert.ToInt32(miDataReader["Orden"]);
                    miDestacado.TipoDestacado = Convert.ToInt32(miDataReader["cod_tipo_destacado"]);
                    miDestacado.TipoDescripcion = miDataReader["TipoDestacado"].ToString();

                    miDestacado.Rol = miDataReader["Rol"].ToString();
                    miDestacado.Modulo = miDataReader["Modulo"].ToString();
                    miDestacado.Curso = miDataReader["Curso"].ToString();
                    miDestacado.Leccion = miDataReader["Leccion"].ToString();

                    miDestacado.Imagen = miDataReader["URL_Imagen"].ToString();
                    
                    
                    miLista.Add(miDestacado);
                    
                }

            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public List<TipoDestacado> getTipoDestacados()
        {
            List<TipoDestacado> miLista = new List<TipoDestacado>();
            TipoDestacado miDestacado;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_tipo_destacados");

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miDestacado = new TipoDestacado();
                    miDestacado.Cod_tipo_destacado = Convert.ToInt32(miDataReader["cod_tipo_destacado"]);
                    miDestacado.Nombre = miDataReader["Nombre"].ToString();
                    miLista.Add(miDestacado);

                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public string AgregarDestacado(int id_modulo, int? id_curso, int cod_rol, int cod_tipo_destacado, string url_imagen)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros;
                misParametros = new OleDbParameter[6];

                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_rol";
                miParametro.Value = cod_rol;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = (id_curso == null) ? null : id_curso;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = null;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_tipo_destacado";
                miParametro.Value = cod_tipo_destacado;
                misParametros[4] = miParametro;
                
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@url_destacado";
                miParametro.Value = url_imagen;
                misParametros[5] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_destacado", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }
        public string EliminarDestacado(int id_destacado)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros;
                misParametros = new OleDbParameter[1];

                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_destacado";
                miParametro.Value = id_destacado;
                misParametros[0] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_del_destacado", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string ModificarDestacado(int id_destacado, int cod_tipo_destacado, string url_imagen, int orden)
        {
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros;
                misParametros = new OleDbParameter[4];

                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_destacado";
                miParametro.Value = id_destacado;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_tipo_destacado";
                miParametro.Value = cod_tipo_destacado;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@url_imagen";
                miParametro.Value = url_imagen;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@orden";
                miParametro.Value = orden;
                misParametros[3] = miParametro;
                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_upd_destacado", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
    }
}
