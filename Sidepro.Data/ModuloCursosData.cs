﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using System.Data;
using System.Data.OleDb;
namespace Sidepro.Data
{
    public class ModuloCursosData
    {
        private int GrupoHistorial = 1;
        public List<ModuloCursos> getGrupos() {
            List<ModuloCursos> miLista = new List<ModuloCursos>();
            
            CursosData miCursosData = new CursosData();
            ModuloCursos miGrupo = new ModuloCursos();
            miGrupo.Cursos = miCursosData.getCursosDeGrupo(2);
            miGrupo.Id_modulo = 2;
            miGrupo.Nombre = "Equipos";
            miLista.Add(miGrupo);
            return miLista;
        
        }
        public List<ModuloCursos> getGrupoMenu(int id_usuario)
        {
            //Obtiene los grupos del menú de la derecha de la BD (customizable) con su URL, CONTROLER y VIEW correspondiente.
            List<ModuloCursos> miLista = new List<ModuloCursos>();

            //miGrupo.Cursos = miCursosData.getCursosDeGrupo(GrupoHistorial);
            ModuloCursos miGrupo = new ModuloCursos();

            
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_modulos", misParametros);
            if (miDataReader.HasRows) {
                while (miDataReader.Read())
                {
                    miGrupo = new ModuloCursos();
                    miGrupo.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miGrupo.Nombre = miDataReader["Nombre"].ToString();
                    miGrupo.Style = "";// miDataReader["Style"].ToString();
                    miGrupo.URL = "";
                    miGrupo.Controller = miDataReader["Controller"].ToString();
                    miGrupo.View = miDataReader["View"].ToString();
                    miGrupo.HTMLTag = miDataReader["HTMLTags"].ToString();
                    miGrupo.PuedeRealizar = Convert.ToInt32(miDataReader["PuedeRealizar"]);
                    miLista.Add(miGrupo);    

                }

            }

            miConn.Close();

            #region - Hardcodes -
            /*
            miGrupo.Idgrupo = 1;
            miGrupo.Nombre = "Módulo inicial";
            miGrupo.Style = "";
            miGrupo.URL = "Home/Module";
            miGrupo.Controller = "Module";
            miGrupo.View = "Module";
            miGrupo.HTMLTag = "";
            miLista.Add(miGrupo);            


            miGrupo = new ModuloCursos();
            miGrupo.Idgrupo = 2;
            miGrupo.Nombre = "Equipos";
            miGrupo.Style = "";
            miGrupo.URL = "Home/Module";
            miGrupo.Controller = "Module";
            miGrupo.View = "Module";
            miGrupo.HTMLTag = @" <span class='label label--blue'>Nuevos cursos</span>";
            miLista.Add(miGrupo);            

            miGrupo = new ModuloCursos();
            miGrupo.Idgrupo = 3;
            miGrupo.Nombre = "Oferta y Campaña";
            miGrupo.Style = "";
            miGrupo.URL = "Home/Module";
            miGrupo.Controller = "Module";
            miGrupo.View = "Module";
            miGrupo.HTMLTag = "";
            miLista.Add(miGrupo);            

            miGrupo = new ModuloCursos();
            miGrupo.Idgrupo = 4;
            miGrupo.Nombre = "Saber más";
            miGrupo.Style = "";
            miGrupo.URL = "Home/Module";
            miGrupo.Controller = "Module";
            miGrupo.View = "Module";
            miGrupo.HTMLTag = "";
            miLista.Add(miGrupo);        


            miGrupo = new ModuloCursos();
            miGrupo.Idgrupo = 5;
            miGrupo.Nombre = "Certificación";
            miGrupo.Style = "";
            miGrupo.URL = "Home/Module";
            miGrupo.Controller = "Home";
            miGrupo.View = "Certification";
            miGrupo.HTMLTag = " <span class='label label--green'>¡Nuevo!</span>";
            miLista.Add(miGrupo);      
            */
            #endregion

            return miLista;
        }

        public List<ModuloCursos> getModulos()
        {
            //Obtiene los grupos del menú de la derecha de la BD (customizable) con su URL, CONTROLER y VIEW correspondiente.
            List<ModuloCursos> miLista = new List<ModuloCursos>();

            //miGrupo.Cursos = miCursosData.getCursosDeGrupo(GrupoHistorial);
            ModuloCursos miModulo = new ModuloCursos();


            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_modulos_ABM");
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miModulo = new ModuloCursos();
                    miModulo.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miModulo.Nombre = miDataReader["Nombre"].ToString();
                    miModulo.Controller = miDataReader["Controller"].ToString();
                    miModulo.View = miDataReader["View"].ToString();
                    miModulo.HTMLTag = miDataReader["Tags"].ToString();
                    miModulo.Style = miDataReader["Style"].ToString();
                    miModulo.Sn_activo = Convert.ToInt32(miDataReader["Activo"]);
                    miLista.Add(miModulo);
                }
            }

            miConn.Close();
            return miLista;
        }

        public string agregarModulo(string Nombre, string Controller, string View, string Style, string Tags)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[5];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@controller";
                miParametro.Value = Controller;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@view";
                miParametro.Value = View;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@style";
                miParametro.Value = Style;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@tags";
                miParametro.Value = Tags;
                misParametros[4] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_agregar_modulo", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string modificarModulo(int ID_Modulo, string Nombre, string Controller, string View, string Style, string Tags)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[6];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = ID_Modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@controller";
                miParametro.Value = Controller;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@view";
                miParametro.Value = View;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@style";
                miParametro.Value = Style;
                misParametros[4] = miParametro;
                
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@tags";
                miParametro.Value = Tags;
                misParametros[5] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_modificar_modulo", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string inhabilitarModulo(int ID_Modulo)
        {
            string Respuesta = "";

            try { 

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[1];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = ID_Modulo;
                misParametros[0] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_inhabilitar_modulo", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch(Exception ex)
            {
                return ex.Message;

            }
        }
        public string habilitarModulo(int ID_Modulo)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[1];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = ID_Modulo;
                misParametros[0] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_habilitar_modulo", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
    }
}