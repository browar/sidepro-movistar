﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using System.Data.OleDb;
namespace Sidepro.Data
{
    public class LeccionData
    {
        public List<Leccion> getLeccionesDeCurso(int id_modulo, int id_curso, int id_usuario) {

            List<Leccion> miLista = new List<Leccion>();
            bool ultimoVisto = false;


            Leccion miLeccion = new Leccion();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[3];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_usuario";
            miParametro.Value = id_usuario;
            misParametros[2] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_lecciones_del_curso", misParametros);
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miLeccion = new Leccion();
                    miLeccion.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miLeccion.Id_curso = Convert.ToInt32(miDataReader["Curso"]);
                    miLeccion.Id_leccion = Convert.ToInt32(miDataReader["Leccion"]);
                    miLeccion.Nombre = miDataReader["Nombre"].ToString();
                    miLeccion.NombreCurso = miDataReader["NombreCurso"].ToString();
                    miLeccion.ImagenCurso = miDataReader["ImagenCurso"].ToString();
                    miLeccion.Style = "";
                    miLeccion.URL_Video =  miDataReader["URL_Video"].ToString();
                    miLeccion.URL_Imagen = miDataReader["URL_Imagen"].ToString();
                    miLeccion.Descripcion = miDataReader["Descripcion"].ToString();
                    miLeccion.Pje_aprobacion = Convert.ToDecimal(miDataReader["pje_aprobacion"]);
                    miLeccion.Aprobo = Convert.ToInt32( miDataReader["Aprobo"]);
                    miLeccion.Sn_Activo = -1;
                    miLeccion.PuedeRealizar = Convert.ToInt32(miDataReader["PuedeRealizar"]);
                    if (miDataReader["Fec_realizado"].ToString() != "")
                    {
                        miLeccion.Fec_realizado =  Convert.ToDateTime(miDataReader["Fec_realizado"]);
                    }


                    if (miLeccion.Aprobo == -1)//miLeccion.Fec_realizado != null)
                    {
                        miLeccion.Style = "course-list__item is-viewed";
                    }
                    else
                    {
                        if (ultimoVisto)
                        {
                            miLeccion.Style = "course-list__item";
                        }
                        else
                        {
                            miLeccion.Style = "course-list__item is-playing";
                            miLeccion.Sn_Activo = -2;
                            ultimoVisto = true;
                        }
                    }

                    miLista.Add(miLeccion);
                }


            }
            if (!ultimoVisto)
            {
                miLeccion.Style = miLeccion.Style + " is-playing";
                miLeccion.Sn_Activo = -2;
            }
            #region - HARDCODES -
            /*
            miLeccion.Id_leccion = 1;
            miLeccion.Nombre = "Cámara";
            miLeccion.Descripcion = "Caracteristicas de la cámara del Samsung S6";
            //miLeccion.URLVideo = "";
            miLeccion.Sn_Activo = -1;
            miLeccion.Sn_visto = -1;
            if (miLeccion.Sn_visto == -1) {
                miLeccion.Style = "course-list__item is-viewed";
            }else{
                if (ultimoVisto) {
                    miLeccion.Style = "course-list__item";
                }else{
                    miLeccion.Style = "course-list__item is-playing";
                    ultimoVisto = true;
                }
            }

            miLista.Add(miLeccion);

            miLeccion = new Leccion();
            miLeccion.Id_leccion = 2;
            miLeccion.Nombre = "Caracteristicas Generales";
            miLeccion.Descripcion = "Caracteristicas generales del Samsung S6";
            miLeccion.URLVideo = "";
            miLeccion.Sn_Activo = -1;
            miLeccion.Sn_visto = 0;
            if (miLeccion.Sn_visto == -1) {
                miLeccion.Style = "course-list__item is-viewed";
            }else{
                if (ultimoVisto) {
                    miLeccion.Style = "course-list__item";
                }else{
                    miLeccion.Style = "course-list__item is-playing";
                    ultimoVisto = true;
                }
            }
            miLista.Add(miLeccion);

            miLeccion = new Leccion();
            miLeccion.Id_leccion = 2;
            miLeccion.Nombre = "Diseño y Pantalla";
            miLeccion.Descripcion = "Diseño y Pantalla del Samsung S6";
            miLeccion.URLVideo = "";
            miLeccion.Sn_Activo = -1;
            miLeccion.Sn_visto = 0;
            if (miLeccion.Sn_visto == -1)
            {
                miLeccion.Style = "course-list__item is-viewed";
            }
            else
            {
                if (ultimoVisto)
                {
                    miLeccion.Style = "course-list__item";
                }
                else
                {
                    miLeccion.Style = "course-list__item is-playing";
                    ultimoVisto = true;
                }
            }
            miLista.Add(miLeccion);

            miLeccion = new Leccion();
            miLeccion.Id_leccion = 2;
            miLeccion.Nombre = "Seguridad";
            miLeccion.Descripcion = "Seguridad del Samsung S6";
            miLeccion.URLVideo = "";
            miLeccion.Sn_Activo = -1;
            miLeccion.Sn_visto = 0;
            if (miLeccion.Sn_visto == -1)
            {
                miLeccion.Style = "course-list__item is-viewed";
            }
            else
            {
                if (ultimoVisto)
                {
                    miLeccion.Style = "course-list__item";
                }
                else
                {
                    miLeccion.Style = "course-list__item is-playing";
                    ultimoVisto = true;
                }
            }
            miLista.Add(miLeccion);
            
            miLeccion = new Leccion();
            miLeccion.Id_leccion = 2;
            miLeccion.Nombre = "Tips para el vendedor";
            miLeccion.Descripcion = "Tips para el vendedor Samsung S6";
            miLeccion.URLVideo = "";
            miLeccion.Sn_Activo = -1;
            miLeccion.Sn_visto = 0;
            if (miLeccion.Sn_visto == -1)
            {
                miLeccion.Style = "course-list__item is-viewed";
            }
            else
            {
                if (ultimoVisto)
                {
                    miLeccion.Style = "course-list__item";
                }
                else
                {
                    miLeccion.Style = "course-list__item is-playing";
                    ultimoVisto = true;
                }
            }
            miLista.Add(miLeccion);           



            */
            #endregion

            return miLista;
        
        }

        public Leccion getLeccion(int id_modulo, int id_curso, int id_leccion)
        {
            bool ultimoVisto = false;


            Leccion miLeccion = new Leccion();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[3];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_leccion";
            miParametro.Value = id_leccion;
            misParametros[2] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_leccion", misParametros);
            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                miLeccion.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                miLeccion.Id_curso = Convert.ToInt32(miDataReader["Curso"]);
                miLeccion.Id_leccion = Convert.ToInt32(miDataReader["Leccion"]);
                miLeccion.Nombre = miDataReader["Nombre"].ToString();
                miLeccion.Style = "";
                miLeccion.URL_Video = miDataReader["URL_Video"].ToString();
                miLeccion.URL_Imagen = miDataReader["URL_Imagen"].ToString();
                miLeccion.Descripcion = miDataReader["Descripcion"].ToString();
                miLeccion.Pje_aprobacion = Convert.ToDecimal(miDataReader["pje_aprobacion"]);
                miLeccion.Aprobo = 0;// Convert.ToInt32(miDataReader["Aprobo"]);
                miLeccion.Sn_Activo = -1;
                if (miLeccion.Fec_realizado != null)
                {
                    miLeccion.Style = "course-list__item is-viewed";
                }
                else
                {
                    if (ultimoVisto)
                    {
                        miLeccion.Style = "course-list__item";
                    }
                    else
                    {
                        miLeccion.Style = "course-list__item is-playing";
                        miLeccion.Sn_Activo = -2;
                        ultimoVisto = true;
                    }
                }

            }
            miDataReader.Close();
            miConn.Close();

            return miLeccion;
        }


        public Quiz getQuizCertificaciones(int id_usuario) {
            Quiz miQuiz = new Quiz();
            List<Question> misQuestions = new List<Question>();
            Question miQuestion;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_preguntas_de_certificacion", misParametros);
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miQuestion = new Question();
                    miQuestion.q = miDataReader["Pregunta"].ToString();
                    miQuestion.correct = miDataReader["Correcto"].ToString();
                    miQuestion.incorrect = miDataReader["Incorrecto"].ToString();

                    miQuestion.id = Convert.ToInt32(miDataReader["IdPregunta"]);
                    miQuestion.valor = Convert.ToInt32(miDataReader["Puntos"]);


                    miQuiz.nombre = miDataReader["Nombre"].ToString();
                    miQuiz.aprobado = (Convert.ToInt32(miDataReader["sn_aprobado"]) == 0 ? (miQuestion.valor < 0 ? -1: 0) : -1) == -1;
                    miQuiz.sn_inicial = Convert.ToInt32(miDataReader["sn_inicial"]) == -1;
                     
                        
                    misQuestions.Add(miQuestion);
                }
            }
            miDataReader.Close();
            miConn.Close();
            foreach (Question pregunta in misQuestions)
            {
                pregunta.a = getAnswers(4, 0, 0, pregunta.id).ToArray();
            }
            //Question[] misQuestions = new Question[2];
            Answer[] misAnswers = new Answer[4];

            Info quizInfo = new Info();
            quizInfo.name = miQuiz.nombre;
            quizInfo.main = "";
            quizInfo.results = "&nbsp";

            miQuiz.info = quizInfo;
            miQuiz.questions = misQuestions.ToArray();
            return miQuiz;
        }
        public Quiz getQuiz(int id_modulo, int id_curso, int id_leccion, int id_usuario)
        {
            Quiz miQuiz = new Quiz();
            List<Question> misQuestions = new List<Question>();
            Question miQuestion;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[4];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_leccion";
            miParametro.Value = id_leccion;
            misParametros[2] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_usuario";
            miParametro.Value = id_usuario;
            misParametros[3] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_preguntas_de_leccion", misParametros);
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read()) 
                {
                    miQuestion = new Question();
                    miQuestion.q = miDataReader["Pregunta"].ToString();
                    miQuestion.correct = miDataReader["Correcto"].ToString();
                    miQuestion.incorrect = miDataReader["Incorrecto"].ToString();
                    
                    miQuestion.id = Convert.ToInt32(miDataReader["IdPregunta"]);
                    miQuestion.valor = Convert.ToDecimal(miDataReader["Puntos"]);


                    miQuiz.nombre = miDataReader["Nombre"].ToString();
                    miQuiz.aprobado = Convert.ToInt32(miDataReader["sn_aprobado"]) == -1;
                    misQuestions.Add(miQuestion);
                }
            }
            miDataReader.Close();
            miConn.Close();
            foreach (Question pregunta in misQuestions) { 
                pregunta.a = getAnswers(id_modulo, id_curso, id_leccion, pregunta.id).ToArray();
            }
            //Question[] misQuestions = new Question[2];
            Answer[] misAnswers = new Answer[4];

            Info quizInfo = new Info();
            quizInfo.name = miQuiz.nombre;
            quizInfo.main = "";
            quizInfo.results = "&nbsp";

            miQuiz.info = quizInfo;
            miQuiz.questions = misQuestions.ToArray();
            return miQuiz;
        }
        public List<Answer> getAnswers(int id_modulo, int id_curso, int id_leccion, int id_pregunta){
            List<Answer> misAnswers = new List<Answer>();
            Answer miAnswer;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[4];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_leccion";
            miParametro.Value = id_leccion;
            misParametros[2] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_pregunta";
            miParametro.Value = id_pregunta;
            misParametros[3] = miParametro;
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_respuestas_de_pregunta_leccion", misParametros);
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miAnswer = new Answer();
                    miAnswer.option = miDataReader["Pregunta"].ToString();
                    miAnswer.correct = Convert.ToInt32(miDataReader["Correcta"]) == -1;
                    misAnswers.Add(miAnswer);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return misAnswers;
        }
        public bool grabarResultado(int id_modulo, int id_curso, int id_leccion, decimal nota, int id_usuario)
        {
            bool resultado = false;
            try 
            { 
                Leccion miLeccion = new Leccion();
                
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[5];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Numeric;
                miParametro.ParameterName = "@nota";
                miParametro.Value = nota;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_usuario";
                miParametro.Value = id_usuario;
                misParametros[4] = miParametro;
                
                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_grabar_nota_leccion", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    resultado = (Convert.ToInt32(miDataReader["Aprobado"]) == -1);
                    
                }
                miDataReader.Close();
                miConn.Close();
                return resultado;
            }
            catch (Exception ex)
            {
                return resultado;
            }


        }
        public void grabarRespuesta(int id_usuario, int question, int answer)
        {


                Leccion miLeccion = new Leccion();

                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[6];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_usuario";
                miParametro.Value = id_usuario;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = 4;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = 0;
                misParametros[2] = miParametro;


                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = 0;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_pregunta";
                miParametro.Value = question;
                misParametros[4] = miParametro;


                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_respuesta";
                miParametro.Value = answer;
                misParametros[5] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_grabar_respuesta", misParametros);
                miDataReader.Close();
                miConn.Close();

 
        }

        public Certification getResultado(int id_modulo, int id_curso, int id_leccion, int id_usuario)
        {
            Certification miCert = new Certification();
            try 
            { 
                Leccion miLeccion = new Leccion();
                
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[4];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_usuario";
                miParametro.Value = id_usuario;
                misParametros[3] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_grabar_nota_certificacion", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    miCert.Nota = Convert.ToInt32(miDataReader["Nota"]);
                    miCert.Aprobado = Convert.ToInt32(miDataReader["Aprobado"]) == -1; 
                    
                }
                miDataReader.Close();
                miConn.Close();
                return miCert;
            }
            catch (Exception ex)
            {
                return miCert;
            }
            
          
        }
        public List<Leccion> getLecciones(int id_modulo, int id_curso)
        {
            List<Leccion> miLista = new List<Leccion>();
            Leccion miLeccion = new Leccion();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[2];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_leccion_ABM", misParametros);
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miLeccion = new Leccion();
                    miLeccion.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miLeccion.Id_curso = Convert.ToInt32(miDataReader["Curso"]);
                    miLeccion.Id_leccion = Convert.ToInt32(miDataReader["Leccion"]);
                    miLeccion.Nombre = miDataReader["NombreLeccion"].ToString();
                    miLeccion.Descripcion = miDataReader["DescripcionLeccion"].ToString();
                    miLeccion.URL_Video = miDataReader["Video"].ToString();
                    miLeccion.URL_Imagen = miDataReader["Imagen"].ToString();
                    miLeccion.Pje_aprobacion = Convert.ToDecimal(miDataReader["PjeAprobacion"]);
                    miLeccion.Sn_Activo = Convert.ToInt32(miDataReader["Activo"]);
                    if (miDataReader["Orden"] != DBNull.Value)
                    {
                        miLeccion.Orden = Convert.ToInt32(miDataReader["Orden"]);
                    }

                    miLista.Add(miLeccion);
                }
            }

            return miLista;
        }
        public string AgregarLeccion(int Id_modulo, int Id_curso, string Nombre, string Descripcion, string Imagen, string Video, decimal Pje_aprobacion)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[8];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = Id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_descripcion";
                miParametro.Value = Descripcion;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@URL_imagen";
                miParametro.Value = Imagen;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@URL_video";
                miParametro.Value = Video;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Decimal;
                miParametro.ParameterName = "@pje_aprobacion";
                miParametro.Value = Pje_aprobacion;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Decimal;
                miParametro.ParameterName = "@sn_activo";
                miParametro.Value = -1;
                misParametros[7] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_leccion", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string ModificarLeccion(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Descripcion, string Imagen, string Video, decimal Pje_aprobacion, int? Orden)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[9];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = Id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;
                
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = Id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_descripcion";
                miParametro.Value = Descripcion;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@URL_imagen";
                miParametro.Value = Imagen;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@URL_video";
                miParametro.Value = Video;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Decimal;
                miParametro.ParameterName = "@pje_aprobacion";
                miParametro.Value = Pje_aprobacion;
                misParametros[7] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@Orden";
                miParametro.Value = Orden;
                misParametros[8] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_modificar_leccion", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string InhabilitarLeccion(int id_modulo, int id_curso, int id_leccion)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[3];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;


                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;


                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_inhabilitar_leccion", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string HabilitarLeccion(int id_modulo, int id_curso, int id_leccion)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[3];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_habilitar_leccion", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }

        public List<Pregunta> getPreguntas(int id_modulo, int id_curso, int id_leccion)
        {

            List<Pregunta> miLista = new List<Pregunta>();
            Pregunta miPregunta;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[3];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;


            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;


            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_leccion";
            miParametro.Value = id_leccion;
            misParametros[2] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_pregunta_abm", misParametros);
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miPregunta = new Pregunta();

                    miPregunta.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miPregunta.Id_curso = Convert.ToInt32(miDataReader["Curso"]);
                    miPregunta.Id_leccion = Convert.ToInt32(miDataReader["Leccion"]);
                    miPregunta.Id_pregunta = Convert.ToInt32(miDataReader["Pregunta"]);
                    miPregunta.Nombre = miDataReader["NombrePregunta"].ToString();
                    miPregunta.Cant_puntos = Convert.ToDecimal(miDataReader["Puntos"]);
                    miPregunta.Correcto = miDataReader["Correcto"].ToString();
                    miPregunta.Incorrecto = miDataReader["Incorrecto"].ToString();
                    miPregunta.Sn_activo = Convert.ToInt32(miDataReader["Activo"]);
                    if (miDataReader["Categoria"] != DBNull.Value)
                    {
                        miPregunta.Categoria = Convert.ToInt32(miDataReader["Categoria"]);
                    }
                    if (miDataReader["NombreCategoria"] != DBNull.Value)
                    {
                        miPregunta.NombreCategoria = (miDataReader["NombreCategoria"]).ToString();
                    }

                    miPregunta.ID_ModuloCert = Convert.ToInt32(miDataReader["ID_ModuloCert"]);
                    miPregunta.ModuloCert = miDataReader["ModuloCert"].ToString();
                    miPregunta.ID_CursoCert = Convert.ToInt32(miDataReader["ID_CursoCert"]);
                    miPregunta.CursoCert = miDataReader["CursoCert"].ToString();
                    miPregunta.ID_LeccionCert = Convert.ToInt32(miDataReader["ID_LeccionCert"]);
                    miPregunta.LeccionCert = miDataReader["LeccionCert"].ToString();
                    /*
		 , ISNULL(tpl.id_modulo_rel_cert,-1) 'ID_ModuloCert'
		 , ISNULL(tm3.txt_nombre,'') 'ModuloCert'
		 , ISNULL(tpl.id_curso_rel_cert,'') 'ID_CursoCert'
		 , ISNULL(tc3.txt_nombre,-1) 'CursoCert'
		 , ISNULL(tpl.id_leccion_rel_cert,'') 'ID_LeccionCert'
		 , ISNULL(tl3.txt_nombre,-1) 'LeccionCert'
                */    

                    miLista.Add(miPregunta);
                }
            }
            miConn.Close();

            return miLista;
        }
        public string InhabilitarPregunta(int id_modulo, int id_curso, int id_leccion, int id_pregunta)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[4];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;


                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;


                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_pregunta";
                miParametro.Value = id_pregunta;
                misParametros[3] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_inhabilitar_pregunta", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string HabilitarPregunta(int id_modulo, int id_curso, int id_leccion, int id_pregunta)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[4];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_pregunta";
                miParametro.Value = id_pregunta;
                misParametros[3] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_habilitar_pregunta", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string AgregarPregunta(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Correcto, string Incorrecto)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[6];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = Id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = Id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Correcto";
                miParametro.Value = Correcto;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Incorrecto";
                miParametro.Value = Incorrecto;
                misParametros[5] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_agregar_pregunta", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string ModificarPregunta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, string Correcto, string Incorrecto, decimal Cant_puntos)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[8];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = Id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = Id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_pregunta";
                miParametro.Value = Id_pregunta;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Correcto";
                miParametro.Value = Correcto;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Incorrecto";
                miParametro.Value = Incorrecto;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Decimal;
                miParametro.ParameterName = "@Cant_puntos";
                miParametro.Value = Cant_puntos;
                misParametros[7] = miParametro;



                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_modificar_pregunta", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public List<Respuesta> GetRespuesta(int id_modulo, int id_curso, int Id_leccion, int id_pregunta)
        {
            List<Respuesta> miLista = new List<Respuesta>();
            Respuesta miRespuesta;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[4];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_modulo";
            miParametro.Value = id_modulo;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_curso";
            miParametro.Value = id_curso;
            misParametros[1] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_leccion";
            miParametro.Value = Id_leccion;
            misParametros[2] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_pregunta";
            miParametro.Value = id_pregunta;
            misParametros[3] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_respuesta_abm", misParametros);
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miRespuesta = new Respuesta();

                    miRespuesta.Id_modulo = Convert.ToInt32(miDataReader["Modulo"]);
                    miRespuesta.Id_curso = Convert.ToInt32(miDataReader["Curso"]);
                    miRespuesta.Id_leccion = Convert.ToInt32(miDataReader["Leccion"]);
                    miRespuesta.Id_pregunta = Convert.ToInt32(miDataReader["Pregunta"]);
                    miRespuesta.Id_respuesta = Convert.ToInt32(miDataReader["Respuesta"]);
                    miRespuesta.Nombre = miDataReader["NombreRespuesta"].ToString();
                    miRespuesta.Sn_correcto = Convert.ToInt32(miDataReader["Correcta"]);
                    miRespuesta.Sn_activo = Convert.ToInt32(miDataReader["Activo"]);


                    miLista.Add(miRespuesta);
                }
            }
            miConn.Close();

            return miLista;
        }
        public string AgregarRespuesta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, int Sn_correcto)
        {
           string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[6];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = Id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = Id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@Id_pregunta";
                miParametro.Value = Id_pregunta;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@Sn_correcto";
                miParametro.Value = Sn_correcto;
                misParametros[5] = miParametro;


                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_agregar_respuesta", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string ModificarRespuesta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta, string Nombre, int Sn_correcto)
        {
                       string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[7];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = Id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = Id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@Id_pregunta";
                miParametro.Value = Id_pregunta;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_respuesta";
                miParametro.Value = id_respuesta;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@Sn_correcto";
                miParametro.Value = Sn_correcto;
                misParametros[6] = miParametro;


                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_modificar_respuesta", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string AgregarPreguntaCertificacion(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Correcto, string Incorrecto, int categoria, int moduloCert, int cursoCert, int leccionCert)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[10];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = Id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = Id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Correcto";
                miParametro.Value = Correcto;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Incorrecto";
                miParametro.Value = Incorrecto;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@Categoria";
                miParametro.Value = categoria;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@moduloCert";
                miParametro.Value = moduloCert;
                misParametros[7] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cursoCert";
                miParametro.Value = cursoCert;
                misParametros[8] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@leccionCert";
                miParametro.Value = leccionCert;
                misParametros[9] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_agregar_pregunta_cert", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string ModificarPreguntaCertificacion(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, string Correcto, string Incorrecto, decimal Cant_puntos, int categoria, int moduloCert, int cursoCert, int leccionCert)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[12];
                OleDbParameter miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = Id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = Id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = Id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_pregunta";
                miParametro.Value = Id_pregunta;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_nombre";
                miParametro.Value = Nombre;
                misParametros[4] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Correcto";
                miParametro.Value = Correcto;
                misParametros[5] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@Incorrecto";
                miParametro.Value = Incorrecto;
                misParametros[6] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Decimal;
                miParametro.ParameterName = "@Cant_puntos";
                miParametro.Value = Cant_puntos;
                misParametros[7] = miParametro;

                
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@Categoria";
                miParametro.Value = categoria;
                misParametros[8] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@moduloCert";
                miParametro.Value = moduloCert;
                misParametros[9] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cursoCert";
                miParametro.Value = cursoCert;
                misParametros[10] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@leccionCert";
                miParametro.Value = leccionCert;
                misParametros[11] = miParametro;
                



                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_modificar_pregunta_cert", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }    
        }
        
        public string InhabilitarRespuesta(int id_modulo, int id_curso, int id_leccion, int id_pregunta, int id_respuesta)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[5];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_pregunta";
                miParametro.Value = id_pregunta;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_respuesta";
                miParametro.Value = id_respuesta;
                misParametros[4] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_inhabilitar_respuesta", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
        public string HabilitarRespuesta(int id_modulo, int id_curso, int id_leccion, int id_pregunta, int id_respuesta)
        {
            string Respuesta = "";

            try
            {

                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                OleDbParameter[] misParametros = new OleDbParameter[5];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_modulo";
                miParametro.Value = id_modulo;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_curso";
                miParametro.Value = id_curso;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_leccion";
                miParametro.Value = id_leccion;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_pregunta";
                miParametro.Value = id_pregunta;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_respuesta";
                miParametro.Value = id_respuesta;
                misParametros[4] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_habilitar_respuesta", misParametros);
                if (miDataReader.HasRows)
                {
                    miDataReader.Read();
                    Respuesta = miDataReader["Mensaje"].ToString();
                }
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }
        }
    }
}