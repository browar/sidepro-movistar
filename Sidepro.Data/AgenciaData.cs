﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using System.Data.OleDb;
namespace Sidepro.Data
{
    public class AgenciaData
    {
        public List<Agencia> getAgencias()
        {
            
            List<Agencia> miLista = new List<Agencia>();
            Agencia miAgencia = new Agencia();


            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_agencias");
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miAgencia = new Agencia();
                    miAgencia.ID_Agencia = Convert.ToInt32(miDataReader["id_agencia"]);
                    miAgencia.Nombre = miDataReader["txt_desc"].ToString();
                    miAgencia.Documento = miDataReader["nro_doc"].ToString();
                    miAgencia.Email = miDataReader["Email"].ToString();
                    miAgencia.Cod_tipo_agencia = Convert.ToInt32(miDataReader["cod_tipo_agencia"]);
                    miAgencia.TipoAgencia = miDataReader["TipoAgencia"].ToString();
                    miLista.Add(miAgencia);
                }
            }

            miConn.Close();
            return miLista;
            
        }
        public List<TipoAgencia> getTipoAgencias()
        {
            List<TipoAgencia> miLista = new List<TipoAgencia>();
            TipoAgencia miTipoAgencia;


            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_tipo_agencias");
            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miTipoAgencia = new TipoAgencia();
                    miTipoAgencia.Cod_tipo_agencia= Convert.ToInt32(miDataReader["cod_tipo_agencia"]);
                    miTipoAgencia.Nombre = miDataReader["TipoAgencia"].ToString();
                    miLista.Add(miTipoAgencia);
                }
            }

            miConn.Close();
            return miLista;
        }

        public string agregarAgencia(string Agencia, string Documento, string Email, int TipoAgencia)
        {
            
            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();
                
                OleDbParameter[] misParametros = new OleDbParameter[4];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@nro_doc";
                miParametro.Value = Documento;
                misParametros[0] = miParametro;
                
                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_desc";
                miParametro.Value = Agencia;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@email";
                miParametro.Value = Email;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_tipo_agencia";
                miParametro.Value = TipoAgencia;
                misParametros[3] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_ins_agencia", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }
        public string modificarAgencia(int id_agencia, string Agencia, string Documento, string Email, int TipoAgencia)
        {

            try
            {
                string Respuesta = "";
                OleDbConnection miConn = Helpers.HelpersData.getConnection();

                OleDbParameter[] misParametros = new OleDbParameter[5];
                OleDbParameter miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@id_agencia";
                miParametro.Value = id_agencia;
                misParametros[0] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@nro_doc";
                miParametro.Value = Documento;
                misParametros[1] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@txt_desc";
                miParametro.Value = Agencia;
                misParametros[2] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.VarChar;
                miParametro.ParameterName = "@email";
                miParametro.Value = Email;
                misParametros[3] = miParametro;

                miParametro = new OleDbParameter();
                miParametro.OleDbType = OleDbType.Integer;
                miParametro.ParameterName = "@cod_tipo_agencia";
                miParametro.Value = TipoAgencia;
                misParametros[4] = miParametro;

                OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_upd_agencia", misParametros);
                miConn.Close();

                return Respuesta;
            }
            catch (Exception ex)
            {
                return ex.Message;

            }

        }

    }
}