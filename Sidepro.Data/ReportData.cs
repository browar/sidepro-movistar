﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using System.Data.OleDb;
namespace Sidepro.Data
{
    public class ReportData
    {
        public List<CourseProgress> getProgresoMensual(int id_usuario)
        {

            List<CourseProgress> miLista = new List<CourseProgress>();
            CourseProgress miProgreso = new CourseProgress(); 

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_progress_mensual", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read()) {
                    miProgreso = new CourseProgress(); 

                    miProgreso.Total = Convert.ToDecimal(miDataReader["Resultado"]);
                    miProgreso.Mes = Convert.ToInt32(miDataReader["MesNumerico"]);
                    miProgreso.MesTexto = miDataReader["MesTexto"].ToString();


                    miLista.Add(miProgreso);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public List<CourseProgress> getCertMensuales()
        {
            List<CourseProgress> miLista = new List<CourseProgress>();
            CourseProgress miProgreso = new CourseProgress();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_certificaciones_mensuales");

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miProgreso = new CourseProgress();

                    miProgreso.Total = Convert.ToDecimal(miDataReader["Resultado"]);
                    miProgreso.Mes = Convert.ToInt32(miDataReader["MesNumerico"]);
                    miProgreso.MesTexto = miDataReader["MesTexto"].ToString();


                    miLista.Add(miProgreso);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public List<CourseProgress> getLogeoUsuarios()
        {
            List<CourseProgress> miLista = new List<CourseProgress>();
            CourseProgress miProgreso = new CourseProgress();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_logeos_mensuales");

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miProgreso = new CourseProgress();

                    miProgreso.Total = Convert.ToDecimal(miDataReader["Resultado"]);
                    miProgreso.Mes = Convert.ToInt32(miDataReader["MesNumerico"]);
                    miProgreso.MesTexto = miDataReader["MesTexto"].ToString();


                    miLista.Add(miProgreso);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;        
        }
        public List<CourseProgress> getProgresoMensualAgencia(int id_Agencia)
        {

            List<CourseProgress> miLista = new List<CourseProgress>();
            CourseProgress miProgreso = new CourseProgress();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_agencia";
            miParametro.Value = id_Agencia;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_progress_mensual_agencia", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miProgreso = new CourseProgress();

                    miProgreso.Total = Convert.ToDecimal(miDataReader["Resultado"]);
                    miProgreso.Mes = Convert.ToInt32(miDataReader["MesNumerico"]);
                    miProgreso.MesTexto = miDataReader["MesTexto"].ToString();
                    miProgreso.Periodo = miDataReader["Periodo"].ToString();
                    miProgreso.Ano = Convert.ToInt32(miDataReader["Ano"]);

                    miLista.Add(miProgreso);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public List<CourseProgress> getProgresoMensualUsuariosAgencia(int id_agencia)
        {

            List<CourseProgress> miLista = new List<CourseProgress>();
            CourseProgress miProgreso = new CourseProgress();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_agencia";
            miParametro.Value = id_agencia;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_progreso_agencia_por_mes", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miProgreso = new CourseProgress();

                    miProgreso.Total = Convert.ToDecimal(miDataReader["Resultado"]);
                    miProgreso.Mes = Convert.ToInt32(miDataReader["MesNumerico"]);
                    miProgreso.MesTexto = miDataReader["MesTexto"].ToString();
                    miProgreso.Ano = Convert.ToInt32(miDataReader["Ano"]);

                    miLista.Add(miProgreso);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }
        public CourseProgress getProgresoReportesAgencia(int id_usuario, int id_agencia)
        {


            CourseProgress miProgreso = new CourseProgress(); ;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[2];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;
            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_agencia";
            miParametro.Value = id_agencia;
            misParametros[1] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_progress_agencia", misParametros);

            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                miProgreso.ModuloInicial = Convert.ToDecimal(miDataReader["ProgresoInicial"]);
                miProgreso.Equipos = Convert.ToDecimal(miDataReader["ProgresoEquipos"]);
                miProgreso.OfertaYCampana = Convert.ToDecimal(miDataReader["ProgresoOferta"]);
                miProgreso.Certificacion = Convert.ToDecimal(miDataReader["ProgresoCertificacion"]);
                miProgreso.Total = Convert.ToDecimal(miDataReader["ProgresoTotal"]);
                miProgreso.CantidadUsuarios = Convert.ToDecimal(miDataReader["CantidadUsuarios"]);
                miProgreso.NombreAgencia = miDataReader["NombreAgencia"].ToString();
                miProgreso.Url_imagen = miDataReader["Imagen"].ToString();
                miProgreso.UsuariosActivos = Convert.ToInt32(miDataReader["UsuariosActivos"]);
                miProgreso.UsuariosInactivos = Convert.ToInt32(miDataReader["UsuariosInactivos"]);
                miProgreso.UsuariosTotales = Convert.ToInt32(miDataReader["UsuariosTotales"]);

            }
            miDataReader.Close();
            miConn.Close();
            return miProgreso;
        }
        public List<AgenteProgress> getProgresoAgente(int id_usuario)
        {

            List<AgenteProgress> miLista = new List<AgenteProgress>();
            AgenteProgress miAgente;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            /*
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;
            */
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_listado_agentes");//, misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miAgente = new AgenteProgress();

                    miAgente.Progreso = Convert.ToDecimal(miDataReader["Progreso"]);
                    miAgente.Nota = Convert.ToDecimal(miDataReader["Nota"]);
                    miAgente.Agencia= Convert.ToInt32(miDataReader["Agencia"]);
                    miAgente.Nombre = miDataReader["Nombre"].ToString();
                    miLista.Add(miAgente);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;


        }
        public List<AgenteProgress> getProgresoAgente(int desde, int hasta)
        {
            List<AgenteProgress> miLista = new List<AgenteProgress>();
            AgenteProgress miAgente;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            
            OleDbParameter[] misParametros = new OleDbParameter[2];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@desde";
            miParametro.Value = desde;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@hasta";
            miParametro.Value = hasta;
            misParametros[1] = miParametro;
            
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_listado_agentes", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miAgente = new AgenteProgress();

                    miAgente.Progreso = Convert.ToDecimal(miDataReader["Progreso"]);
                    miAgente.Nota = Convert.ToDecimal(miDataReader["Nota"]);
                    miAgente.Agencia = Convert.ToInt32(miDataReader["Agencia"]);
                    miAgente.Nombre = miDataReader["Nombre"].ToString();
                    miLista.Add(miAgente);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;
        }

        public List<UserProgress> getProgresoUsuarios()
        {
            List<UserProgress> miLista = new List<UserProgress>();
            UserProgress miUsuario;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_vista_users");

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new UserProgress();
                    miUsuario.Cod_usuario = Convert.ToInt32(miDataReader["Usuario"]);
                    miUsuario.Nombre = miDataReader["Nombre"].ToString();
                    miUsuario.Documento = miDataReader["Documento"].ToString();
                    miUsuario.Agencia = miDataReader["Agencia"].ToString();
                    miUsuario.UltimaActividad = miDataReader["UltimaActividad"].ToString();
                    miUsuario.Accesos = Convert.ToInt32(miDataReader["Accesos"]);
                    miUsuario.CursosAprobados = Convert.ToInt32(miDataReader["CursosAprobados"]);
                    miUsuario.Progreso = Convert.ToDecimal(miDataReader["Progreso"]);
                    miUsuario.Certificacion = miDataReader["Certificacion"].ToString();
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;
        }
        public List<UserProgress> getProgresoUsuarios(string Usuario)
        {
            List<UserProgress> miLista = new List<UserProgress>();
            UserProgress miUsuario;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.VarChar;
            miParametro.ParameterName = "@busqueda";
            miParametro.Value = Usuario;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_vista_users_filtro", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new UserProgress();
                    miUsuario.Cod_usuario = Convert.ToInt32(miDataReader["Usuario"]);
                    miUsuario.Nombre = miDataReader["Nombre"].ToString();
                    miUsuario.Documento = miDataReader["Documento"].ToString();
                    miUsuario.Agencia = miDataReader["Agencia"].ToString();
                    miUsuario.UltimaActividad = miDataReader["UltimaActividad"].ToString();
                    miUsuario.CursosAprobados = Convert.ToInt32(miDataReader["CursosAprobados"]);
                    miUsuario.Accesos = Convert.ToInt32(miDataReader["Accesos"]);
                    miUsuario.Progreso = Convert.ToDecimal(miDataReader["Progreso"]);
                    miUsuario.Certificacion = miDataReader["Certificacion"].ToString();
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;
        }

        public List<UserProgress> getProgresoUsuarios(int id_agencia)
        {
            List<UserProgress> miLista = new List<UserProgress>();
            UserProgress miUsuario;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_agencia";
            miParametro.Value = id_agencia;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_vista_users_agencia", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new UserProgress();
                    miUsuario.Cod_usuario = Convert.ToInt32(miDataReader["Usuario"]);
                    miUsuario.Nombre = miDataReader["Nombre"].ToString();
                    miUsuario.Documento = miDataReader["Documento"].ToString();
                    miUsuario.Agencia = miDataReader["Agencia"].ToString();
                    miUsuario.UltimaActividad = miDataReader["UltimaActividad"].ToString();
                    miUsuario.CursosAprobados = Convert.ToInt32(miDataReader["CursosAprobados"]);
                    miUsuario.Accesos = Convert.ToInt32(miDataReader["Accesos"]);
                    miUsuario.Progreso = Convert.ToDecimal(miDataReader["Progreso"]);
                    miUsuario.Certificacion = miDataReader["Certificacion"].ToString();
                    miUsuario.User = miDataReader["User"].ToString();
                    miUsuario.Password = miDataReader["Password"].ToString();
                    miUsuario.Estado = Convert.ToInt32(miDataReader["Estado"]) == 0 ? "Activo" : "Baja";
                    miUsuario.Activo = Convert.ToInt32(miDataReader["Activo"]);
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;
        }
        public List<UserProgress> getProgresoUsuarios(string Usuario, int id_agencia)
        {
            List<UserProgress> miLista = new List<UserProgress>();
            UserProgress miUsuario;

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[2];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.VarChar;
            miParametro.ParameterName = "@busqueda";
            miParametro.Value = Usuario;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_agencia";
            miParametro.Value = id_agencia;
            misParametros[1] = miParametro;
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_vista_users_agencia_filtro", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new UserProgress();
                    miUsuario.Cod_usuario = Convert.ToInt32(miDataReader["Usuario"]);
                    miUsuario.Nombre = miDataReader["Nombre"].ToString();
                    miUsuario.Documento = miDataReader["Documento"].ToString();
                    miUsuario.Agencia = miDataReader["Agencia"].ToString();
                    miUsuario.UltimaActividad = miDataReader["UltimaActividad"].ToString();
                    miUsuario.CursosAprobados = Convert.ToInt32(miDataReader["CursosAprobados"]);
                    miUsuario.Accesos = Convert.ToInt32(miDataReader["Accesos"]);
                    miUsuario.Progreso = Convert.ToDecimal(miDataReader["Progreso"]);
                    miUsuario.Certificacion = miDataReader["Certificacion"].ToString();
                    miUsuario.User = miDataReader["User"].ToString();
                    miUsuario.Password = miDataReader["Password"].ToString();
                    miUsuario.Estado = Convert.ToInt32(miDataReader["Estado"]) == 0 ? "Activo" : "Baja";
                    miUsuario.Activo = Convert.ToInt32(miDataReader["Activo"]);
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;
        }
        public DatosReporte getDatosReporte()
        {
            DatosReporte misDatos = new DatosReporte();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_datos_reportes");

            if (miDataReader.HasRows)
            {
                miDataReader.Read();


                misDatos.CantidadAgencias = Convert.ToInt32(miDataReader["cantAgencias"]);
                misDatos.CantidadCecs = Convert.ToInt32(miDataReader["cantCecs"]);
                misDatos.CantidadUsuarios = Convert.ToInt32(miDataReader["cantUsuarios"]);
                misDatos.CantidadOtros = Convert.ToInt32(miDataReader["cantOtros"]);
                misDatos.CantidadActivos = Convert.ToInt32(miDataReader["cantActivos"]);
                misDatos.CantidadInactivos = Convert.ToInt32(miDataReader["cantInactivos"]);

                
            }
            miDataReader.Close();
            miConn.Close();

            return misDatos;
            
        }

        public AgenteProgress getProgresoAgencia(int id_usuario)
        {


            AgenteProgress miAgente = new AgenteProgress();;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_progress_agencias", misParametros);

            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                miAgente.Progreso = Convert.ToDecimal(miDataReader["ProgresoTotal"]);
                
            }
            miDataReader.Close();
            miConn.Close();

            return miAgente;
        }
        public AgenteProgress getProgresoCec(int id_usuario)
        {

            AgenteProgress miAgente = new AgenteProgress(); ;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_progress_cecs", misParametros);

            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                miAgente.Progreso = Convert.ToDecimal(miDataReader["ProgresoTotal"]);

            }
            miDataReader.Close();
            miConn.Close();

            return miAgente;


        }

        public UserProgress getProgresoUsuario(int id_usuario) {
            UserProgress miUsuario = new UserProgress();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;


            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_datos_user", misParametros);

            if (miDataReader.HasRows)
            {
                miDataReader.Read();
                miUsuario = new UserProgress();
                miUsuario.Cod_usuario = Convert.ToInt32(miDataReader["IDUsuario"]);
                miUsuario.Nombre = miDataReader["Nombre"].ToString();
                miUsuario.Apellido = miDataReader["Apellido"].ToString();
                miUsuario.Documento = miDataReader["Documento"].ToString();
                miUsuario.Agencia = miDataReader["Agencia"].ToString();
                miUsuario.UltimaActividad = miDataReader["UltimaActividad"].ToString();
                miUsuario.Registro = miDataReader["Registro"].ToString();
                miUsuario.Accesos = Convert.ToInt32(miDataReader["Accesos"]);
                
            }
            miDataReader.Close();
            miConn.Close();

            var miProgreso =  new CursosData().getProgreso(id_usuario);
            miUsuario.Total = miProgreso.Total;
            miUsuario.ModuloInicial = miProgreso.ModuloInicial;
            miUsuario.Equipos = miProgreso.Equipos;
            miUsuario.OfertaYCampana = miProgreso.OfertaYCampana;


            return miUsuario;
        }
        public List<CourseProgress> getCertificacionMensual(int id_usuario)
        {

            List<CourseProgress> miLista = new List<CourseProgress>();
            CourseProgress miProgreso = new CourseProgress();

            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@cod_usuario";
            miParametro.Value = id_usuario;
            misParametros[0] = miParametro;

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_certificacion_mensual_usuario", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miProgreso = new CourseProgress();

                    miProgreso.Total = Convert.ToDecimal(miDataReader["Resultado"]);
                    miProgreso.Mes = Convert.ToInt32(miDataReader["MesNumerico"]);
                    miProgreso.Periodo = miDataReader["Periodo"].ToString();
                    miProgreso.MesTexto = miDataReader["MesTexto"].ToString();


                    miLista.Add(miProgreso);
                }
            }
            miDataReader.Close();
            miConn.Close();
            return miLista;
        }

        public List<UserProgress> getCertificacionesDeUsuariosDeAgenciaDelMes(int id_agencia, int mes, int ano)
        {

            List<UserProgress> miLista = new List<UserProgress>();
            UserProgress miUsuario;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[3];
            OleDbParameter miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_agencia";
            miParametro.Value = id_agencia;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@mes";
            miParametro.Value = mes;
            misParametros[1] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@ano";
            miParametro.Value = ano;
            misParametros[2] = miParametro;
            

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cert_usr_agencia_mes", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new UserProgress();
                    miUsuario.Cod_usuario = Convert.ToInt32(miDataReader["IDUsuario"]);
                    miUsuario.Nombre = miDataReader["Nombre"].ToString();
                    miUsuario.Apellido = miDataReader["Apellido"].ToString();
                    miUsuario.Documento = miDataReader["Documento"].ToString();
                    miUsuario.Certificacion = miDataReader["Certificacion"].ToString();
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;

        }

        public List<UserProgress> getCertificacionesAgencias(int mes, int ano)
        {
            List<UserProgress> miLista = new List<UserProgress>();
            UserProgress miUsuario;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[2];
            OleDbParameter miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@mes";
            miParametro.Value = mes;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@ano";
            miParametro.Value = ano;
            misParametros[1] = miParametro;


            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cert_agencias_report", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new UserProgress();
                    miUsuario.Agencia = miDataReader["Agencia"].ToString();
                    miUsuario.Certificacion = miDataReader["Resultado"].ToString();
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;

        }
        public List<UserProgress> getCertificacionesUsuariosAgencias(int mes, int ano)
        {
            List<UserProgress> miLista = new List<UserProgress>();
            UserProgress miUsuario;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[2];
            OleDbParameter miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@mes";
            miParametro.Value = mes;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@ano";
            miParametro.Value = ano;
            misParametros[1] = miParametro;


            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cert_usr_agencias_mes", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new UserProgress();
                    miUsuario.Agencia = miDataReader["Agencia"].ToString();
                    miUsuario.Certificacion = miDataReader["Certificacion"].ToString();
                    miUsuario.Nombre = miDataReader["Nombre"].ToString();
                    miUsuario.Apellido = miDataReader["Apellido"].ToString();
                    miUsuario.Documento = miDataReader["Documento"].ToString();
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;

        }
        public List<UserProgress> getCantidadUsuariosAgencias(int mes, int ano)
        {
            List<UserProgress> miLista = new List<UserProgress>();
            UserProgress miUsuario;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[2];
            OleDbParameter miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@mes";
            miParametro.Value = mes;
            misParametros[0] = miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@ano";
            miParametro.Value = ano;
            misParametros[1] = miParametro;


            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_cert_cant_users_agencia", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new UserProgress();
                    miUsuario.Agencia = miDataReader["Descripcion"].ToString();
                    miUsuario.Cantidad = Convert.ToInt32(miDataReader["Cantidad"]);
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;
        }
        public List<MailUsuario> getEstadoEmails()
        {
            List<MailUsuario> miLista = new List<MailUsuario>();
            MailUsuario miUsuario;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_estado_mails");

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new MailUsuario();
                    miUsuario.User = miDataReader["User"].ToString();
                    miUsuario.Nombre = miDataReader["Nombre"].ToString();
                    miUsuario.Apellido = miDataReader["Apellido"].ToString();
                    miUsuario.Password = miDataReader["Password"].ToString();
                    miUsuario.Email = miDataReader["Email"].ToString();
                    miUsuario.Agencia = miDataReader["Agencia"].ToString();
                    miUsuario.sn_enviado = Convert.ToInt32(miDataReader["sn_enviado"]);
                    miUsuario.FechaEnviado = Convert.ToDateTime(miDataReader["FechaEnviado"]).ToString("dd/MM/yyyy HH:mm");
                    miUsuario.TipoMail = miDataReader["TipoMail"].ToString();
                    
                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;

        }

        public List<MailUsuario> getEstadoEmailsFiltro(string usuario)
        {
            List<MailUsuario> miLista = new List<MailUsuario>();
            MailUsuario miUsuario;
            OleDbConnection miConn = Helpers.HelpersData.getConnection();

            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.VarChar;
            miParametro.ParameterName = "@txt_usuario";
            miParametro.Value = usuario;
            misParametros[0] = miParametro;


            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_estado_mails", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    miUsuario = new MailUsuario();
                    miUsuario.User = miDataReader["User"].ToString();
                    miUsuario.Nombre = miDataReader["Nombre"].ToString();
                    miUsuario.Apellido = miDataReader["Apellido"].ToString();
                    miUsuario.Password = miDataReader["Password"].ToString();
                    miUsuario.Email = miDataReader["Email"].ToString();
                    miUsuario.Agencia = miDataReader["Agencia"].ToString();
                    miUsuario.sn_enviado = Convert.ToInt32(miDataReader["sn_enviado"]);
                    miUsuario.FechaEnviado = Convert.ToDateTime(miDataReader["FechaEnviado"]).ToString("dd/MM/yyyy HH:mm");
                    miUsuario.TipoMail = miDataReader["TipoMail"].ToString();

                    miLista.Add(miUsuario);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;
        }

        public List<ReporteAltas> GetHistorialAltas(int id_agencia){

            List<ReporteAltas> miLista = new List<ReporteAltas>();
            ReporteAltas reporteAltas;
            
            OleDbConnection miConn = Helpers.HelpersData.getConnection();
            OleDbParameter[] misParametros = new OleDbParameter[1];
            OleDbParameter miParametro;

            miParametro = new OleDbParameter();
            miParametro.OleDbType = OleDbType.Integer;
            miParametro.ParameterName = "@id_agencia";
            miParametro.Value = id_agencia;
            misParametros[0] = miParametro;
            OleDbDataReader miDataReader = Helpers.HelpersData.getDataReader(miConn, "sp_get_historial_altas_agencia", misParametros);

            if (miDataReader.HasRows)
            {
                while (miDataReader.Read())
                {
                    
                    reporteAltas = new ReporteAltas();
                    reporteAltas.ID_Agencia = Convert.ToInt32(miDataReader["ID_Agencia"]);
                    reporteAltas.Agencia = miDataReader["Agencia"].ToString();
                    reporteAltas.Ano = Convert.ToInt32(miDataReader["Ano"]);
                    reporteAltas.Mes = Convert.ToInt32(miDataReader["Mes"]);
                    reporteAltas.Cantidad = Convert.ToInt32(miDataReader["Cantidad"]);
                    reporteAltas.MesTexto = miDataReader["MesTexto"].ToString();
                    
                    miLista.Add(reporteAltas);
                }
            }
            miDataReader.Close();
            miConn.Close();

            return miLista;

        }
    }
    
}