﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Data;
namespace Sidepro.Business
{
    public class ReportBusiness
    {
        CursosData cursosData = new CursosData();
        ReportData reportData = new ReportData();
        public CourseProgress getProgresoReportes(int id_usuario)
        {
            return cursosData.getProgresoReportes(id_usuario);
        }
        public CourseProgress getProgresoReportesAgencia(int id_usuario, int id_agencia)
        {
            return reportData.getProgresoReportesAgencia(id_usuario, id_agencia);
        }
        public List<CourseProgress> getProgresoMensual(int id_usuario)
        {
            return reportData.getProgresoMensual(id_usuario);
        }
        public List<CourseProgress> getCertMensuales()
        {
            return reportData.getCertMensuales();
        }
        public List<CourseProgress> getCertificacionMensual(int id_usuario)
        {
            return reportData.getCertificacionMensual(id_usuario);
        }
        public List<CourseProgress> getLogeoUsuarios()
        {
            return reportData.getLogeoUsuarios();
        }
        public List<CourseProgress> getProgresoMensualAgencia(int id_Agencia)
        {
            return reportData.getProgresoMensualAgencia(id_Agencia);
        }
        public List<CourseProgress> getProgresoMensualUsuariosAgencia(int id_agencia)
        {
            return reportData.getProgresoMensualUsuariosAgencia(id_agencia);
        }
        public List<AgenteProgress> getProgresoAgente(int id_usuario)
        {
            return reportData.getProgresoAgente(id_usuario);
        }
        public AgenteProgress getProgresoAgencia(int id_usuario)
        {
            return reportData.getProgresoAgencia(id_usuario);
        }
        public AgenteProgress getProgresoCec(int id_usuario)
        {
            return reportData.getProgresoCec(id_usuario);
        }
        public List<AgenteProgress> getProgresoAgente(int desde, int hasta)
        {
            return reportData.getProgresoAgente(desde, hasta);
        }
        public List<UserProgress> getProgresoUsuarios()
        {
            return reportData.getProgresoUsuarios();
        }
        public List<UserProgress> getProgresoUsuarios(string usuario, int id_agencia)
        {
            return reportData.getProgresoUsuarios(usuario, id_agencia);
        }
        public List<UserProgress> getProgresoUsuarios(string usuario)
        {
            return reportData.getProgresoUsuarios(usuario);
        }
        public List<UserProgress> getProgresoUsuarios(int id_agencia)
        {
            return reportData.getProgresoUsuarios(id_agencia);
        }       
        public DatosReporte getDatosReporte()
        {
            return reportData.getDatosReporte();
        }
        public UserProgress getProgresoUsuario(int id_agencia)
        {
            return reportData.getProgresoUsuario(id_agencia);
        }
        public List<UserProgress> getCertificacionesDeUsuariosDeAgenciaDelMes(int id_agencia, int mes, int ano)
        {
            return reportData.getCertificacionesDeUsuariosDeAgenciaDelMes(id_agencia, mes, ano);
        }
        public List<ReporteNovedades> GetReporteNovedades(string usuario)
        {
            return new NoticiaData().GetReporteNovedades(usuario);
        }
        public List<UserProgress> getCertificacionesAgencias(int mes, int ano)
        {
            return reportData.getCertificacionesAgencias(mes, ano);

        }
        public List<UserProgress> getCertificacionesUsuariosAgencias(int mes, int ano)
        {
            return reportData.getCertificacionesUsuariosAgencias(mes, ano);
        }
        public List<UserProgress> getCantidadUsuariosAgencias(int mes, int ano)
        {
            return reportData.getCantidadUsuariosAgencias(mes, ano);
        }
        public List<MailUsuario> getEstadoEmails()
        {
            return reportData.getEstadoEmails();
        }
        public List<MailUsuario> getEstadoEmailsFiltro(string usuario)
        {
            return reportData.getEstadoEmailsFiltro(usuario);
        }
        public List<ReporteAltas> GetHistorialAltas(int id_agencia)
        {
            return reportData.GetHistorialAltas(id_agencia);
        }
    }
}