﻿using Sidepro.Data;
using Sidepro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Business
{
    public class NoticiaBusiness
    {
        public Noticia GetNoticia(int id_modulo, int id_curso, int cod_usuario)
        {
            return new NoticiaData().GetNoticia(id_modulo, id_curso, cod_usuario);
        }
    }
}