﻿using Sidepro.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Data;
namespace Sidepro.Business
{
    public class CanalBusiness
    {
        CanalData canalData = new CanalData();
        public List<Canal> getCanales()
        {
            return canalData.getCanales();
        }
        public string agregarCanal(string Descripcion)
        {
            return canalData.agregarCanal(Descripcion);
        }
        public string modificarCanal(int cod_canal, string Descripcion)
        {
            return canalData.modificarCanal(cod_canal, Descripcion);
        }
    }
}