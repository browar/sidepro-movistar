﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Data;
namespace Sidepro.Business
{
    public class AgenciaBusiness
    {
        AgenciaData agenciaData = new AgenciaData();
        public List<Agencia> getAgencias()
        {
            return agenciaData.getAgencias();
        }
        public List<TipoAgencia> getTipoAgencias()
        {
            return agenciaData.getTipoAgencias();
        }
        public string agregarAgencia(string Agencia, string Documento, string Email, int TipoAgencia)
        {
            return agenciaData.agregarAgencia(Agencia, Documento, Email, TipoAgencia);
        }
        public string modificarAgencia(int id_agencia, string Agencia, string Documento, string Email, int TipoAgencia)
        {
            return agenciaData.modificarAgencia(id_agencia, Agencia, Documento, Email, TipoAgencia);
        }
    }
}