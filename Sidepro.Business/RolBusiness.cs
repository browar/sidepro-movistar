﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Data;
namespace Sidepro.Business
{
    public class RolBusiness
    {
        RolData rolData = new RolData();
        public List<Rol> getRoles()
        {
            return rolData.getRoles();
        }
        public string agregarRol(int cod_canal, string rol)
        {
            return rolData.agregarRol(cod_canal, rol);
        }
        public string modificarRol(int cod_canal, int id_rol, string rol)
        {
            return rolData.modificarRol(cod_canal, id_rol, rol);
        }
        public List<ModuloCursos> getRolModulos(int id_rol)
        {
            return rolData.getRolModulos(id_rol);
        }
        public List<Curso> getRolCursos(int id_rol, int id_modulo, int? id_curso)
        {
            return rolData.getRolCursos(id_rol, id_modulo, id_curso);
        }
        public List<ModuloCursos> getModulos(int id_rol)
        {
            return rolData.getModulos(id_rol);
        }
        public string agregarRolModulo(int id_rol, int id_modulo)
        {
            return rolData.agregarRolModulo(id_rol, id_modulo);
        }
        public string eliminarRolModulo(int id_modulo, int id_rol)
        {
            return rolData.eliminarRolModulo(id_modulo, id_rol);
        }
        public string getNombrePadre(int id_rol, int id_modulo, int? id_curso)
        {
            return rolData.getNombrePadre(id_rol, id_modulo, id_curso);
        }
        public List<Curso> getCursos(int id_rol, int id_modulo, int? id_curso)
        {
            return rolData.getCursos(id_rol, id_modulo, id_curso);
        }
        public List<Curso> getCursosDestacados(int cod_rol, int id_modulo, int? id_curso)
        {
            return rolData.getCursosDestacados(cod_rol, id_modulo, id_curso);
        }
        public string agregarRolCurso(int id_rol, int id_modulo, int id_curso, int? parent)
        {
            return rolData.agregarRolCurso(id_rol, id_modulo, id_curso, parent);
        }
        public string eliminarRolCurso(int id_modulo, int id_curso, int id_rol)
        {
            return rolData.eliminarRolCurso(id_modulo, id_curso, id_rol);
        }
        public List<Destacado> getDestacados()
        {
            return rolData.getDestacados();
        }
        public List<ModuloCursos> getModulosDestacados(int cod_rol)
        {
            return rolData.getModulosDestacados(cod_rol);
        }
        public List<TipoDestacado> getTipoDestacados()
        {
            return rolData.getTipoDestacados();
        }
        public string AgregarDestacado(int id_modulo, int? id_curso, int cod_rol, int cod_tipo_destacado, string url_imagen)
        {
            return rolData.AgregarDestacado(id_modulo, id_curso, cod_rol, cod_tipo_destacado, url_imagen);
        }
        public string EliminarDestacado(int id_destacado)
        {
            return rolData.EliminarDestacado(id_destacado);
        }
        public string ModificarDestacado(int id_destacado, int cod_tipo_destacado, string url_imagen, int orden)
        {
            return rolData.ModificarDestacado(id_destacado, cod_tipo_destacado, url_imagen, orden);
        }
        public List<Rol> getRolesDestacados()
        {
            return rolData.getRolesDestacados();
        }
    }
}