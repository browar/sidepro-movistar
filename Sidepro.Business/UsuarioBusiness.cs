﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Data;
namespace Sidepro.Business
{
    public class UsuarioBusiness
    {
        public MailUsuario DesbloquearUsuario(int cod_usuario)
        {
            return new UsuarioData().DesbloquearUsuario(cod_usuario);
        }
        public string getEmail(int cod_usuario)
        {
            return new UsuarioData().getEmail(cod_usuario);
        }
    }
}