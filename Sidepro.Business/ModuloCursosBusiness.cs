﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Data;
using Sidepro.Entities;
namespace Sidepro.Business
{
    public class ModuloCursosBusiness
    {
        private ModuloCursosData modulosData = new ModuloCursosData();
        public List<ModuloCursos> getGrupos(){
            return modulosData.getGrupos();
        }
        public List<ModuloCursos> getGrupoMenu(int id_usuario)
        {
            return modulosData.getGrupoMenu(id_usuario);
        }
        public List<ModuloCursos> getModulos()
        {
            return modulosData.getModulos();
        }
        public string agregarModulo(string Nombre, string Controller, string View, string Style, string Tags)
        {
            return modulosData.agregarModulo(Nombre, Controller, View, Style, Tags);
        }
        public string modificarModulo(int ID_Modulo, string Nombre, string Controller, string View, string Style, string Tags)
        {
            return modulosData.modificarModulo(ID_Modulo, Nombre, Controller, View, Style, Tags);
        }
        public string inhabilitarModulo(int ID_Modulo)
        {
            return modulosData.inhabilitarModulo(ID_Modulo);
        }
        public string habilitarModulo(int ID_Modulo)
        {
            return modulosData.habilitarModulo(ID_Modulo);
        }
    }
}