﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Data;
using Sidepro.Entities;
namespace Sidepro.Business
{
    public class HomeBusiness
    {
        HomeData homeData = new HomeData();
        public Usuario getUser(string user, string pass)
        {
            return homeData.getUser(user, pass);
        }
        public List<Certification> getCertificaciones(int id_usuario)
        {
            return homeData.getCertificaciones(id_usuario);
        }
        public List<Destacado> getDestacados(int id_usuario)
        {
            return homeData.getDestacados(id_usuario);
        }
    }
}