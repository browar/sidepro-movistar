﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Application;
using Sidepro.Entities;
using Sidepro.Application.Helpers;


namespace Sidepro.ViewModels
{

    public abstract class ViewModelBase 
    {
        public List<ModuloCursos> menu;
        public List<Curso> cursosHistorial;
        public string nombreUsuario;
        public string apellidoUsuario;
        public int id_usuario;
        public CourseProgress progreso;
        public ViewModelBase() {
            //CursosViewModel miViewModel = new CursosViewModel();
            CursosManagementService cursosManagementService = new CursosManagementService();
            ModuloCursosManagementService grupoCursosManagementService = new ModuloCursosManagementService();

            HttpCookie myCookie = HttpContext.Current.Request.Cookies.Get("UserSettings");
            
            if (myCookie == null)
            {
                id_usuario = -1;
                nombreUsuario = "";
                apellidoUsuario = "";
            }
            else
            {
                id_usuario = HelpersManagementService.getCookie(myCookie).ID_Usuario;//Convert.ToInt32(myCookie["Codigo"]);
                nombreUsuario = HelpersManagementService.getCookie(myCookie).Nombre;// myCookie["Nombre"].ToString();
                apellidoUsuario = HelpersManagementService.getCookie(myCookie).Apellido;// myCookie["Apellido"].ToString();
            }

            if (HttpContext.Current.Session["Menu"] == null)
            {
                menu = grupoCursosManagementService.getGrupoMenu(id_usuario);
                HttpContext.Current.Session["Menu"] = menu;
            } else {
                menu = (List<ModuloCursos>) HttpContext.Current.Session["Menu"];
            }
            //if (HttpContext.Current.Session["Historial"] == null)
            //{
                cursosHistorial = cursosManagementService.getCursosHistorial(id_usuario);
                HttpContext.Current.Session["Historial"] = cursosHistorial;

                progreso = cursosManagementService.getProgreso(id_usuario);
            //}
            //else
           // {
            //    cursosHistorial = (List<Curso>)HttpContext.Current.Session["Historial"];
           // }

            
        }
    }

    public class CursosViewModelController 
    {
        public CursosViewModel getCursosViewModel(int id_modulo, int id_curso, int id_usuario) 
        {
            CursosViewModel miViewModel = new CursosViewModel();
            CursosManagementService cursosManagementService = new CursosManagementService();
            LeccionManagementService leccionManagementService = new LeccionManagementService();

            miViewModel.leccionesDelCurso = leccionManagementService.getLeccionesDeCurso(id_modulo, id_curso, id_usuario);
            if (miViewModel.leccionesDelCurso.Count > 0)
            {
                miViewModel.nombreCurso = miViewModel.leccionesDelCurso.First().NombreCurso;
                miViewModel.imagenCurso = miViewModel.leccionesDelCurso.First().ImagenCurso;
            }
            return miViewModel;
        }
    }
    public class ModuleCursosViewModelController
    {
        public ModuleCursosViewModel getCursosViewModel(int id_modulo, int? id_curso, int id_usuario)
        {
            CursosManagementService cursosManagementService = new CursosManagementService();
            ModuleCursosViewModel miViewModel = new ModuleCursosViewModel();
            miViewModel.moduleCourses = cursosManagementService.getCursosDelModulo(id_modulo, id_curso, id_usuario);
            if (miViewModel.moduleCourses.Count > 0) {
                miViewModel.nombreModulo = miViewModel.moduleCourses.First().Nombre_Modulo;
            }
            return miViewModel;
        }        
    }
    public class ReportViewModelController {
        
        public ReportViewModel getReportViewModel(int id_usuario)
        {
            ReportViewModel miViewModel = new ReportViewModel();
            ReportManagementService reportManagementService = new ReportManagementService();
            miViewModel.progreso = reportManagementService.getProgresoReportes(id_usuario);
            miViewModel.progresoMensual = reportManagementService.getProgresoMensual(id_usuario);
            miViewModel.CertMensuales = reportManagementService.getCertMensuales();
            miViewModel.progresoAgente = reportManagementService.getProgresoAgente(id_usuario);
            miViewModel.progresoAgencia = reportManagementService.getProgresoAgencia(id_usuario);
            miViewModel.progresoCec = reportManagementService.getProgresoCec(id_usuario);
            miViewModel.progresoUsuario = reportManagementService.getProgresoUsuarios();
            miViewModel.logeos = reportManagementService.getLogeoUsuarios();
            miViewModel.datosGenerales = reportManagementService.getDatosReporte();
            miViewModel.reporteNovedades = reportManagementService.GetReporteNovedades("");
            miViewModel.estadoMails = reportManagementService.getEstadoEmails();
            return miViewModel;
        }
        public ReportAgenciaViewModel getReportAgenciaViewModel(int id_usuario, int id_agencia)
        {
            ReportAgenciaViewModel miViewModel = new ReportAgenciaViewModel();
            ReportManagementService reportManagementService = new ReportManagementService();
            miViewModel.progreso = reportManagementService.getProgresoAgenciaReportes(id_usuario, id_agencia);
            miViewModel.progresoMensual = reportManagementService.getProgresoMensualAgencia(id_agencia);
            miViewModel.progresoUsuario = reportManagementService.getProgresoUsuarios(id_agencia);
            miViewModel.progresoMensualUsuariosAgencia = reportManagementService.getProgresoMensualUsuariosAgencia(id_agencia);
            miViewModel.id_agencia = id_agencia;
            miViewModel.reporteAltas = reportManagementService.GetHistorialAltas(id_agencia);
            return miViewModel;
        }
        public ReportUsuarioViewModel getReportUsuarioViewModel(int id_usuario) {
            ReportUsuarioViewModel miViewModel = new ReportUsuarioViewModel();
            ReportManagementService reportManagementService = new ReportManagementService();

            miViewModel.progresoUsuario = reportManagementService.getProgresoUsuario(id_usuario);
            miViewModel.progresoMensual = reportManagementService.getCertificacionMensual(id_usuario);

            return miViewModel;
        }

    }
    public class ABMViewModelController {
        public ABMViewModel getABMViewModel(int id_modulo, int? id_curso)
        {
            ABMViewModel miViewModel = new ABMViewModel();
            
            if (id_curso == null){
                var miModulo =  new ModuloCursosManagementService().getModulos().Where(x => x.Id_modulo == id_modulo).First();
                miViewModel.id_modulo = miModulo.Id_modulo;
                miViewModel.Titulo = miModulo.Nombre;
                miViewModel.id_curso = id_curso;
            }else {
                var temp = new CursosManagementService().getCursos(id_modulo, id_curso);
                if (temp.Count() == 0) { 
                    var miModulo = new CursosManagementService().getCursos(id_modulo,null).Where(x =>x.Id_curso == id_curso);
                    if (miModulo.Count() > 0) {
                        var miModulo2 = miModulo.First();
                        miViewModel.id_modulo = miModulo2.Id_modulo;
                        miViewModel.id_curso = miModulo2.Id_curso;
                        miViewModel.Titulo = miModulo2.Nombre;
                    }
                    else
                    {
                        miViewModel.id_modulo = id_modulo;
                        miViewModel.id_curso = id_curso;
                        miViewModel.Titulo = "No hay ningun curso asociado";

                    }
                }else{
                    var temp2 = temp.First();
                    miViewModel.id_modulo = temp2.Id_modulo;
                    miViewModel.id_curso = id_curso;
                    miViewModel.Titulo = temp2.Nombre_Padre;                
                }

            }
                
            return miViewModel;

        }
        public ABMViewModel getLessonABMViewModel(int id_modulo, int id_curso) { 

            ABMViewModel miViewModel = new ABMViewModel();

            var miModulo =  new CursosManagementService().getCursos(id_modulo, -1).Where(x => x.Id_curso == id_curso).First();
            miViewModel.id_modulo = miModulo.Id_modulo;
            miViewModel.Titulo = miModulo.Nombre;
            miViewModel.id_curso = id_curso;
            return miViewModel;
            
        }
        public ABMViewModel getQuestionABMViewModel(int id_modulo, int id_curso, int id_leccion)
        {
            ABMViewModel miViewModel = new ABMViewModel();

            var miModulo = new LeccionManagementService().getLecciones(id_modulo, id_curso).Where(x => x.Id_leccion== id_leccion).First();
            miViewModel.id_modulo = miModulo.Id_modulo;
            miViewModel.Titulo = miModulo.Nombre;
            miViewModel.id_curso = id_curso;
            miViewModel.id_leccion = id_leccion;
            return miViewModel;
        }
        public ABMViewModel getAnswerABMViewModel(int id_modulo, int id_curso, int id_leccion, int id_pregunta)
        {
            ABMViewModel miViewModel = new ABMViewModel();

            var miModulo = new LeccionManagementService().GetPregunta(id_modulo, id_curso, id_leccion).Where(x => x.Id_pregunta == id_pregunta).First();
            miViewModel.id_modulo = miModulo.Id_modulo;
            miViewModel.Titulo = miModulo.Nombre;
            miViewModel.id_curso = id_curso;
            miViewModel.id_leccion = id_leccion;
            miViewModel.id_pregunta = id_pregunta;
            return miViewModel;
        }
        public RolModuleViewModel getRolModuleViewModel(int id_rol){
            RolModuleViewModel miViewModel = new RolModuleViewModel();
            miViewModel.id_rol = id_rol;
            return miViewModel;
        }
        public RolCourseViewModel getRolCourseViewModel(int id_rol, int id_modulo, int? id_curso)
        {
            RolCourseViewModel miViewModel = new RolCourseViewModel();
            RolManagementService rolManagementService = new RolManagementService();
            miViewModel.id_rol = id_rol;
            miViewModel.id_modulo = id_modulo;
            miViewModel.id_curso = id_curso;
            miViewModel.NombrePadre = rolManagementService.getNombrePadre(id_rol, id_modulo, id_curso);
            return miViewModel;
        }
    }

    public class HelperViewModelController
    {
        public HelperViewModel getHelperViewModel(int id_agencia, int mes, int ano)
        {
            HelperViewModel miViewModel = new HelperViewModel();
            ReportManagementService reportManagementService = new ReportManagementService();
            miViewModel.id_agencia = id_agencia;
            miViewModel.mes = mes;
            miViewModel.progresoUsuarios = reportManagementService.getCertificacionesDeUsuariosDeAgenciaDelMes(id_agencia,mes, ano);
            
            return miViewModel;
        }
    }
    public class HomeViewModelController
    {
        public HomeViewModel getHomeViewModel(int id_usuario)
        {
            HomeViewModel miViewModel = new HomeViewModel();
            miViewModel.id_usuario = id_usuario;
            miViewModel.destacados = new HomeManagementService().getDestacados(id_usuario);

            return miViewModel;
        }
    }
    public class CursosViewModel :ViewModelBase
    {
        public List<Leccion> leccionesDelCurso; 
        public bool ImNotEmpty = true;
        public string nombreCurso = "";
        public string imagenCurso = "";

    }
    public class ModuleCursosViewModel : ViewModelBase
    {
        //public List<GrupoCursos> grupoCursos { get; set; }
        public bool ImNotEmpty = true;
        public List<Curso> moduleCourses;
        public string nombreModulo = "";
    }
    public class indexViewModel : ViewModelBase
    { 

    
    }
    public class ReportViewModel : ViewModelBase
    {
        public List<CourseProgress> progresoMensual = new List<CourseProgress>();
        public List<CourseProgress> CertMensuales = new List<CourseProgress>();
        public List<AgenteProgress> progresoAgente = new List<AgenteProgress>();
        public List<UserProgress> progresoUsuario = new List<UserProgress>();
        public List<CourseProgress> logeos = new List<CourseProgress>();
        public DatosReporte datosGenerales = new DatosReporte();
        public AgenteProgress progresoAgencia = new AgenteProgress();
        public AgenteProgress progresoCec = new AgenteProgress();
        public List<ReporteNovedades> reporteNovedades = new List<ReporteNovedades>();
        public List<MailUsuario> estadoMails = new List<MailUsuario>();
        
        
        

    }
    public class ReportAgenciaViewModel : ViewModelBase
    {
        public string nombreAgencia;
        public List<CourseProgress> progresoMensual = new List<CourseProgress>();
        public List<UserProgress> progresoUsuario = new List<UserProgress>();
        public List<CourseProgress> progresoMensualUsuariosAgencia = new List<CourseProgress>();
        public List<ReporteAltas> reporteAltas = new List<ReporteAltas>();
        public int id_agencia;
    }
    public class ReportUsuarioViewModel : ViewModelBase
    {
        public UserProgress progresoUsuario = new UserProgress();
        public List<CourseProgress> progresoMensual = new List<CourseProgress>();
    }

    public class ABMViewModel : ViewModelBase
    {
        public int id_modulo;
        public string Titulo;
        public int? id_curso;
        public int id_leccion;
        public int id_pregunta;

    }
    public class RolModuleViewModel : ViewModelBase
    {
        public int id_rol;

    }
    public class RolCourseViewModel : ViewModelBase
    {
        public int id_rol;
        public int id_modulo;
        public int? id_curso;
        public string NombrePadre;

    }
    public class HelperViewModel : ViewModelBase
    {
        public int id_agencia = 0;
        public int mes = 0;
        public List<UserProgress> progresoUsuarios;

    }
    public class HomeViewModel : ViewModelBase
    {
        public int cod_usuario;
        public List<Destacado> destacados;
    }
}