﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Optimization;

namespace Sidepro
{
    public class BundleConfig
    {
        // Para obtener más información acerca de Bundling, consulte http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            AddDefaultIgnorePatterns(bundles.IgnoreList);

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryABM").Include(
                        "~/Scripts/jquery-1.11.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));


            var bundle = new Bundle("~/bundles/jslayout");
            bundle.Orderer = new AsIsBundleOrderer();
            bundle
                .Include("~/Scripts/jquery.featherlight.js")
                .Include("~/Scripts/chart.min.js")
                .Include("~/Scripts/masonry.pkgd.min.js")
                .Include("~/Scripts/jquery.dotdotdot.min.js")
                .Include("~/Scripts/perfect-scrollbar.min.js")
                .Include("~/Scripts/jquery.tooltipster.min.js")
                .Include("~/Scripts/jquery.slick.min.js")
                .Include("~/Scripts/scripts.js");
            bundles.Add(bundle);

            bundle = new Bundle("~/bundles/jslayoutReport");
            bundle.Orderer = new AsIsBundleOrderer();
            bundle
                .Include("~/Scripts/Report/jquery.featherlight.js")
                .Include("~/Scripts/Report/chart.min.js")
                .Include("~/Scripts/Report/masonry.pkgd.min.js")
                .Include("~/Scripts/Report/jquery.dotdotdot.min.js")
                .Include("~/Scripts/Report/perfect-scrollbar.min.js")
                .Include("~/Scripts/Report/jquery.tooltipster.min.js")
                .Include("~/Scripts/Report/jquery.slick.min.js")
                .Include("~/Scripts/Report/scripts.js");
            bundles.Add(bundle);

            bundle = new Bundle("~/bundles/jslayoutABM");
            bundle.Orderer = new AsIsBundleOrderer();
            bundle
                
                .Include("~/Scripts/Report/chart.min.js")
                .Include("~/Scripts/Report/masonry.pkgd.min.js")
                .Include("~/Scripts/Report/jquery.dotdotdot.min.js")
                .Include("~/Scripts/Report/perfect-scrollbar.min.js")
                .Include("~/Scripts/Report/jquery.tooltipster.min.js")
                .Include("~/Scripts/Report/jquery.slick.min.js")
                .Include("~/Scripts/Report/scripts.js")
               .Include("~/Scripts/Report/jquery.featherlight.js")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/bootstrap.min.js")
                ;
            bundles.Add(bundle);

            // Utilice la versión de desarrollo de Modernizr para desarrollar y obtener información sobre los formularios. De este modo, estará
            // preparado para la producción y podrá utilizar la herramienta de creación disponible en http://modernizr.com para seleccionar solo las pruebas que necesite.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/css/style.css")); //"~/Content/site.css",
            bundles.Add(new StyleBundle("~/ReportContents/css").Include("~/ReportContents/css/style.css")); //"~/Content/site.css",

            bundle = new Bundle("~/ABMContents/css");
            bundle.Orderer = new AsIsBundleOrderer();
            bundle
                //.Include("~/Content/css/Layout.css")
                .Include("~/Content/css/bootstrap/bootstrap.css")
                .Include("~/Content/css/bootstrap/bootstrap.min.css")
                .Include("~/ReportContents/css/style.css")
                
               ; 
            bundles.Add(bundle);

            //bundles.Add(new StyleBundle("~/ABMContents/css").Include("~/Content/css/style.css")); //"~/Content/site.css",

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/bundles/index").Include(
                        "~/Scripts/jquery-1.11.3.min.js", //8.2
                        "~/Scripts/jquery.featherlight.js",
                        "~/Scripts/chart.min.js",
                        "~/Scripts/masonry.pkgd.min.js",
                        "~/Scripts/jquery.dotdotdot.min.js",
                        "~/Scripts/perfect-scrollbar.min.js",
                        "~/Scripts/jquery.tooltipster.min.js",
                        "~/Scripts/scripts.js",
                        "~/Scripts/jquery.slick.min.js"
                        ));
        }
        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
                throw new ArgumentNullException("ignoreList");
            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");
            ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            //ignoreList.Ignore("*.min.js", OptimizationMode.WhenDisabled);
            ignoreList.Ignore("*.min.css", OptimizationMode.WhenDisabled);
        }
    }
    public class AsIsBundleOrderer : IBundleOrderer
    {
        public virtual IEnumerable<FileInfo> OrderFiles(BundleContext context, IEnumerable<FileInfo> files)
        {
            return files;
        }
    }

}