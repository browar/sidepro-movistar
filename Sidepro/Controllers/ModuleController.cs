﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sidepro.Application;
using Sidepro.ViewModels;
using Sidepro.Entities;
using Sidepro.Application.Helpers;
namespace Sidepro.Controllers
{
    public class ModuleController : Controller
    {

        ModuleCursosViewModelController moduleVieweModelController = new ModuleCursosViewModelController();

        public ActionResult Module(int? id_module, int? id_curso)
        {
            ViewBag.Message = "Página de Modulos";
            int id_usuario = obtenerUsuario();
            return View(moduleVieweModelController.getCursosViewModel((int)id_module, id_curso, id_usuario));
        }
        public int obtenerUsuario()
        {
            HttpCookie myCookie = Request.Cookies["UserSettings"];
            int id_usuario = HelpersManagementService.getCookie(myCookie).ID_Usuario;
            /*
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
             */ 
            return id_usuario;
        }
        public ActionResult DescargarNoticia(int id_modulo, int id_curso)
        {

            int cod_usuario = obtenerUsuario();
            Noticia miNoticia = new NoticiaManagementService().GetNoticia(id_modulo, id_curso, cod_usuario);
            string filename = miNoticia.FileName;// "File1.pdf";
            string filepath = AppDomain.CurrentDomain.BaseDirectory + miNoticia.Path;// "/Images/Noticias/" + filename;
            byte[] filedata = System.IO.File.ReadAllBytes(filepath);
            string contentType = MimeMapping.GetMimeMapping(filepath);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = filename,
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(filedata, contentType);
        }

    }
}
