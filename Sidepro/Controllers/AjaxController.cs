﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using Sidepro.Application;
using Sidepro.ViewModels;
using Sidepro.Application.Helpers;
namespace Sidepro.Controllers
{
    public class AjaxController : Controller
    {
        //
        // GET: /Ajax/

        public ActionResult help()
        {
            return View();
        }
        public ActionResult certificacionesAgencia(int id_agencia, int mes, int ano)
        {
            HelperViewModel miViewModel = new HelperViewModelController().getHelperViewModel(id_agencia,mes,ano);
            return View(miViewModel);
        }
        public ActionResult EnviarMailConsulta(string Consulta)
        {
            bool result = true;
            HelperManagementService helperManagementService = new HelperManagementService();
            //int cod_usuario = obtenerUsuario();

            HttpCookie myCookie = Request.Cookies["UserSettings"];
            var cookie = HelpersManagementService.getCookie(myCookie);
            string Email = new UsuarioManagementService().getEmail(cookie.ID_Usuario);
            string body = getBodyMailConsulta(cookie.Nombre, cookie.Apellido, cookie.ID_Usuario, Consulta, Email);
            string title = "Consulta de Usuario Sidepro";
            string rta = new HelperManagementService().enviarMailGmail(@"mibrowar@gmail.com, agustinrq@amedia.com.ar", title, body);
            if (rta == ""){
                result = true;
            }else{
                result = false;
            }
            //result = helperManagementService.enviarMail(cod_usuario, "no-reply@sidepro.com.ar", "mibrowar@gmail.com", "mibrowar@gmail.com", "Testeando Mail", Consulta);
            //string pepe = SendUserMail("no-reply@s184-168-147-58.secureserver.net", "mibrowar@gmail.com", "probando, probando", "no-reply@s184-168-147-58.secureserver.net", "titulo");
            return Json(new { resultado = result}, JsonRequestBehavior.AllowGet);
        }
       
       public string SendUserMail(string fromad, string toad, string body, string header, string subjectcontent)
       {
           string result = "";
           MailMessage usermail = Mailbodplain(fromad, toad, body, header, subjectcontent);
           SmtpClient client = new SmtpClient();
           //Add the Creddentials- use your own email id and password
           client.Credentials = new System.Net.NetworkCredential("amedia", "Agus221#"); ;

           client.Host = "s184-168-147-58.secureserver.net";// "smtp.gmail.com";
           client.Port = 25;
           //client.EnableSsl = true;
           try
           {
               client.Send(usermail);
               result = "todoOk";
           }
           catch (Exception ex)
           {
               result = ex.Message;
           } // end try

           return result;
 
       }
       public MailMessage Mailbodplain(string fromad, string toad, string body, string header, string subjectcontent)
       {
           System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
           try
           {
               string from = fromad;
               string to = toad;
               mail.To.Add(to);
               mail.From = new MailAddress( from, header, System.Text.Encoding.UTF8);
               mail.Subject = subjectcontent;
               mail.SubjectEncoding = System.Text.Encoding.UTF8;
               mail.Body = body;
               mail.BodyEncoding = System.Text.Encoding.UTF8;
               mail.IsBodyHtml = true;
               mail.Priority = MailPriority.High;
           }
           catch
           {
               throw;
           }
           return mail;
       }
       public int obtenerUsuario()
       {
           HttpCookie myCookie = Request.Cookies["UserSettings"];
           int id_usuario = HelpersManagementService.getCookie(myCookie).ID_Usuario;

           /*
           int id_usuario = -1;
           if (HttpContext.Session["IDUsuario"] != null)
           {
               id_usuario = (int)HttpContext.Session["IDUsuario"];
           }
           else
           {
               id_usuario = new indexViewModel().id_usuario;
           }
            */
           return id_usuario;
       }


       public string getBodyMailConsulta(string Nombre, string Apellido, int cod_usuario, string Consulta, string Email)
       {

           string body = "";
           body = body + @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional //EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html><head>";
           body = body + @"            <title>Reporte General - Portal Training</title>";
           body = body + @"            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
           body = body + @"            <style type='text/css'>";
           body = body + @"        .font-sans-serif{font-family:sans-serif}.font-avenir{font-family:Avenir,sans-serif}.mso .font-avenir{font-family:sans-serif!important}.font-lato{font-family:Lato,Tahoma,sans-serif}.mso .font-lato{font-family:Tahoma,sans-serif!important}.font-cabin{font-family:Cabin,Avenir,sans-serif}.mso .font-cabin{font-family:sans-serif!important}.font-open-Sans{font-family:'Open Sans',sans-serif}.mso .font-open-Sans{font-family:sans-serif!important}.font-Raleway{font-family:Raleway,Tahoma,sans-serif}.mso .font-Raleway{font-family:Tahoma,sans-serif!important}.font-ubuntu{font-family:Ubuntu,sans-serif}.mso .font-ubuntu{font-family:sans-serif!important}.font-pt-sans{font-family:'PT Sans','Trebuchet MS',sans-serif}.mso .font-pt-sans{font-family:'Trebuchet MS',sans-serif!important}.font-georgia{font-family:Georgia,serif}.font-merriweather{font-family:Merriweather,Georgia,serif}.mso .font-merriweather{font-family:Georgia,serif!important}.font-bitter{font-family:Bitter,Georgia,serif}.mso .font-bitter{font-family:Georgia,serif!important}.font-pt-serif{font-family:'PT Serif',Georgia,serif}.mso .font-pt-serif{font-family:Georgia,serif!important}.font-pompiere{font-family:Pompiere,'Trebuchet MS',sans-serif}.mso .font-pompiere{font-family:'Trebuchet MS',sans-serif!important}.font-Raleway-slab{font-family:'Raleway Slab',Georgia,serif}.mso .font-Raleway-slab{font-family:Georgia,serif!important}@media only screen and (max-width: 620px){.wrapper .column .size-8{font-size:8px!important;line-height:14px!important}.wrapper .column .size-9{font-size:9px!important;line-height:16px!important}.wrapper .column .size-10{font-size:10px!important;line-height:18px!important}.wrapper .column .size-11{font-size:11px!important;line-height:19px!important}.wrapper .column .size-12{font-size:12px!important;line-height:19px!important}.wrapper .column .size-13{font-size:13px!important;line-height:21px!important}.wrapper .column .size-14{font-size:14px!important;line-height:21px!important}.wrapper .column .size-15{font-size:15px!important;line-height:23px!important}.wrapper .column .size-16{font-size:16px!important;line-height:24px!important}.wrapper .column .size-17{font-size:17px!important;line-height:26px!important}.wrapper .column .size-18{font-size:17px!important;line-height:26px!important}.wrapper .column .size-20{font-size:17px!important;line-height:26px!important}.wrapper .column .size-22{font-size:18px!important;line-height:26px!important}.wrapper .column .size-24{font-size:20px!important;line-height:28px!important}.wrapper .column .size-26{font-size:22px!important;line-height:31px!important}.wrapper .column .size-28{font-size:24px!important;line-height:32px!important}.wrapper .column .size-30{font-size:26px!important;line-height:34px!important}.wrapper .column .size-32{font-size:28px!important;line-height:36px!important}.wrapper .column .size-34{font-size:30px!important;line-height:38px!important}.wrapper .column .size-36{font-size:30px!important;line-height:38px!important}.wrapper .column .size-40{font-size:32px!important;line-height:40px!important}.wrapper .column .size-44{font-size:34px!important;line-height:43px!important}.wrapper .column .size-48{font-size:36px!important;line-height:43px!important}.wrapper .column .size-56{font-size:40px!important;line-height:47px!important}.wrapper .column .size-64{font-size:44px!important;line-height:50px!important}}body{margin:0;padding:0;min-width:100%}.mso body{mso-line-height-rule:exactly}.no-padding .wrapper .column .column-top,.no-padding .wrapper .column .column-bottom{font-size:0;line-height:0}table{border-collapse:collapse;border-spacing:0}td{padding:0;vertical-align:top}.spacer,.border{font-size:1px;line-height:1px}.spacer{width:100%}img{border:0;-ms-interpolation-mode:bicubic}.image{font-size:12px;mso-line-height-rule:at-least}.image img{display:block}.logo{mso-line-height-rule:at-least}.logo img{display:block}strong{font-weight:700}h1,h2,h3,p,ol,ul,blockquote,.image{font-style:normal;font-weight:400}ol,ul,li{padding-left:0}blockquote{margin-left:0;margin-right:0;padding-right:0}.column-top,.column-bottom{font-size:32px;line-height:32px;transition-timing-function:cubic-bezier(0,0,0.2,1);transition-duration:150ms;transition-property:all}.half-padding .column .column-top,.half-padding .column .column-bottom{font-size:16px;line-height:16px}.column{text-align:left}.contents{table-layout:fixed;width:100%}.padded{padding-left:32px;padding-right:32px;word-break:break-word;word-wrap:break-word}.wrapper{display:table;table-layout:fixed;width:100%;min-width:620px;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}.wrapper a{transition:opacity .2s ease-in}table.wrapper{table-layout:fixed}.one-col,.two-col,.three-col{margin-left:auto;margin-right:auto;width:600px}.centered{margin-left:auto;margin-right:auto}.btn a{border-radius:3px;display:inline-block;font-size:14px;font-weight:700;line-height:24px;padding:13px 35px 12px;text-align:center;text-decoration:none!important}.btn a:hover{opacity:.8}.two-col .btn a{font-size:12px;line-height:22px;padding:10px 28px}.three-col .btn a,.wrapper .narrower .btn a{font-size:11px;line-height:19px;padding:6px 18px 5px}@media only screen and (max-width: 620px){.btn a{display:block!important;font-size:14px!important;line-height:24px!important;padding:13px 10px 12px!important}}.two-col .column{width:300px}.two-col .first .padded{padding-left:32px;padding-right:16px}.two-col .second .padded{padding-left:16px;padding-right:32px}.three-col .column{width:200px}.three-col .first .padded{padding-left:32px;padding-right:8px}.three-col .second .padded{padding-left:20px;padding-right:20px}.three-col .third .padded{padding-left:8px;padding-right:32px}.wider{width:400px}.narrower{width:200px}@media only screen and (min-width: 0){.wrapper{text-rendering:optimizeLegibility}}@media only screen and (max-width: 620px){[class=wrapper]{min-width:320px!important;width:100%!important}[class=wrapper] .one-col,[class=wrapper] .two-col,[class=wrapper] .three-col{width:320px!important}[class=wrapper] .column,[class=wrapper] .gutter{display:block;float:left;width:320px!important}[class=wrapper] .padded{padding-left:20px!important;padding-right:20px!important}[class=wrapper] .block{display:block!important}[class=wrapper] .hide{display:none!important}[class=wrapper] .image img{height:auto!important;width:100%!important}}.footer{width:100%}.footer .inner{padding:58px 0 29px;width:600px}.footer .left td,.footer .right td{font-size:12px;line-height:22px}.footer .left td{text-align:left;width:400px}.footer .right td{max-width:200px;mso-line-height-rule:at-least}.footer .links{line-height:26px;margin-bottom:26px;mso-line-height-rule:at-least}.footer .links a:hover{opacity:.8}.footer .links img{vertical-align:middle}.footer .address{margin-bottom:18px}.footer .campaign{margin-bottom:18px}.footer .campaign a{font-weight:700;text-decoration:none}.footer .sharing div{margin-bottom:5px}.wrapper .footer .fblike,.wrapper .footer .tweet,.wrapper .footer .linkedinshare,.wrapper .footer .forwardtoafriend{background-repeat:no-repeat;background-size:200px 56px;border-radius:2px;color:#fff;display:block;font-size:11px;font-weight:700;line-height:11px;padding:8px 11px 7px 28px;text-align:left;text-decoration:none}.wrapper .footer .fblike:hover,.wrapper .footer .tweet:hover,.wrapper .footer .linkedinshare:hover,.wrapper .footer .forwardtoafriend:hover{color:#fff!important;opacity:.8}.footer .fblike{background-image:url(https://i3.createsend1.com/static/eb/master/03-fresh/imgf/fblike.png)}.footer .tweet{background-image:url(https://i4.createsend1.com/static/eb/master/03-fresh/imgf/tweet.png)}.footer .linkedinshare{background-image:url(https://i5.createsend1.com/static/eb/master/03-fresh/imgf/lishare.png)}.footer .forwardtoafriend{background-image:url(https://i6.createsend1.com/static/eb/master/03-fresh/imgf/forward.png)}@media only screen and (-webkit-min-device-pixel-ratio: 2),only screen and (min--moz-device-pixel-ratio: 2),only screen and (-o-min-device-pixel-ratio: 21),only screen and (min-device-pixel-ratio: 2),only screen and (min-resolution: 192dpi),only screen and (min-resolution: 2dppx){.footer .fblike{background-image:url(https://i7.createsend1.com/static/eb/master/03-fresh/imgf/fblike@2x.png)!important}.footer .tweet{background-image:url(https://i8.createsend1.com/static/eb/master/03-fresh/imgf/tweet@2x.png)!important}.footer .linkedinshare{background-image:url(https://i9.createsend1.com/static/eb/master/03-fresh/imgf/lishare@2x.png)!important}.footer .forwardtoafriend{background-image:url(https://i10.createsend1.com/static/eb/master/03-fresh/imgf/forward@2x.png)!important}}@media only screen and (max-width: 620px){.footer{width:320px!important}.footer td{display:none}.footer .inner,.footer .inner td{display:block;text-align:center!important;max-width:320px!important;width:320px!important}.footer .sharing{margin-bottom:40px}.footer .sharing div{display:inline-block}.footer .fblike,.footer .tweet,.footer .linkedinshare,.footer .forwardtoafriend{display:inline-block!important}}.wrapper h1,.wrapper h2,.wrapper h3,.wrapper p,.wrapper ol,.wrapper ul,.wrapper li,.wrapper blockquote,.image,.btn,.divider{margin-bottom:0;margin-top:0}.wrapper .column h1 + *{margin-top:20px}.wrapper .column h2 + *{margin-top:16px}.wrapper .column h3 + *{margin-top:12px}.wrapper .column p + *,.wrapper .column ol + *,.wrapper .column ul + *,.wrapper .column blockquote + *,.image + .contents td > :first-child{margin-top:24px}.contents:nth-last-child(n+3) h1:last-child,.no-padding .contents:nth-last-child(n+2) h1:last-child{margin-bottom:20px}.contents:nth-last-child(n+3) h2:last-child,.no-padding .contents:nth-last-child(n+2) h2:last-child{margin-bottom:16px}.contents:nth-last-child(n+3) h3:last-child,.no-padding .contents:nth-last-child(n+2) h3:last-child{margin-bottom:12px}.contents:nth-last-child(n+3) p:last-child,.no-padding .contents:nth-last-child(n+2) p:last-child,.contents:nth-last-child(n+3) ol:last-child,.no-padding .contents:nth-last-child(n+2) ol:last-child,.contents:nth-last-child(n+3) ul:last-child,.no-padding .contents:nth-last-child(n+2) ul:last-child,.contents:nth-last-child(n+3) blockquote:last-child,.no-padding .contents:nth-last-child(n+2) blockquote:last-child,.contents:nth-last-child(n+3) .image,.no-padding .contents:nth-last-child(n+2) .image,.contents:nth-last-child(n+3) .divider,.no-padding .contents:nth-last-child(n+2) .divider,.contents:nth-last-child(n+3) .btn,.no-padding .contents:nth-last-child(n+2) .btn{margin-bottom:24px}.two-col .column p + *,.wider .column p + *,.two-col .column ol + *,.wider .column ol + *,.two-col .column ul + *,.wider .column ul + *,.two-col .column blockquote + *,.wider .column blockquote + *,.two-col .image + .contents td > :first-child,.wider .image + .contents td > :first-child{margin-top:21px}.two-col .contents:nth-last-child(n+3) p:last-child,.wider .contents:nth-last-child(n+3) p:last-child,.no-padding .two-col .contents:nth-last-child(n+2) p:last-child,.no-padding .wider .contents:nth-last-child(n+2) p:last-child,.two-col .contents:nth-last-child(n+3) ol:last-child,.wider .contents:nth-last-child(n+3) ol:last-child,.no-padding .two-col .contents:nth-last-child(n+2) ol:last-child,.no-padding .wider .contents:nth-last-child(n+2) ol:last-child,.two-col .contents:nth-last-child(n+3) ul:last-child,.wider .contents:nth-last-child(n+3) ul:last-child,.no-padding .two-col .contents:nth-last-child(n+2) ul:last-child,.no-padding .wider .contents:nth-last-child(n+2) ul:last-child,.two-col .contents:nth-last-child(n+3) blockquote:last-child,.wider .contents:nth-last-child(n+3) blockquote:last-child,.no-padding .two-col .contents:nth-last-child(n+2) blockquote:last-child,.no-padding .wider .contents:nth-last-child(n+2) blockquote:last-child,.two-col .contents:nth-last-child(n+3) .image,.wider .contents:nth-last-child(n+3) .image,.no-padding .two-col .contents:nth-last-child(n+2) .image,.no-padding .wider .contents:nth-last-child(n+2) .image,.two-col .contents:nth-last-child(n+3) .divider,.wider .contents:nth-last-child(n+3) .divider,.no-padding .two-col .contents:nth-last-child(n+2) .divider,.no-padding .wider .contents:nth-last-child(n+2) .divider,.two-col .contents:nth-last-child(n+3) .btn,.wider .contents:nth-last-child(n+3) .btn,.no-padding .two-col .contents:nth-last-child(n+2) .btn,.no-padding .wider .contents:nth-last-child(n+2) .btn{margin-bottom:21px}.three-col .column p + *,.narrower .column p + *,.three-col .column ol + *,.narrower .column ol + *,.three-col .column ul + *,.narrower .column ul + *,.three-col .column blockquote + *,.narrower .column blockquote + *,.three-col .image + .contents td > :first-child,.narrower .image + .contents td > :first-child{margin-top:18px}.three-col .contents:nth-last-child(n+3) p:last-child,.narrower .contents:nth-last-child(n+3) p:last-child,.no-padding .three-col .contents:nth-last-child(n+2) p:last-child,.no-padding .narrower .contents:nth-last-child(n+2) p:last-child,.three-col .contents:nth-last-child(n+3) ol:last-child,.narrower .contents:nth-last-child(n+3) ol:last-child,.no-padding .three-col .contents:nth-last-child(n+2) ol:last-child,.no-padding .narrower .contents:nth-last-child(n+2) ol:last-child,.three-col .contents:nth-last-child(n+3) ul:last-child,.narrower .contents:nth-last-child(n+3) ul:last-child,.no-padding .three-col .contents:nth-last-child(n+2) ul:last-child,.no-padding .narrower .contents:nth-last-child(n+2) ul:last-child,.three-col .contents:nth-last-child(n+3) blockquote:last-child,.narrower .contents:nth-last-child(n+3) blockquote:last-child,.no-padding .three-col .contents:nth-last-child(n+2) blockquote:last-child,.no-padding .narrower .contents:nth-last-child(n+2) blockquote:last-child,.three-col .contents:nth-last-child(n+3) .image,.narrower .contents:nth-last-child(n+3) .image,.no-padding .three-col .contents:nth-last-child(n+2) .image,.no-padding .narrower .contents:nth-last-child(n+2) .image,.three-col .contents:nth-last-child(n+3) .divider,.narrower .contents:nth-last-child(n+3) .divider,.no-padding .three-col .contents:nth-last-child(n+2) .divider,.no-padding .narrower .contents:nth-last-child(n+2) .divider,.three-col .contents:nth-last-child(n+3) .btn,.narrower .contents:nth-last-child(n+3) .btn,.no-padding .three-col .contents:nth-last-child(n+2) .btn,.no-padding .narrower .contents:nth-last-child(n+2) .btn{margin-bottom:18px}@media only screen and (max-width: 620px){.wrapper p + *,.wrapper ol + *,.wrapper ul + *,.wrapper blockquote + *,.image + .contents td > :first-child{margin-top:24px!important}.contents:nth-last-child(n+3) p:last-child,.no-padding .contents:nth-last-child(n+2) p:last-child,.contents:nth-last-child(n+3) ol:last-child,.no-padding .contents:nth-last-child(n+2) ol:last-child,.contents:nth-last-child(n+3) ul:last-child,.no-padding .contents:nth-last-child(n+2) ul:last-child,.contents:nth-last-child(n+3) blockquote:last-child,.no-padding .contents:nth-last-child(n+2) blockquote:last-child,.contents:nth-last-child(n+3) .image:last-child,.no-padding .contents:nth-last-child(n+2) .image:last-child,.contents:nth-last-child(n+3) .divider:last-child,.no-padding .contents:nth-last-child(n+2) .divider:last-child,.contents:nth-last-child(n+3) .btn:last-child,.no-padding .contents:nth-last-child(n+2) .btn:last-child{margin-bottom:24px!important}}td.border{width:1px}tr.border{background-color:#e3e3e3;height:1px}tr.border td{line-height:1px}.sidebar{width:600px}.first.wider .padded{padding-left:32px;padding-right:20px}.second.wider .padded{padding-left:20px;padding-right:32px}.first.narrower .padded{padding-left:32px;padding-right:8px}.second.narrower .padded{padding-left:8px;padding-right:32px}.wrapper h1{font-size:40px;line-height:48px}.wrapper h2{font-size:24px;line-height:32px}.wrapper h3{font-size:18px;line-height:24px}.wrapper h1 a,.wrapper h2 a,.wrapper h3 a{text-decoration:none}.wrapper a:hover{text-decoration:none}.wrapper p,.wrapper ol,.wrapper ul{font-size:15px;line-height:24px}.wrapper ol,.wrapper ul{margin-left:20px}.wrapper li{padding-left:2px}.wrapper blockquote{margin-left:0;padding-left:18px}.one-col,.two-col,.three-col,.sidebar{background-color:#fff;table-layout:fixed}.wrapper .two-col blockquote,.wrapper .wider blockquote{padding-left:16px}.wrapper .three-col ol,.wrapper .narrower ol,.wrapper .three-col ul,.wrapper .narrower ul{margin-left:14px}.wrapper .three-col li,.wrapper .narrower li{padding-left:1px}.wrapper .three-col blockquote,.wrapper .narrower blockquote{padding-left:12px}.wrapper h2{font-weight:700}.wrapper blockquote{font-style:italic}.preheader a,.header a{text-decoration:none}.header{margin-left:auto;margin-right:auto;width:600px}.preheader td{padding-bottom:24px;padding-top:24px}.logo{width:280px}.logo div{font-weight:700}.logo div.logo-center{text-align:center}.logo div.logo-center img{margin-left:auto;margin-right:auto}.preheader td{text-align:right;width:280px}.preheader td{letter-spacing:.01em;line-height:17px;font-weight:400}.preheader a{letter-spacing:.03em;font-weight:700}.preheader td{font-size:11px}@media only screen and (max-width: 620px){[class=wrapper] .header,[class=wrapper] .preheader td,[class=wrapper] .logo,[class=wrapper] .sidebar{width:320px!important}[class=wrapper] .header .logo{padding-left:10px!important;padding-right:10px!important}[class=wrapper] .header .logo img{max-width:280px!important;height:auto!important}[class=wrapper] .header .preheader td{padding-top:3px!important;padding-bottom:22px!important}[class=wrapper] .header .title{display:none!important}[class=wrapper] .header .webversion{text-align:center!important}[class=wrapper] h1{font-size:40px!important;line-height:48px!important}[class=wrapper] h2{font-size:24px!important;line-height:32px!important}[class=wrapper] h3{font-size:18px!important;line-height:24px!important}[class=wrapper] .column p,[class=wrapper] .column ol,[class=wrapper] .column ul{font-size:15px!important;line-height:24px!important}[class=wrapper] ol,[class=wrapper] ul{margin-left:20px!important}[class=wrapper] li{padding-left:2px!important}[class=wrapper] blockquote{border-left-width:4px!important;padding-left:18px!important}[class=wrapper] table.border{width:320px!important}[class=wrapper] .second .column-top,[class=wrapper] .third .column-top{display:none}}@media only screen and (max-width: 320px){td.border{display:none}}";
           body = body + @"        </style>";
           body = body + @"          <!--[if !mso]><!--><style type='text/css'>";
           body = body + @"        @import url(https://fonts.googleapis.com/css?family=Raleway:400,300,600);";
           body = body + @"        </style><link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'><!--<![endif]--><style type='text/css'>";
           body = body + @"        .wrapper h1{}.wrapper h1{font-family:Raleway,Tahoma,sans-serif}.mso .wrapper h1{font-family:Tahoma,sans-serif !important}.wrapper h2{}.wrapper h2{font-family:Raleway,Tahoma,sans-serif}.mso .wrapper h2{font-family:Tahoma,sans-serif !important}.wrapper h3{}.wrapper h3{font-family:Raleway,Tahoma,sans-serif}.mso .wrapper h3{font-family:Tahoma,sans-serif !important}.wrapper p,.wrapper ol,.wrapper ul,.wrapper .image{}.wrapper p,.wrapper ol,.wrapper ul,.wrapper .image{font-family:sans-serif}.wrapper .btn a{}.wrapper .btn a{font-family:sans-serif}.logo div{}.logo div{font-family:'PT Sans','Trebuchet MS',sans-serif}.mso .logo div{font-family:'Trebuchet MS',sans-serif !important}.title,.webversion,.fblike,.tweet,.linkedinshare,.forwardtoafriend,.link,.address,.permission,.campaign{}.title,.webversion,.fblike,.tweet,.linkedinshare,.forwardtoafriend,.link,.address,.permission,.campaign{font-family:Raleway,Tahoma,sans-serif}.mso .title,.mso .webversion,.mso .fblike,.mso .tweet,.mso .linkedinshare,.mso .forwardtoafriend,.mso .link,.mso .address,.mso .permission,.mso .campaign{font-family:Tahoma,sans-serif !important}body,.wrapper,.emb-editor-canvas{background-color:#ecf3f5}.border{background-color:#d4dbdd}.wrapper h1{color:#557187}.wrapper h2{color:#557187}.wrapper h3{color:#557187}.wrapper p,.wrapper ol,.wrapper ul{color:#757575}.wrapper .image{color:#757575}.wrapper a{color:#454545}.wrapper a:hover{color:#2b2b2b !important}.wrapper blockquote{border-left:4px solid #ecf3f5}.wrapper .three-col blockquote,.wrapper .narrower blockquote{border-left:2px solid #ecf3f5}.wrapper .btn a{background-color:#557187;color:#fff}.wrapper .btn a:hover{color:#fff !important}.logo div{color:#c3ced9}.logo div a{color:#c3ced9}.logo div a:hover{color:#c3ced9 !important}.title,.webversion,.footer .inner td{color:#b9b9b9}.wrapper .title a,.wrapper .webversion a,.wrapper .footer a{color:#b9b9b9}.wrapper .title a:hover,.wrapper .webversion a:hover,.wrapper .footer a:hover{color:#939393 !important}.wrapper .footer .fblike,.wrapper .footer .tweet,.wrapper .footer .linkedinshare,.wrapper .footer .forwardtoafriend{background-color:#767a7b}";
           body = body + @"        </style><!--[if mso]>";
           body = body + @"        <style type='text/css'>";
           body = body + @"        @import url(https://fonts.googleapis.com/css?family=Raleway:400,300,600);";
           body = body + @"        </style>";
           body = body + @"        <![endif]--><meta name='robots' content='noindex,nofollow' />";
           body = body + @"        <meta property='og:title' content='Consulta - Portal Training' />";
           body = body + @"        </head>";
           body = body + @"        <!--[if mso]>";
           body = body + @"          <body class='mso'>";
           body = body + @"        <![endif]-->";
           body = body + @"        <!--[if !mso]><!-->";
           body = body + @"          <body class='full-padding' style='margin: 0;padding: 0;min-width: 100%;background-color: #ecf3f5;'>";
           body = body + @"        <!--<![endif]-->";
           body = body + @"            <center class='wrapper' style='display: table;table-layout: fixed;width: 100%;min-width: 620px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #E8E8E8;'>";
           body = body + @"              <table class='header centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;'>";
           body = body + @"                <tbody>";
           body = body + @"                  <tr>";
           body = body + @"                    <td style='padding: 0;vertical-align: top;'>";
           body = body + @"                      <table class='preheader' style='border-collapse: collapse;border-spacing: 0;' align='right'>";
           body = body + @"                        <tbody>";
           body = body + @"                          <tr>";
           body = body + @"                            <td class='emb-logo-padding-box' style='padding: 0;vertical-align: top;padding-bottom: 24px;padding-top: 45px;text-align: left;width: 600px;letter-spacing: 0.01em;line-height: 17px;font-weight: 600;font-size: 25px;'>";
           body = body + @"                              <div class='spacer' style='font-size: 1px;line-height: 2px;width: 100%;'>&nbsp;</div>";
           body = body + @"                              <div class='webversion' style='font-family: Raleway,Tahoma,sans-serif;color: #31708F;'>Consulta - Portal Training</div>";
           body = body + @"                            </td>";
           body = body + @"                          </tr>";
           body = body + @"                        </tbody>";
           body = body + @"                      </table>";
           body = body + @"                    </td>";
           body = body + @"                  </tr>";
           body = body + @"                </tbody>";
           body = body + @"              </table>";
           body = body + @"              ";
           body = body + @"              <table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #d4dbdd;Margin-left: auto;Margin-right: auto;' width='602'>";
           body = body + @"                <tbody>";
           body = body + @"                  <tr>";
           body = body + @"                    <td style='padding: 0;vertical-align: top;'>&#8203;</td>";
           body = body + @"                  </tr>";
           body = body + @"                </tbody>";
           body = body + @"              </table>";
           body = body + @"                ";
           body = body + @"           <table class='centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;'>";
           body = body + @"                    <tbody>";
           body = body + @"                      <tr>";
           body = body + @"                        <td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #d4dbdd;width: 1px;'>&#8203;</td>";
           body = body + @"                        <td style='padding: 0;vertical-align: top;'>";
           body = body + @"                          <table class='two-col' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;background-color: #ffffff;table-layout: fixed;' emb-background-style>";
           body = body + @"                            <tbody>";
           body = body + @"                              <tr>";
           body = body + @"                                ";
           body = body + @"                                <td class='column first' style='padding: 0;vertical-align: top;text-align: left;width: 300px;'>";
           body = body + @"                                  <div>";
           body = body + @"                                    <div class='column-top' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;'>&nbsp;</div>";
           body = body + @"                                  </div>";
           body = body + @"                                  <table class='contents' style='border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;'>";
           body = body + @"                                    <tbody>";
           body = body + @"                                      <tr>";
           body = body + @"                                        <td class='padded' style='padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px;word-break: break-word;word-wrap: break-word;'>";
           body = body + @"                                          <h3 style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-size: 18px;line-height: 24px;font-family: Raleway,Tahoma,sans-serif;color: #557187;'>" + Nombre + " " + Apellido + "</h3>";
           body = body + @"                                          <p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 12px;font-size: 16px;line-height: 24px;font-family: sans-serif;color: #757575;'>Email: " + Email + " </p>";
           body = body + @"                                          <p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 12px;font-size: 16px;line-height: 24px;font-family: sans-serif;color: #757575;'>Usuario: " + cod_usuario+ " </p>";
           body = body + @"                                          <p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 12px;font-size: 16px;line-height: 24px;font-family: sans-serif;color: #757575;'>Consulta: " + Consulta + "</p>";
           body = body + @"                                        </td>";
           body = body + @"                                      </tr>";
           body = body + @"                                    </tbody>";
           body = body + @"                                  </table>";
           body = body + @"                                  <div class='column-bottom' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;'>&nbsp;</div>";
           body = body + @"                                </td>";
           body = body + @"                              </tr>";
           body = body + @"                            </tbody>";
           body = body + @"                          </table>";
           body = body + @"                        </td>";
           body = body + @"                        <td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #d4dbdd;width: 1px;'>&#8203;</td>";
           body = body + @"                      </tr>";
           body = body + @"                    </tbody>";
           body = body + @"                  </table>";
           body = body + @"                ";
           body = body + @"                  <table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #d4dbdd;Margin-left: auto;Margin-right: auto;' width='602'>";
           body = body + @"                    <tbody><tr class='border' style='font-size: 1px;line-height: 1px;background-color: #e3e3e3;height: 1px;'>";
           body = body + @"                      <td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #d4dbdd;width: 1px;'>&#8203;</td>";
           body = body + @"                      <td style='padding: 0;vertical-align: top;line-height: 1px;'>&#8203;</td>";
           body = body + @"                      <td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #d4dbdd;width: 1px;'>&#8203;</td>";
           body = body + @"                    </tr>";
           body = body + @"                  </tbody></table>";
           body = body + @"                ";
           body = body + @"";
           body = body + @"                  <table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #d4dbdd;Margin-left: auto;Margin-right: auto;' width='602'>";
           body = body + @"                    <tbody><tr><td style='padding: 0;vertical-align: top;'>&nbsp;</td></tr>";
           body = body + @"                  </tbody></table>";
           body = body + @"                ";
           body = body + @"              <table class='footer centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 100%;'>";
           body = body + @"                <tbody><tr>";
           body = body + @"                  <td style='padding: 0;vertical-align: top;'>&nbsp;</td>";
           body = body + @"                  <td class='inner' style='padding: 15px 0 0 0;vertical-align: top;width: 600px;'>";
           body = body + @"        ";
           body = body + @"                    <table class='left' style='border-collapse: collapse;border-spacing: 0;' align='left'>";
           body = body + @"                      <tbody><tr>";
           body = body + @"                        <td style='padding: 0;vertical-align: top;color: #b9b9b9;font-size: 12px;line-height: 22px;text-align: left;width: 400px;'>";
           body = body + @"                          ";
           body = body + @"                          <div class='campaign' style='font-family: Raleway,Tahoma,sans-serif;Margin-bottom: 18px;'>";
           body = body + @"                            <span>";
           body = body + @"                              Desarrollado por <a href='http://www.amedia.com.ar/'>Grupo Amedia S.R.L.</a>";
           body = body + @"                            </span>";
           body = body + @"                            ";
           body = body + @"                          </div>";
           body = body + @"                        </td>";
           body = body + @"                      </tr>";
           body = body + @"                    </tbody></table>";
           body = body + @"                  </td>";
           body = body + @"                  <td style='padding: 0;vertical-align: top;'>&nbsp;</td>";
           body = body + @"                </tr>";
           body = body + @"              </tbody></table>";
           body = body + @"            </center>";
           body = body + @"          ";
           body = body + @"        </body></html>";
           return body;
       }
       public static string gmail_send(string Email, string Nombre, string Apellido, string Password)
       {
           string body = "";
           body = body + @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>";
           body = body + @"<html xmlns='http://www.w3.org/1999/xhtml'>";
           body = body + @"<head>";
           body = body + @"<title>Recupera tu contraseña</title>";
           body = body + @"<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
           body = body + @"<meta name='viewport' content='width=device-width, initial-scale=1.0' />";
           body = body + @"<style type='text/css'>*{-ms-text-size-adjust:100%;-webkit-text-size-adjust:none;-webkit-text-resize:100%;text-resize:100%}a{outline:0;color:#fda50c;text-decoration:none}a:hover{text-decoration:underline!important}a[x-apple-data-detectors]{color:inherit!important;text-decoration:none!important}.link a{color:#fff;text-decoration:underline}.link a:hover{text-decoration:none!important}.active:hover{opacity:.8}.active{-webkit-transition:all .3s ease;-moz-transition:all .3s ease;-ms-transition:all .3s ease;transition:all .3s ease}.footer a{color:#fff}table td{border-collapse:collapse!important;mso-line-height-rule:exactly}.ExternalClass,.ExternalClass a,.ExternalClass span,.ExternalClass b,.ExternalClass br,.ExternalClass p,.ExternalClass div{line-height:inherit}@media only screen and (max-width:500px){table[class='flexible']{width:100%!important}*[class='hide']{display:none!important;width:0!important;height:0!important;padding:0!important;font-size:0!important;line-height:0!important}td[class='img-flex'] img{width:100%!important;height:auto!important}td[class='aligncenter']{text-align:center!important}th[class='flex']{display:block!important;width:100%!important}tr[class='table-holder']{display:table!important;width:100%!important}th[class='thead']{display:table-header-group!important;width:100%!important}th[class='tfoot']{display:table-footer-group!important;width:100%!important}td[class='wrapper']{padding:5px!important}td[class='header']{padding:30px 10px!important}td[class='header'] *{text-align:center!important}td[class='title']{padding:0 0 30px!important;font-size:30px!important;line-height:36px!important}td[class='frame']{padding:20px 10px!important}td[class='holder']{padding:20px 20px 30px!important}td[class='holder'] *{text-align:center!important}td[class='block']{padding:0 10px 10px!important}td[class='box']{padding:0 20px 20px!important}td[class='nav']{line-height:30px!important}td[class='p-30']{padding:0 0 30px!important}td[class='p-20']{padding:0 0 20px!important}td[class='p-10']{padding:0 0 10px!important}td[class='block-10']{padding:0 10px 20px!important}td[class='indent-null']{padding:0!important}td[class='indent']{padding:0 10px!important}td[class='w-20']{width:20px!important}td[class='h-auto']{height:auto!important}td[class='footer']{padding:20px 10px!important}}</style>";
           body = body + @"</head>";
           body = body + @"<body style='margin:0;padding:0'><table style='min-width:320px' width='100%' cellspacing='0' cellpadding='0'><tr><td style='line-height:0'><div style='display:none;white-space:nowrap;font:15px/1px courier'>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></td></tr><tr><td class='hide'><table width='700' cellpadding='0' cellspacing='0' style='width:700px!important'><tr><td style='min-width:700px;font-size:0;line-height:0'>&nbsp;</td></tr></table></td></tr></table><table style='min-width:320px' width='100%' cellspacing='0' cellpadding='0'><tr><td><table data-module='pre-header' width='100%' cellpadding='0' cellspacing='0' style='display:none'><tr><td data-bgcolor='bg-module' bgcolor='#32862B' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td data-color='text-pre-header' data-size='size text-pre-header' data-min='10' data-max='24' data-link-color='link text-pre-header color' data-link-style='text-decoration:none; color:#fff;' align='center' style='padding:9px 10px 11px;font:14px/19px Arial,Helvetica,sans-serif;color:#83d27d'></td></tr></table></td></tr></table><table data-module='header' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='header' style='padding:38px 0 37px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><th class='flex' align='center' style='vertical-align:top;padding:0'><table width='100%' cellpadding='0' cellspacing='0'><tr><td><a href='#' style='text-decoration:none;color:#fff;display:block;width:78px;margin:auto'><img src='https://sidepro.com.ar/som/images/som-logo-header2.png' height='121' width='78' border='0' style='vertical-align:top' alt='SOM' /></a></td></tr></table></th></tr></table></td></tr></table></td></tr></table><table data-module='module-1' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='img-flex'><a href='#'><img src='https://sidepro.com.ar/som/images/imgMail.jpg' height='318' width='700' border='0' style='vertical-align:top' /></a></td></tr></table></td></tr></table><table data-module='module-2' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' bgcolor='#4EB046' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='frame' bgcolor='#ffffff' style='padding:28px 40px 20px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#000;' align='center' style='padding:0 0 15px;font:bold 24px/26px Arial,Helvetica,sans-serif;color:#000'>¡Hola " + Nombre + " " + Apellido + "!</td></tr><tr><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:underline; color:#000;' style='font:14px/25px Arial,Helvetica,sans-serif;color:#000'>Solicitaste que se enviara este email para recuperar tu contraseña. <br />Para ingresar deberas utilizar la siguiente contraseña y cambiarla una vez que hayas ingresado a la plataforma.</td></tr></table></td></tr></table></td></tr></table><table data-module='module-3' width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-module' class='block-10' bgcolor='#4EB046' style='padding:0 10px 50px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-block' class='frame' bgcolor='#ffffff' style='border-radius:0 0 3px 3px;padding:20px 40px 40px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td><table width='100%' cellpadding='0' cellspacing='0'><tr><td data-bgcolor='bg-content' class='holder' bgcolor='#32862B' style='padding:36px 39px 40px;border-radius:3px'><table width='100%' cellpadding='0' cellspacing='0'><tr class='link'><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#fff;' align='center' style='padding:0 0 15px;font:bold 24px/26px Arial,Helvetica,sans-serif;color:#fff'>Tu contraseña</td></tr><tr class='link'><td data-color='title' data-size='size title' data-min='20' data-max='40' data-link-color='link title color' data-link-style='text-decoration:underline; color:#fff;' align='center' style='padding:0 0 15px;font:18px/20px Arial,Helvetica,sans-serif;color:#fff'>" + Password + "</td></tr><tr class='link' align='center'><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:underline; color:#fff;' style='padding:0 0 23px;font:14px/25px Arial,Helvetica,sans-serif;color:#fff'>Recuerda cambiarla cuando ingreses a la plataforma</td></tr><tr><td><table align='center' cellpadding='0' cellspacing='0' style='margin:0 auto'><tr><td data-bgcolor='bg-button' data-size='size button' data-min='10' data-max='26' class='active' bgcolor='#ffffff' align='center' style='mso-padding-alt:13px 20px 11px;border-radius:3px;font:bold 15px/17px Arial,Helvetica,sans-serif;color:#fda50c;text-transform:uppercase'><a href='http://sidepro.com.ar/som' style='text-decoration:none!important;color:#32862B!important;display:block;padding:13px 20px 11px'>Quiero ingresar</a></td></tr></table></td></tr></table></td></tr></table></td></tr></table></td></tr></table></td></tr></table><table data-module='footer' width='100%' cellpadding='0' cellspacing='0' style='display:none'><tr><td data-bgcolor='bg-module' bgcolor='#32862B' style='padding:0 10px'><table class='flexible' width='700' align='center' style='margin:0 auto' cellpadding='0' cellspacing='0'><tr><td class='footer' style='padding:30px 20px'><table width='100%' cellpadding='0' cellspacing='0'><tr><td class='p-20' style='padding:0 0 20px'><table align='center' cellpadding='0' cellspacing='0' style='margin:0 auto'><tr><td><a class='active' href='https://www.facebook.com/Sindicato-de-Obreros-de-Maestranza-143264025813304'><img src='sidepro.com.ar/som/Images/ico-facebook.png' height='17' width='9' border='0' style='vertical-align:top' alt='facebook' /></a></td><td class='w-20' width='29'></td><td><a class='active' href='https://twitter.com/somaestranza'><img src='sidepro.com.ar/som/images/ico-twitter.png' height='15' width='19' border='0' style='vertical-align:top' alt='twitter' /></a></td></tr></table></td></tr><tr><td data-color='text' data-size='size text' data-min='10' data-max='26' data-link-color='link text color' data-link-style='text-decoration:none; color:#fff;' align='center' style='font:14px/25px Arial,Helvetica,sans-serif;color:#fff'><a href='http://www.som.org.ar'>www.som.org.ar</a></td></tr></table></td></tr></table></td></tr></table></td></tr></table></body></html>";

           using (MailMessage mailMessage =
           new MailMessage(new MailAddress(@"no-reply@amedia.com.ar"),
           new MailAddress(@"no-reply@amedia.com.ar")))
           {
               mailMessage.Body = "body";
               mailMessage.Subject = "subject";
               try
               {
                   SmtpClient SmtpServer = new SmtpClient();
                   SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                   SmtpServer.Credentials =
                       new System.Net.NetworkCredential(@"no-reply@amedia.com.ar", "sotelo21");
                   SmtpServer.Port = 587;
                   SmtpServer.Host = "smtp.gmail.com";
                   SmtpServer.EnableSsl = true;
                   var mail = new MailMessage();
                   String[] addr = Email.Split(','); // toemail is a string which contains many email address separated by comma
                   mail.From = new MailAddress(@"no-reply@amedia.com.ar");
                   Byte i;
                   for (i = 0; i < addr.Length; i++)
                       mail.To.Add(addr[i]);
                   mail.Subject = "Recuperar Password Sidepro-SOM";
                   mail.Body = body;// "cuerpo browar";
                   mail.IsBodyHtml = true;
                   mail.DeliveryNotificationOptions =
                       DeliveryNotificationOptions.OnFailure;
                   //   mail.ReplyTo = new MailAddress(toemail);
                   mail.ReplyToList.Add(@"no-reply@amedia.com.ar");
                   SmtpServer.Send(mail);
                   return "";
               }
               catch (Exception ex)
               {
                   string exp = ex.ToString();

                   //Console.WriteLine("Mail Not Sent ... and ther error is " + exp);
                   return exp;
               }
           }
       }


    }
}
