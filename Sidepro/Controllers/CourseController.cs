﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sidepro.Application;
using Sidepro.ViewModels;
using Sidepro.Entities;
using System.Web.Script.Serialization;
using Sidepro.Application.Helpers;
namespace Sidepro.Controllers
{
    public class CourseController : Controller
    {
        
        CursosViewModelController cursosVieweModelController = new CursosViewModelController();
        LeccionManagementService leccionManagementService = new LeccionManagementService();
        public ActionResult Course(int? id_module, int id_curso)
        {
            ViewBag.Message = "Página de cursos";
            int id_usuario = obtenerUsuario();
            return View(cursosVieweModelController.getCursosViewModel((int)id_module, id_curso, id_usuario));
        }
        public ActionResult ObtenerVideo(int id_modulo, int id_curso, int id_leccion) {


            Leccion miLeccion = new Leccion();
            int id_usuario = obtenerUsuario();
            miLeccion = leccionManagementService.getLeccion(id_modulo, id_curso, id_leccion);
            var misPreguntas = leccionManagementService.getQuiz(id_modulo, id_curso, id_leccion, id_usuario);
            misPreguntas.video = miLeccion.URL_Video;
            misPreguntas.imagen = miLeccion.URL_Imagen;
            misPreguntas.nombre = miLeccion.Nombre;
            var json = new JavaScriptSerializer().Serialize(misPreguntas);
            return Json(json, JsonRequestBehavior.AllowGet);
            //return Json(new { video = miLeccion.URL_Video, imagen = miLeccion.URL_Imagen, nombre = miLeccion.Nombre }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ObtenerQuiz(int id_modulo, int id_curso, int id_leccion) {

            int id_usuario = obtenerUsuario();
            var json = new JavaScriptSerializer().Serialize(leccionManagementService.getQuiz(id_modulo, id_curso, id_leccion, id_usuario));
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult grabarResultado(int id_modulo, int id_curso, int id_leccion, decimal nota)
        {
            int id_usuario = obtenerUsuario();
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
            bool result = leccionManagementService.grabarResultado(id_modulo, id_curso, id_leccion, nota, id_usuario);
            return Json(new { aprobo = result }, JsonRequestBehavior.AllowGet);
        }
        public int obtenerUsuario()
        {
            HttpCookie myCookie = Request.Cookies["UserSettings"];
            int id_usuario = HelpersManagementService.getCookie(myCookie).ID_Usuario;
            /*
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }*/
            return id_usuario;
        }


        
    }
}
