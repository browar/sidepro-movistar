﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sidepro.ViewModels;
using System.Web.Script.Serialization;
using Sidepro.Application;
using Sidepro.Entities;
using Sidepro.Application.Helpers;
namespace Sidepro.Controllers
{
    public class CertificationController : Controller
    {
        LeccionManagementService leccionManagementService = new LeccionManagementService();
        CursosViewModelController cursosVieweModelController = new CursosViewModelController();
        indexViewModel IndexViewModel = new indexViewModel();
        public ActionResult Certification()
        {
            ViewBag.Message = "Página inicial";
            return View(IndexViewModel);//cursosVieweModelController.getCursosViewModel(1, 1));
        }
        public ActionResult obtenerQuiz()
        {

            int id_usuario = obtenerUsuario();
            var json = new JavaScriptSerializer().Serialize(leccionManagementService.getQuizCertificaciones(id_usuario));
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult grabarResultado()
        {

            int id_usuario = obtenerUsuario();
            //bool result = leccionManagementService.grabarResultado(4, 0, 0, nota, id_usuario);
            Certification result = leccionManagementService.getResultado(4, 0, 0, id_usuario);
            return Json(new { notaFinal = result.Nota, aprobado = result.Aprobado }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult grabarRespuesta(int question, int answer) {

            int id_usuario = obtenerUsuario();
            leccionManagementService.grabarRespuesta(id_usuario, question, answer);
            return Json(new { result = true }, JsonRequestBehavior.AllowGet);
        
        }
        public int obtenerUsuario()
        {
            HttpCookie myCookie = Request.Cookies["UserSettings"];
            int id_usuario = HelpersManagementService.getCookie(myCookie).ID_Usuario;
            /*
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
            */
            return id_usuario;
        }

        

    }
}
