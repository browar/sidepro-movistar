﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sidepro.Application;
using Sidepro.ViewModels;
using Sidepro.Entities;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using ClosedXML.Excel;
using Sidepro.Application.Helpers;
namespace Sidepro.Controllers 
{
    public class ReportController : Controller
    {
        public ActionResult Index()
        {
            ReportViewModelController viewModelController = new ReportViewModelController();

            int id_usuario = obtenerUsuario();
            ReportViewModel miViewModel = viewModelController.getReportViewModel(id_usuario);
            return View(miViewModel);
        }
        public ActionResult listadoAgentes()
        {
            ReportViewModelController viewModelController = new ReportViewModelController();

            int id_usuario = obtenerUsuario();
            ReportViewModel miViewModel = viewModelController.getReportViewModel(id_usuario);
            return View(miViewModel);
        }
        public int obtenerUsuario()
        {
            HttpCookie myCookie = Request.Cookies["UserSettings"];
            int id_usuario = HelpersManagementService.getCookie(myCookie).ID_Usuario;
            /*
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }*/
            return id_usuario;
        }
        public ActionResult GetPagina(int id_pagina)
        {
            ReportManagementService reportManagementService = new ReportManagementService();
            int desde = (id_pagina - 1) * 6;
            int hasta = (id_pagina) * 6;
            var misDatos = reportManagementService.getProgresoAgente(desde, hasta);
            var json = new JavaScriptSerializer().Serialize(misDatos);
            return Json(json, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetUsuarios(string usuario) {

            ReportManagementService reportManagementService = new ReportManagementService();
            var misDatos = reportManagementService.getProgresoUsuarios(usuario);
            var json = new JavaScriptSerializer().Serialize(misDatos);
            return Json(json, JsonRequestBehavior.AllowGet);        
        }
        public ActionResult agente(int id_agencia)
        {
            ReportViewModelController viewModelController = new ReportViewModelController();

            int id_usuario = obtenerUsuario();
            ReportAgenciaViewModel miViewModel = viewModelController.getReportAgenciaViewModel(id_usuario, id_agencia);
            return View(miViewModel);
        }
        public ActionResult usuario(int id_usuario)
        {
            ReportViewModelController viewModelController = new ReportViewModelController();
            var miViewModel = viewModelController.getReportUsuarioViewModel(id_usuario);
            return View(miViewModel);
        }
        public ActionResult GetUsuariosAgencia(string usuario, int id_agencia)
        {

            ReportManagementService reportManagementService = new ReportManagementService();
            var misDatos = reportManagementService.getProgresoUsuarios(usuario, id_agencia);
            var json = new JavaScriptSerializer().Serialize(misDatos);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReporteNovedadesFiltro(string usuario)
        {
            var misDatos = new ReportManagementService().GetReporteNovedades(usuario);
            var json = new JavaScriptSerializer().Serialize(misDatos);
            return Json(json, JsonRequestBehavior.AllowGet);

        }
        public ActionResult GetReporteEstadoMailsFiltro(string usuario)
        {

            var misDatos = new ReportManagementService().getEstadoEmailsFiltro(usuario);
            var json = new JavaScriptSerializer().Serialize(misDatos);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public void ExportToExcel(int id_agencia)
        {
            
            ReportManagementService reportManagementService = new ReportManagementService();
            //List<CourseProgress>
            var certificaciones = reportManagementService.getProgresoMensualAgencia(id_agencia).OrderByDescending(x => x.Ano).ThenByDescending(y => y.Mes);


            var libro = new XLWorkbook();

            IXLWorksheet hoja;
            foreach (var cert in certificaciones)
            {

                int mes = cert.Mes;
                int ano = cert.Ano;
                var progresoUsuarios = reportManagementService.getCertificacionesDeUsuariosDeAgenciaDelMes(id_agencia, mes, ano);
                hoja = libro.Worksheets.Add(cert.Periodo);
                int cantidadUsuarios = 0;

                //Hoja
                //Titulo
                hoja.Cell("B2").Value = "Certificacion de " + cert.Periodo;
                //Valores agencia
                hoja.Cell("B3").Value = "Puntajes";
                hoja.Cell("C3").Value = cert.Total.ToString("0.00") + "%";//"75.34%";

                //Titulo 2
                hoja.Cells("B5").Value = "Documento";
                hoja.Cells("C5").Value = "Nombre";
                hoja.Cells("D5").Value = "Apellido";
                hoja.Cells("E5").Value = "Puntaje";

                foreach (var userProgress in progresoUsuarios)
                {
                    hoja.Cells("B" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Documento;
                    hoja.Cells("C" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Nombre ;
                    hoja.Cells("D" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Apellido;
                    hoja.Cells("E" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Certificacion.ToString() + (userProgress.Certificacion.ToString() != "No Realizado" ? "%" : "");
                    cantidadUsuarios++;
                }
                #region - Generar Rangos -

                var rangoAgencia = "B2:C3";
                var cellDocumento = "B5";
                var cellNombre = "C5";
                var cellApellido = "D5";
                var cellPuntaje = "E5";
                var rangoUsuarios = "B6:E" + (5 + cantidadUsuarios).ToString();
                var tablaUsuarios = "B5:E" + (5 + cantidadUsuarios).ToString();

                #endregion

                #region - Generar Formato -

                var rngTableAgencia = hoja.Range(rangoAgencia);
                rngTableAgencia.FirstRow().Merge();
                rngTableAgencia.FirstCell().Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    ;
                rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                //var rngTableUsuarios = hoja.Range("B5:C5");
                hoja.Cells(cellDocumento).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellNombre).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellApellido).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellPuntaje).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Columns().AdjustToContents();

                hoja.Column("B").Width = 20;
                hoja.Column("C").Width = 20;
                hoja.Column("D").Width = 20;
                hoja.Column("E").Width = 20;

                var rngTableUsuarios = hoja.Range(rangoUsuarios);
                rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                rngTableUsuarios = hoja.Range(tablaUsuarios);
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                #endregion
            }
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Certificaciones.xlsx\"");

            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();

        }

        public void ExportToExcelTodosDic()
        {

            ReportManagementService reportManagementService = new ReportManagementService();
            var certificaciones = reportManagementService.getProgresoMensualAgencia(1).OrderByDescending(x => x.Ano).ThenByDescending(y => y.Mes);
            //List<CourseProgress>
            var agencias = new AgenciaManagementService().getAgencias();
            var libro = new XLWorkbook();
            IXLWorksheet hoja;
            hoja = libro.Worksheets.Add("Diciembre 2015");
            hoja.Cell("B2").Value = "Certificacion de " + "Diciembre 2015";
            hoja.Cells("B5").Value = "Documento";
            hoja.Cells("C5").Value = "Nombre";
            hoja.Cells("D5").Value = "Apellido";
            hoja.Cells("E5").Value = "Agencia";
            hoja.Cells("F5").Value = "Puntaje";

            int cantidadUsuarios = 0;
            foreach (Agencia miAgencia in agencias.Where(x => x.Cod_tipo_agencia == 1))
            {
                
                certificaciones = reportManagementService.getProgresoMensualAgencia(miAgencia.ID_Agencia).OrderByDescending(x => x.Ano).ThenByDescending(y => y.Mes);
                //foreach (var cert in certificaciones.Where(x => x.Ano == 2015 && x.Mes == 12))
                //{

                    int mes = 12; //cert.Mes;
                    int ano = 2015;//cert.Ano;
                    var progresoUsuarios = reportManagementService.getCertificacionesDeUsuariosDeAgenciaDelMes(miAgencia.ID_Agencia, mes, ano);
                    
                    

                    //Hoja
                    //Titulo
                    
                    //Valores agencia
                    /*
                    hoja.Cell("B3").Value = "Puntajes";
                    hoja.Cell("C3").Value = cert.Total.ToString("0.00") + "%";//"75.34%";
                    */
                    //Titulo 2

                    foreach (var userProgress in progresoUsuarios)
                    {
                        hoja.Cells("B" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Documento;
                        hoja.Cells("C" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Nombre;
                        hoja.Cells("D" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Apellido;
                        hoja.Cells("E" + (6 + cantidadUsuarios).ToString()).Value = miAgencia.Nombre;
                        hoja.Cells("F" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Certificacion.ToString() + (userProgress.Certificacion.ToString() != "No Realizado" ? "%" : "");
                        cantidadUsuarios++;
                    }
                    #region - Generar Rangos -

                    var rangoAgencia = "B2:C3";
                    var cellDocumento = "B5";
                    var cellNombre = "C5";
                    var cellApellido = "D5";
                    var cellAgencia = "E5";
                    var cellPuntaje = "F5";
                    var rangoUsuarios = "B6:F" + (5 + cantidadUsuarios).ToString();
                    var tablaUsuarios = "B5:F" + (5 + cantidadUsuarios).ToString();

                    #endregion

                    #region - Generar Formato -

                    var rngTableAgencia = hoja.Range(rangoAgencia);
                    rngTableAgencia.FirstRow().Merge();
                    rngTableAgencia.FirstCell().Style
                        .Font.SetBold()
                        .Fill.SetBackgroundColor(XLColor.LightGreen)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        ;
                    rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                    rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                    //var rngTableUsuarios = hoja.Range("B5:C5");
                    hoja.Cells(cellDocumento).Style
                        .Font.SetBold()
                        .Fill.SetBackgroundColor(XLColor.LightGreen)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    hoja.Cells(cellNombre).Style
                        .Font.SetBold()
                        .Fill.SetBackgroundColor(XLColor.LightGreen)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    hoja.Cells(cellApellido).Style
                        .Font.SetBold()
                        .Fill.SetBackgroundColor(XLColor.LightGreen)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    hoja.Cells(cellPuntaje).Style
                        .Font.SetBold()
                        .Fill.SetBackgroundColor(XLColor.LightGreen)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    hoja.Cells(cellAgencia).Style
                        .Font.SetBold()
                        .Fill.SetBackgroundColor(XLColor.LightGreen)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                    hoja.Columns().AdjustToContents();

                    hoja.Column("B").Width = 20;
                    hoja.Column("C").Width = 20;
                    hoja.Column("D").Width = 20;
                    hoja.Column("E").Width = 20;
                    hoja.Column("F").Width = 20;

                    var rngTableUsuarios = hoja.Range(rangoUsuarios);
                    rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                    rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                    rngTableUsuarios = hoja.Range(tablaUsuarios);
                    rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                    #endregion
               // }

            }
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Certificaciones.xlsx\"");


            

            


            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();

        }
        public void ExportToExcelTodosNov()
        {

            ReportManagementService reportManagementService = new ReportManagementService();
            var certificaciones = reportManagementService.getProgresoMensualAgencia(1).OrderByDescending(x => x.Ano).ThenByDescending(y => y.Mes);
            //List<CourseProgress>
            var agencias = new AgenciaManagementService().getAgencias();
            var libro = new XLWorkbook();
            IXLWorksheet hoja;
            hoja = libro.Worksheets.Add("Noviembre 2015");
            hoja.Cell("B2").Value = "Certificacion de " + "Noviembre 2015";
            hoja.Cells("B5").Value = "Documento";
            hoja.Cells("C5").Value = "Nombre";
            hoja.Cells("D5").Value = "Apellido";
            hoja.Cells("E5").Value = "Agencia";
            hoja.Cells("F5").Value = "Puntaje";

            int cantidadUsuarios = 0;
            foreach (Agencia miAgencia in agencias.Where(x => x.Cod_tipo_agencia == 1))
            {

                certificaciones = reportManagementService.getProgresoMensualAgencia(miAgencia.ID_Agencia).OrderByDescending(x => x.Ano).ThenByDescending(y => y.Mes);
                //foreach (var cert in certificaciones.Where(x => x.Ano == 2015 && x.Mes == 12))
                //{

                int mes = 11; //cert.Mes;
                int ano = 2015;//cert.Ano;
                var progresoUsuarios = reportManagementService.getCertificacionesDeUsuariosDeAgenciaDelMes(miAgencia.ID_Agencia, mes, ano);



                //Hoja
                //Titulo

                //Valores agencia
                /*
                hoja.Cell("B3").Value = "Puntajes";
                hoja.Cell("C3").Value = cert.Total.ToString("0.00") + "%";//"75.34%";
                */
                //Titulo 2

                foreach (var userProgress in progresoUsuarios)
                {
                    hoja.Cells("B" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Documento;
                    hoja.Cells("C" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Nombre;
                    hoja.Cells("D" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Apellido;
                    hoja.Cells("E" + (6 + cantidadUsuarios).ToString()).Value = miAgencia.Nombre;
                    hoja.Cells("F" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Certificacion.ToString() + (userProgress.Certificacion.ToString() != "No Realizado" ? "%" : "");
                    cantidadUsuarios++;
                }
                #region - Generar Rangos -

                var rangoAgencia = "B2:C3";
                var cellDocumento = "B5";
                var cellNombre = "C5";
                var cellApellido = "D5";
                var cellAgencia = "E5";
                var cellPuntaje = "F5";
                var rangoUsuarios = "B6:F" + (5 + cantidadUsuarios).ToString();
                var tablaUsuarios = "B5:F" + (5 + cantidadUsuarios).ToString();

                #endregion

                #region - Generar Formato -

                var rngTableAgencia = hoja.Range(rangoAgencia);
                rngTableAgencia.FirstRow().Merge();
                rngTableAgencia.FirstCell().Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    ;
                rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                //var rngTableUsuarios = hoja.Range("B5:C5");
                hoja.Cells(cellDocumento).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellNombre).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellApellido).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellPuntaje).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellAgencia).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Columns().AdjustToContents();

                hoja.Column("B").Width = 20;
                hoja.Column("C").Width = 20;
                hoja.Column("D").Width = 20;
                hoja.Column("E").Width = 20;
                hoja.Column("F").Width = 20;

                var rngTableUsuarios = hoja.Range(rangoUsuarios);
                rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                rngTableUsuarios = hoja.Range(tablaUsuarios);
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                #endregion
                // }

            }
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Certificaciones.xlsx\"");







            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();

        }

        public void ExportToExcelTodosOct()
        {

            ReportManagementService reportManagementService = new ReportManagementService();
            var certificaciones = reportManagementService.getProgresoMensualAgencia(1).OrderByDescending(x => x.Ano).ThenByDescending(y => y.Mes);
            //List<CourseProgress>
            var agencias = new AgenciaManagementService().getAgencias();
            var libro = new XLWorkbook();
            IXLWorksheet hoja;
            hoja = libro.Worksheets.Add("Octubre 2015");
            hoja.Cell("B2").Value = "Certificacion de " + "Octubre 2015";
            hoja.Cells("B5").Value = "Documento";
            hoja.Cells("C5").Value = "Nombre";
            hoja.Cells("D5").Value = "Apellido";
            hoja.Cells("E5").Value = "Agencia";
            hoja.Cells("F5").Value = "Puntaje";

            int cantidadUsuarios = 0;
            foreach (Agencia miAgencia in agencias.Where(x => x.Cod_tipo_agencia == 1))
            {

                certificaciones = reportManagementService.getProgresoMensualAgencia(miAgencia.ID_Agencia).OrderByDescending(x => x.Ano).ThenByDescending(y => y.Mes);
                //foreach (var cert in certificaciones.Where(x => x.Ano == 2015 && x.Mes == 12))
                //{

                int mes = 10; //cert.Mes;
                int ano = 2015;//cert.Ano;
                var progresoUsuarios = reportManagementService.getCertificacionesDeUsuariosDeAgenciaDelMes(miAgencia.ID_Agencia, mes, ano);



                //Hoja
                //Titulo

                //Valores agencia
                /*
                hoja.Cell("B3").Value = "Puntajes";
                hoja.Cell("C3").Value = cert.Total.ToString("0.00") + "%";//"75.34%";
                */
                //Titulo 2

                foreach (var userProgress in progresoUsuarios)
                {
                    hoja.Cells("B" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Documento;
                    hoja.Cells("C" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Nombre;
                    hoja.Cells("D" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Apellido;
                    hoja.Cells("E" + (6 + cantidadUsuarios).ToString()).Value = miAgencia.Nombre;
                    hoja.Cells("F" + (6 + cantidadUsuarios).ToString()).Value = userProgress.Certificacion.ToString() + (userProgress.Certificacion.ToString() != "No Realizado" ? "%" : "");
                    cantidadUsuarios++;
                }
                #region - Generar Rangos -

                var rangoAgencia = "B2:C3";
                var cellDocumento = "B5";
                var cellNombre = "C5";
                var cellApellido = "D5";
                var cellAgencia = "E5";
                var cellPuntaje = "F5";
                var rangoUsuarios = "B6:F" + (5 + cantidadUsuarios).ToString();
                var tablaUsuarios = "B5:F" + (5 + cantidadUsuarios).ToString();

                #endregion

                #region - Generar Formato -

                var rngTableAgencia = hoja.Range(rangoAgencia);
                rngTableAgencia.FirstRow().Merge();
                rngTableAgencia.FirstCell().Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    ;
                rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                //var rngTableUsuarios = hoja.Range("B5:C5");
                hoja.Cells(cellDocumento).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellNombre).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellApellido).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellPuntaje).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Cells(cellAgencia).Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
                hoja.Columns().AdjustToContents();

                hoja.Column("B").Width = 20;
                hoja.Column("C").Width = 20;
                hoja.Column("D").Width = 20;
                hoja.Column("E").Width = 20;
                hoja.Column("F").Width = 20;

                var rngTableUsuarios = hoja.Range(rangoUsuarios);
                rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                rngTableUsuarios = hoja.Range(tablaUsuarios);
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                #endregion
                // }

            }
            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Certificaciones.xlsx\"");







            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();

        }

        public void ExportToExcelMensual()
        {

            ReportManagementService miReport = new ReportManagementService();
            // 3 paginas, Certificacion Agencia, Certificacion Usuario, Cantidad Usuarios

            var libro = new XLWorkbook();

            
            #region  - Certificacion Agencia -
            IXLWorksheet hoja;
            hoja = libro.Worksheets.Add("Certificaciones por Agencia");
            
            hoja.Cells("C2").Value = "Agencia";
            hoja.Cells("D2").Value = "Certificacion";



            int mes = DateTime.Now.Month == 1 ? 12 : (DateTime.Now.Month - 1); //cert.Mes;
            int ano = DateTime.Now.Month == 1 ? (DateTime.Now.Year - 1) : (DateTime.Now.Year);//cert.Ano;
            var CertificacionesAgencias = miReport.getCertificacionesAgencias(mes, ano);
            int cantidadUsuarios = 0;
            foreach (UserProgress miProgreso in CertificacionesAgencias)
            {


                hoja.Cells("C" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Agencia;
                hoja.Cells("D" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Certificacion;
                cantidadUsuarios++;
            }
                
                #region - Generar Rangos -

                var rangoAgencia = "C2:D2";
                var rangoUsuarios = "C2:D" + (2 + cantidadUsuarios).ToString();
                var tablaUsuarios = "C2:D" + (2 + cantidadUsuarios).ToString();

                #endregion

                #region - Generar Formato -

                var rngTableAgencia = hoja.Range(rangoAgencia);
                //rngTableAgencia.FirstRow().Merge();
                rngTableAgencia.Style
                    .Font.SetBold()
                    .Fill.SetBackgroundColor(XLColor.LightGreen)
                    .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                    ;
                rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                hoja.Column("C").Width = 40;
                hoja.Column("D").Width = 20;

                var rngTableUsuarios = hoja.Range(rangoUsuarios);
                rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

                rngTableUsuarios = hoja.Range(tablaUsuarios);
                rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

                #endregion

            #endregion

            #region - Certificacion Usuario -
            


            hoja = libro.Worksheets.Add("Certificacion Usuario");
            hoja.Cells("C2").Value = "Agencia";
            hoja.Cells("D2").Value = "Nombre";
            hoja.Cells("E2").Value = "Apellido";
            hoja.Cells("F2").Value = "Documento";
            hoja.Cells("G2").Value = "Certificacion";

            var CertificacionesUsuariosAgencias = miReport.getCertificacionesUsuariosAgencias(mes, ano);
            cantidadUsuarios = 0;
            foreach (UserProgress miProgreso in CertificacionesUsuariosAgencias)
            {


                hoja.Cells("C" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Agencia;
                hoja.Cells("D" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Nombre;
                hoja.Cells("E" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Apellido;
                hoja.Cells("F" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Documento;
                hoja.Cells("G" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Certificacion;
                cantidadUsuarios++;
            }

            #region - Generar Rangos -

            rangoAgencia = "C2:G2";
            rangoUsuarios = "C2:G" + (2 + cantidadUsuarios).ToString();
            tablaUsuarios = "C2:G" + (2 + cantidadUsuarios).ToString();

            #endregion

            #region - Generar Formato -

            rngTableAgencia = hoja.Range(rangoAgencia);
            //rngTableAgencia.FirstRow().Merge();
            rngTableAgencia.Style
                .Font.SetBold()
                .Fill.SetBackgroundColor(XLColor.LightGreen)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                ;
            rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            hoja.Column("C").Width = 40;
            hoja.Column("D").Width = 30;
            hoja.Column("E").Width = 30;
            hoja.Column("F").Width = 20;
            hoja.Column("G").Width = 20;

            rngTableUsuarios = hoja.Range(rangoUsuarios);
            rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            rngTableUsuarios = hoja.Range(tablaUsuarios);
            rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            #endregion


            #endregion

            #region - Cantidad Usuarios -
            hoja = libro.Worksheets.Add("Cantidad Usuarios");
            hoja.Cells("C2").Value = "Agencia";
            hoja.Cells("D2").Value = "Cantidad";
            
            var CantidadUsuariosAgencias = miReport.getCantidadUsuariosAgencias(mes, ano);
            cantidadUsuarios = 0;
            foreach (UserProgress miProgreso in CantidadUsuariosAgencias)
            {


                hoja.Cells("C" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Agencia;
                hoja.Cells("D" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Cantidad;
                cantidadUsuarios++;
            }

            #region - Generar Rangos -

            rangoAgencia = "C2:D2";
            rangoUsuarios = "C2:D" + (2 + cantidadUsuarios).ToString();
            tablaUsuarios = "C2:D" + (2 + cantidadUsuarios).ToString();

            #endregion

            #region - Generar Formato -

            rngTableAgencia = hoja.Range(rangoAgencia);
            //rngTableAgencia.FirstRow().Merge();
            rngTableAgencia.Style
                .Font.SetBold()
                .Fill.SetBackgroundColor(XLColor.LightGreen)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                ;
            rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            hoja.Column("C").Width = 40;
            hoja.Column("D").Width = 20;


            rngTableUsuarios = hoja.Range(rangoUsuarios);
            rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            rngTableUsuarios = hoja.Range(tablaUsuarios);
            rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            #endregion

            #endregion

            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Certificaciones.xlsx\"");







            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();

        }

        public void ExportToExcelUsuariosAgencia(int id_agencia)
        {

            ReportManagementService miReport = new ReportManagementService();
            // 3 paginas, Certificacion Agencia, Certificacion Usuario, Cantidad Usuarios

            var libro = new XLWorkbook();


            #region  - Certificacion Agencia -
            IXLWorksheet hoja;
            hoja = libro.Worksheets.Add("Usuarios");

            hoja.Cells("C2").Value = "Nombre";
            hoja.Cells("D2").Value = "User";
            hoja.Cells("E2").Value = "Password";
            hoja.Cells("F2").Value = "DNI";
            hoja.Cells("G2").Value = "Estado";


            int mes = DateTime.Now.Month;
            int ano = DateTime.Now.Year;
            var CertificacionesAgencias = miReport.getProgresoUsuarios(id_agencia);
            int cantidadUsuarios = 0;
            foreach (UserProgress miProgreso in CertificacionesAgencias)
            {


                hoja.Cells("C" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Nombre + " " + miProgreso.Apellido;
                hoja.Cells("D" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.User;
                hoja.Cells("E" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Password;
                hoja.Cells("F" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Documento;
                hoja.Cells("G" + (3 + cantidadUsuarios).ToString()).Value = miProgreso.Estado;
                cantidadUsuarios++;
            }

            #region - Generar Rangos -

            var rangoAgencia = "C2:G2";
            var rangoUsuarios = "C2:G" + (2 + cantidadUsuarios).ToString();
            var tablaUsuarios = "C2:G" + (2 + cantidadUsuarios).ToString();

            #endregion

            #region - Generar Formato -

            var rngTableAgencia = hoja.Range(rangoAgencia);
            //rngTableAgencia.FirstRow().Merge();
            rngTableAgencia.Style
                .Font.SetBold()
                .Fill.SetBackgroundColor(XLColor.LightGreen)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                ;
            rngTableAgencia.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            rngTableAgencia.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            hoja.Column("C").Width = 40;
            hoja.Column("D").Width = 20;
            hoja.Column("E").Width = 20;
            hoja.Column("F").Width = 20;
            hoja.Column("G").Width = 20;

            var rngTableUsuarios = hoja.Range(rangoUsuarios);
            rngTableUsuarios.Style.Border.InsideBorder = XLBorderStyleValues.Thin;
            rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thin;

            rngTableUsuarios = hoja.Range(tablaUsuarios);
            rngTableUsuarios.Style.Border.OutsideBorder = XLBorderStyleValues.Thick;

            #endregion

            #endregion



            Response.Clear();
            Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            Response.AddHeader("content-disposition", "attachment;filename=\"Usuarios.xlsx\"");


            // Flush the workbook to the Response.OutputStream
            using (MemoryStream memoryStream = new MemoryStream())
            {
                libro.SaveAs(memoryStream);
                memoryStream.WriteTo(Response.OutputStream);
                memoryStream.Close();
            }

            Response.End();

        }

    }

}