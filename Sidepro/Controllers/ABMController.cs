﻿using Sidepro.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sidepro.Application;
using System.Web.Script.Serialization;
using System.Threading.Tasks;
using System.IO;
using Sidepro.Application.Helpers;
namespace Sidepro.Controllers
{
    public class ABMController : Controller
    {
        //
        // GET: /ABM/
        ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
        CursosManagementService cursosManagementService = new CursosManagementService();
        LeccionManagementService leccionManagementService = new LeccionManagementService();
        AgenciaManagementService agenciaManagementService = new AgenciaManagementService();
        RolManagementService rolManagementService = new RolManagementService();
        CanalManagementService canalManagementService = new CanalManagementService();
        public ActionResult Modulo()
        {
            return View( new indexViewModel());
        }
        public ActionResult Curso(int id_modulo, int? id_curso)
        {
            var miViewModel = new ABMViewModelController().getABMViewModel(id_modulo, id_curso);
            return View(miViewModel);
        }
        public ActionResult Leccion(int id_modulo, int id_curso)
        {
            var miViewModel = new ABMViewModelController().getLessonABMViewModel(id_modulo, id_curso);
            return View(miViewModel);
        }
        public ActionResult Pregunta(int id_modulo, int id_curso, int id_leccion)
        {
            var miViewModel = new ABMViewModelController().getQuestionABMViewModel(id_modulo, id_curso, id_leccion);
            return View(miViewModel);
        }
        public ActionResult Certificacion(int id_modulo, int id_curso, int id_leccion)
        {
            var miViewModel = new ABMViewModelController().getQuestionABMViewModel(id_modulo, id_curso, id_leccion);
            return View(miViewModel);
        }
        public ActionResult Respuesta(int id_modulo, int id_curso, int id_leccion, int id_pregunta)
        {
            var miViewModel = new ABMViewModelController().getAnswerABMViewModel(id_modulo, id_curso, id_leccion, id_pregunta);
            return View(miViewModel);
        }
        public ActionResult Agencia()
        {
            return View(new indexViewModel());
        }
        public ActionResult Rol()
        {
            return View(new indexViewModel());
        }
        public ActionResult RolModulo(int id_rol)
        {
            RolModuleViewModel miViewModel = new ABMViewModelController().getRolModuleViewModel(id_rol);
            return View(miViewModel);
        }
        public ActionResult RolCurso(int id_rol, int id_modulo, int? id_curso)
        {
            var miViewModel = new ABMViewModelController().getRolCourseViewModel(id_rol, id_modulo, id_curso);
            return View(miViewModel);
        }
        public ActionResult destacado()
        {
            return View(new indexViewModel());
        }
        public ActionResult subirArchivos()
        {
            return View(new indexViewModel());
        }
        public ActionResult canal()
        {
            return View(new indexViewModel());
        }
        #region Modulos
            public ActionResult GetModulos()
            {
                //ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
                var misModulos = moduloCursosManagementService.getModulos();

                return Json(new { modulos = misModulos }, JsonRequestBehavior.AllowGet);

            }
            public ActionResult AgregarModulo(string Nombre, string Controlador, string View, string Style){
                
                string Tags = "";
                string Mensaje = moduloCursosManagementService.agregarModulo(Nombre, Controlador, View, Style, Tags);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarModulo(int ID_Modulo, string Nombre, string Controlador, string View, string Style)
            {
                string Tags = "";
                string Mensaje = moduloCursosManagementService.modificarModulo(ID_Modulo, Nombre, Controlador, View, Style, Tags);
                return Json(new { Mensaje = Mensaje  }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult InhabilitarModulo(int ID_Modulo)
            {
                string Mensaje = moduloCursosManagementService.inhabilitarModulo(ID_Modulo);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarModulo(int ID_Modulo)
            {
                string Mensaje = moduloCursosManagementService.habilitarModulo(ID_Modulo);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
        #endregion
        #region Cursos
            public ActionResult GetCursos(int id_modulo, int? id_curso)
            {
                //ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
                var misCursos = cursosManagementService.getCursos(id_modulo, id_curso);

                return Json(new { cursos = misCursos }, JsonRequestBehavior.AllowGet);

            }
            public ActionResult AgregarCurso(int Id_modulo, string Nombre, string Descripcion, string Controlador, string View, string Style,string Imagen, int? Parent)
            {

                string Mensaje = cursosManagementService.agregarCurso(Id_modulo, Nombre, Descripcion, Controlador, View, Style, Imagen, Parent);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarCurso(int ID_Modulo, int Id_curso, string Nombre, string Descripcion, string Controlador, string View, string Style, string Imagen)
            {

                string Mensaje = cursosManagementService.modificarCurso(ID_Modulo, Id_curso, Nombre, Descripcion, Controlador, View, Style, Imagen);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult InhabilitarCurso(int ID_Modulo, int Id_curso)
            {
                string Mensaje = cursosManagementService.InhabilitarCurso(ID_Modulo, Id_curso);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarCurso(int ID_Modulo, int Id_curso)
            {
                string Mensaje = cursosManagementService.HabilitarCurso(ID_Modulo, Id_curso);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            
        #endregion
        #region Lecciones
            public ActionResult GetLeccion(int id_modulo, int id_curso)
            {
                //ModuloCursosManagementService moduloCursosManagementService = new ModuloCursosManagementService();
                var misLecciones = leccionManagementService.getLecciones(id_modulo, id_curso);

                return Json(new { lecciones = misLecciones }, JsonRequestBehavior.AllowGet);

            }
            public ActionResult AgregarLeccion(int Id_modulo, int Id_curso, string Nombre, string Descripcion, string Imagen, string Video, decimal Pje_aprobacion)
            {
                string Mensaje = leccionManagementService.AgregarLeccion(Id_modulo, Id_curso, Nombre,  Descripcion,  Imagen,  Video,  Pje_aprobacion);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarLeccion(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Descripcion, string Imagen, string Video, decimal Pje_aprobacion, int? Orden)
            {
                string Mensaje = leccionManagementService.ModificarLeccion(Id_modulo, Id_curso, Id_leccion, Nombre, Descripcion, Imagen, Video, Pje_aprobacion, Orden);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult InhabilitarLeccion(int ID_Modulo, int Id_curso, int Id_leccion)
            {
                string Mensaje = leccionManagementService.InhabilitarLeccion(ID_Modulo, Id_curso, Id_leccion);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarLeccion(int ID_Modulo, int Id_curso, int Id_leccion)
            {
                string Mensaje = leccionManagementService.HabilitarLeccion(ID_Modulo, Id_curso, Id_leccion);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

        #endregion 
        #region Pregunta

            public ActionResult GetPregunta(int id_modulo, int id_curso, int Id_leccion)
            {

                var misPreguntas = leccionManagementService.GetPregunta(id_modulo, id_curso, Id_leccion);
                return Json(new { preguntas = misPreguntas }, JsonRequestBehavior.AllowGet);

            }
            public ActionResult AgregarPregunta(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Correcto, string Incorrecto)
            {
                string Mensaje = leccionManagementService.AgregarPregunta(Id_modulo, Id_curso, Id_leccion, Nombre, Correcto, Incorrecto);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarPregunta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, string Correcto, string Incorrecto, decimal Cant_puntos)
            {
                string Mensaje = leccionManagementService.ModificarPregunta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Correcto, Incorrecto, Cant_puntos);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

            public ActionResult InhabilitarPregunta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta)
            {
                string Mensaje = leccionManagementService.InhabilitarPregunta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarPregunta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta)
            {
                string Mensaje = leccionManagementService.HabilitarPregunta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarPregCertificacion (int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Correcto, string Incorrecto, int categoria, int moduloCert, int cursoCert, int leccionCert)
            {
                string Mensaje = leccionManagementService.AgregarPreguntaCertificacion(Id_modulo, Id_curso, Id_leccion, Nombre, Correcto, Incorrecto, categoria, moduloCert,  cursoCert,  leccionCert);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarPregCertificacion(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, string Correcto, string Incorrecto, decimal Cant_puntos, int categoria, int moduloCert, int cursoCert, int leccionCert)
            {
                string Mensaje = leccionManagementService.ModificarPreguntaCertificacion(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Correcto, Incorrecto, Cant_puntos,categoria,  moduloCert,  cursoCert,  leccionCert);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
        #endregion
        #region Respuesta
            public ActionResult GetRespuesta(int id_modulo, int id_curso, int Id_leccion, int id_pregunta)
            {
                var misRespuestas = leccionManagementService.GetRespuesta(id_modulo, id_curso, Id_leccion, id_pregunta);
                return Json(new { respuestas = misRespuestas }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarRespuesta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, int Sn_correcto)
            {
                string Mensaje = leccionManagementService.AgregarRespuesta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Sn_correcto);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarRespuesta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta, string Nombre, int Sn_correcto)
            {
                string Mensaje = leccionManagementService.ModificarRespuesta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta, Nombre, Sn_correcto);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

            public ActionResult InhabilitarRespuesta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta)
            {
                string Mensaje = leccionManagementService.InhabilitarRespuesta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult HabilitarRespuesta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta)
            {
                string Mensaje = leccionManagementService.HabilitarRespuesta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
        #endregion
        #region Agencias
            public ActionResult GetAgencias()
            {
                var misAgencias = agenciaManagementService.getAgencias();
                return Json(new { Agencias = misAgencias }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetTipoAgencias()
            {
                var tipoAgencias = agenciaManagementService.getTipoAgencias();
                return Json(new { TipoAgencias = tipoAgencias }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarAgencia(string Agencia, string Documento, string Email, int TipoAgencia)
            {

                string Mensaje = agenciaManagementService.agregarAgencia(Agencia, Documento, Email, TipoAgencia);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarAgencia(int id_agencia, string Agencia, string Documento, string Email, int TipoAgencia)
            {
                string Mensaje = agenciaManagementService.modificarAgencia(id_agencia, Agencia, Documento, Email, TipoAgencia);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

        #endregion
        #region Roles
            public ActionResult GetRoles()
            {
                var misRoles = rolManagementService.getRoles();
                return Json(new { Roles = misRoles }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarRol(int cod_canal, string Rol)
            {
                string Mensaje = rolManagementService.agregarRol(cod_canal, Rol);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarRol(int cod_canal, int id_rol, string Rol)
            {
                string Mensaje = rolManagementService.modificarRol(cod_canal, id_rol, Rol);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }

        #endregion
        #region RolesModulos

            public ActionResult GetRolModulos(int id_rol)
            {
                var misModulos = rolManagementService.getRolModulos(id_rol);
                return Json(new { Modulos = misModulos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetModulosABM(int id_rol)
            {
                var misModulos = rolManagementService.getModulos(id_rol);
                return Json(new { Modulos = misModulos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarRolModulo(int ID_Rol, int ID_Modulo)
            {
                string Mensaje = rolManagementService.agregarRolModulo(ID_Rol, ID_Modulo);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult EliminarRolModulo(int ID_Modulo, int ID_Rol)
            {
                string Mensaje = rolManagementService.eliminarRolModulo(ID_Modulo, ID_Rol);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }


        #endregion
        #region RolesCursos
            public ActionResult GetRolCursos(int id_rol, int id_modulo, int? id_curso)
            {
                var misCursos = rolManagementService.getRolCursos(id_rol, id_modulo, id_curso);
                return Json(new { Cursos = misCursos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetCursosABM(int id_rol, int id_modulo, int? id_curso)
            {
                var misModulos = rolManagementService.getCursos(id_rol, id_modulo, id_curso);
                return Json(new { Cursos = misModulos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarRolCurso(int ID_Rol, int ID_Modulo, int ID_Curso, int? parent)
            {
                string Mensaje = rolManagementService.agregarRolCurso(ID_Rol, ID_Modulo,ID_Curso,parent);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult EliminarRolCurso(int ID_Modulo, int ID_Curso, int ID_Rol)
            {
                string Mensaje = rolManagementService.eliminarRolCurso(ID_Modulo,ID_Curso, ID_Rol);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }        
        
        #endregion
        #region Destacados
            public ActionResult GetDestacados()
            {
                var misDestacados = rolManagementService.getDestacados();
                return Json(new { Destacados = misDestacados }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetRolesDestacado() {
                var misRoles = rolManagementService.getRolesDestacados();
                return Json(new { Roles = misRoles }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetModulosDestacado(int cod_rol ) {
                var misModulos = rolManagementService.getModulosDestacados(cod_rol);
                return Json(new { Modulos = misModulos }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetCursosDestacado(int cod_rol, int id_modulo, int? id_curso)
            {
                var misCursos  = rolManagementService.getCursosDestacados(cod_rol, id_modulo, id_curso);
                return Json(new { Cursos = misCursos}, JsonRequestBehavior.AllowGet);
            }
            public ActionResult GetTipoDestacados()
            {
                var misDestacados = rolManagementService.getTipoDestacados();
                return Json(new { Destacados = misDestacados }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult ModificarDestacado(int id_destacado, int cod_tipo_destacado, string url_imagen, int orden)
            {
                string Mensaje = rolManagementService.ModificarDestacado(id_destacado, cod_tipo_destacado, url_imagen, orden);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult AgregarDestacado(int id_modulo, int? id_curso,int cod_rol, int cod_tipo_destacado, string url_imagen)
            {
                string Mensaje = rolManagementService.AgregarDestacado(id_modulo, id_curso, cod_rol, cod_tipo_destacado, url_imagen);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
            public ActionResult InhabilitarDestacado(int id_destacado)
            {
                string Mensaje = rolManagementService.EliminarDestacado(id_destacado);
                return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
            }
        #endregion
        #region Archivos

            public bool checkArchivos(string fileName, int tipo_archivo)
            {

                string extension = Path.GetExtension(fileName).Remove(0, 1);// fileName.Split('.')[fileName.Split('.').Length - 1];

                if (tipo_archivo == 1)
                {
                    if ((extension.ToLower() == "gif") || (extension.ToLower() == "jpg") || (extension.ToLower() == "jpeg") || (extension.ToLower() == "png"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (tipo_archivo == 0) { 
                        if ((extension.ToLower() == "mp4") || (extension.ToLower() == "avi") || (extension.ToLower() == "mpeg4"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if ((extension.ToLower() == "pdf"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            [HttpPost]
            public ActionResult GrabarArchivo()
            {
                int id_modulo = Convert.ToInt32(Request["id_modulo"]);
                int id_curso = Convert.ToInt32(Request["id_curso"]);
                int id_leccion = Convert.ToInt32(Request["id_leccion"]);
                int tipo_archivo = Convert.ToInt32(Request["cod_tipo_archivo"]);


                try
                {
                    foreach (string file in Request.Files)
                    {
                        if (!checkArchivos(Request.Files[file].FileName, tipo_archivo))
                        {
                            return Json(new { Mensaje = "Error, tipo de archivo no válido" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    foreach (string file in Request.Files)
                    {
                        
                        var fileContent = Request.Files[file];
                        if (fileContent != null && fileContent.ContentLength > 0)
                        {
                            // get a stream
                            var stream = fileContent.InputStream;
                            // and optionally write the file to disk
                            var fileName = Path.GetFileName(file);
                            var path = "";
                            var path2 = "";


                            string nombre = new HelperManagementService().saveFile(id_modulo, id_curso, id_leccion, tipo_archivo, Request.Files[file].FileName);
                            if (tipo_archivo == 0 ){
                                path = Path.Combine(Server.MapPath("~/Images/Videos/"), nombre);
                                /*
                                path = Path.Combine(@"\\184.168.147.58\portalMovistar\Images\Videos\",nombre);
                                path2 = Path.Combine(@"\\192.169.231.95\PortalMovistar\Images\Videos\", nombre);
                                 */
                            }else{ 
                                if (tipo_archivo == 1)
                                {
                                    if (id_modulo == 0)
                                    {
                                        path = Path.Combine(Server.MapPath("~/Images/Modulos/Modulo_inicial/"), nombre);
                                        /*
                                        path = Path.Combine(@"\\184.168.147.58\portalMovistar\Images\Modulos\Modulo_inicial\", nombre);
                                        path2 = Path.Combine(@"\\192.169.231.95\PortalMovistar\Images\Modulos\Modulo_inicial\", nombre);
                                        */ 

                                    }
                                    else
                                    {
                                        if (id_modulo == 1)
                                        {
                                            path = Path.Combine(Server.MapPath("~/Images/Modulos/Equipos/"), nombre);
                                            /*
                                            path = Path.Combine(@"\\184.168.147.58\portalMovistar\Images\Modulos\Equipos\", nombre);
                                            path2 = Path.Combine(@"\\192.169.231.95\PortalMovistar\Images\Modulos\Equipos\", nombre);
                                            */
                                        }
                                        else
                                        {
                                            path = Path.Combine(Server.MapPath("~/Images/Modulos/Ofertas_y_campañas/"), nombre);
                                            /*
                                            path = Path.Combine(@"\\184.168.147.58\portalMovistar\Images\Modulos\Ofertas_y_campañas\", nombre);
                                            path2 = Path.Combine(@"\\192.169.231.95\PortalMovistar\Images\Modulos\Ofertas_y_campañas\", nombre);
                                            */
                                        }
                                    }
                                }
                                else
                                {
                                    path = Path.Combine(Server.MapPath("~/Images/Noticias/"), nombre);
                                    /*
                                    path = Path.Combine(@"\\184.168.147.58\portalMovistar\Images\Noticias\", nombre);
                                    path2 = Path.Combine(@"\\192.169.231.95\PortalMovistar\Images\Noticias\", nombre);
                                    */
                                }
                            
                            }
                            /*
                            using (var fileStream = System.IO.File.Create(path2))
                            {
                                stream.CopyTo(fileStream);
                            }              
                            */
                            using (var fileStream = System.IO.File.Create(path))
                            {
                                stream.CopyTo(fileStream);
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    return Json(new { Mensaje = ex.Message }, JsonRequestBehavior.AllowGet);
                }
                
                

                /*
               string path = System.IO.Path.Combine(Server.MapPath("~/Images"), System.IO.Path.GetFileName(file.FileName));
               file.SaveAs(@"C:\testFiles\archivo.txt");
               ViewBag.Message = "File uploaded successfully";
               return RedirectToAction("subirArchivos", "ABM");
                 * */
                return Json(new { Mensaje = "Archivo subido con éxito" }, JsonRequestBehavior.AllowGet);
            }
            
        #endregion

        #region Canales
        
        public ActionResult GetCanales()
        {
            var misCanales = canalManagementService.getCanales();
            return Json(new { Canales = misCanales }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AgregarCanal(string Descripcion)
        {
            string Mensaje = canalManagementService.agregarCanal(Descripcion);
            return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ModificarCanal(int cod_canal, string Descripcion)
        {
            string Mensaje = canalManagementService.modificarCanal(cod_canal,Descripcion);
            return Json(new { Mensaje = Mensaje }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public ActionResult DesbloquearUsuario(int cod_usuario)
        {
            var MailUsuario = new UsuarioManagementService().DesbloquearUsuario(cod_usuario);
            if (MailUsuario.Mensaje == "")
            {
                string body = getBodyMailDesbloqueo(MailUsuario);
                string to = MailUsuario.Mail +"," + MailUsuario.MailAgencia;
                string title = "Desbloqueo Usuarios Sidepro";
                MailUsuario.Mensaje = new HelperManagementService().enviarMailGmail(to, title, body);
            }
            
            return Json(new { Mensaje = MailUsuario.Mensaje }, JsonRequestBehavior.AllowGet);
        }
        public string getBodyMailDesbloqueo(Sidepro.Entities.MailUsuario miMail){

            string body = "";
body = body + @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional //EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'><html><head>";
body = body + @"            <title>Reporte General - Portal Training</title>";
body = body + @"            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
body = body + @"            <style type='text/css'>";
body = body + @"        .font-sans-serif{font-family:sans-serif}.font-avenir{font-family:Avenir,sans-serif}.mso .font-avenir{font-family:sans-serif!important}.font-lato{font-family:Lato,Tahoma,sans-serif}.mso .font-lato{font-family:Tahoma,sans-serif!important}.font-cabin{font-family:Cabin,Avenir,sans-serif}.mso .font-cabin{font-family:sans-serif!important}.font-open-Sans{font-family:'Open Sans',sans-serif}.mso .font-open-Sans{font-family:sans-serif!important}.font-Raleway{font-family:Raleway,Tahoma,sans-serif}.mso .font-Raleway{font-family:Tahoma,sans-serif!important}.font-ubuntu{font-family:Ubuntu,sans-serif}.mso .font-ubuntu{font-family:sans-serif!important}.font-pt-sans{font-family:'PT Sans','Trebuchet MS',sans-serif}.mso .font-pt-sans{font-family:'Trebuchet MS',sans-serif!important}.font-georgia{font-family:Georgia,serif}.font-merriweather{font-family:Merriweather,Georgia,serif}.mso .font-merriweather{font-family:Georgia,serif!important}.font-bitter{font-family:Bitter,Georgia,serif}.mso .font-bitter{font-family:Georgia,serif!important}.font-pt-serif{font-family:'PT Serif',Georgia,serif}.mso .font-pt-serif{font-family:Georgia,serif!important}.font-pompiere{font-family:Pompiere,'Trebuchet MS',sans-serif}.mso .font-pompiere{font-family:'Trebuchet MS',sans-serif!important}.font-Raleway-slab{font-family:'Raleway Slab',Georgia,serif}.mso .font-Raleway-slab{font-family:Georgia,serif!important}@media only screen and (max-width: 620px){.wrapper .column .size-8{font-size:8px!important;line-height:14px!important}.wrapper .column .size-9{font-size:9px!important;line-height:16px!important}.wrapper .column .size-10{font-size:10px!important;line-height:18px!important}.wrapper .column .size-11{font-size:11px!important;line-height:19px!important}.wrapper .column .size-12{font-size:12px!important;line-height:19px!important}.wrapper .column .size-13{font-size:13px!important;line-height:21px!important}.wrapper .column .size-14{font-size:14px!important;line-height:21px!important}.wrapper .column .size-15{font-size:15px!important;line-height:23px!important}.wrapper .column .size-16{font-size:16px!important;line-height:24px!important}.wrapper .column .size-17{font-size:17px!important;line-height:26px!important}.wrapper .column .size-18{font-size:17px!important;line-height:26px!important}.wrapper .column .size-20{font-size:17px!important;line-height:26px!important}.wrapper .column .size-22{font-size:18px!important;line-height:26px!important}.wrapper .column .size-24{font-size:20px!important;line-height:28px!important}.wrapper .column .size-26{font-size:22px!important;line-height:31px!important}.wrapper .column .size-28{font-size:24px!important;line-height:32px!important}.wrapper .column .size-30{font-size:26px!important;line-height:34px!important}.wrapper .column .size-32{font-size:28px!important;line-height:36px!important}.wrapper .column .size-34{font-size:30px!important;line-height:38px!important}.wrapper .column .size-36{font-size:30px!important;line-height:38px!important}.wrapper .column .size-40{font-size:32px!important;line-height:40px!important}.wrapper .column .size-44{font-size:34px!important;line-height:43px!important}.wrapper .column .size-48{font-size:36px!important;line-height:43px!important}.wrapper .column .size-56{font-size:40px!important;line-height:47px!important}.wrapper .column .size-64{font-size:44px!important;line-height:50px!important}}body{margin:0;padding:0;min-width:100%}.mso body{mso-line-height-rule:exactly}.no-padding .wrapper .column .column-top,.no-padding .wrapper .column .column-bottom{font-size:0;line-height:0}table{border-collapse:collapse;border-spacing:0}td{padding:0;vertical-align:top}.spacer,.border{font-size:1px;line-height:1px}.spacer{width:100%}img{border:0;-ms-interpolation-mode:bicubic}.image{font-size:12px;mso-line-height-rule:at-least}.image img{display:block}.logo{mso-line-height-rule:at-least}.logo img{display:block}strong{font-weight:700}h1,h2,h3,p,ol,ul,blockquote,.image{font-style:normal;font-weight:400}ol,ul,li{padding-left:0}blockquote{margin-left:0;margin-right:0;padding-right:0}.column-top,.column-bottom{font-size:32px;line-height:32px;transition-timing-function:cubic-bezier(0,0,0.2,1);transition-duration:150ms;transition-property:all}.half-padding .column .column-top,.half-padding .column .column-bottom{font-size:16px;line-height:16px}.column{text-align:left}.contents{table-layout:fixed;width:100%}.padded{padding-left:32px;padding-right:32px;word-break:break-word;word-wrap:break-word}.wrapper{display:table;table-layout:fixed;width:100%;min-width:620px;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}.wrapper a{transition:opacity .2s ease-in}table.wrapper{table-layout:fixed}.one-col,.two-col,.three-col{margin-left:auto;margin-right:auto;width:600px}.centered{margin-left:auto;margin-right:auto}.btn a{border-radius:3px;display:inline-block;font-size:14px;font-weight:700;line-height:24px;padding:13px 35px 12px;text-align:center;text-decoration:none!important}.btn a:hover{opacity:.8}.two-col .btn a{font-size:12px;line-height:22px;padding:10px 28px}.three-col .btn a,.wrapper .narrower .btn a{font-size:11px;line-height:19px;padding:6px 18px 5px}@media only screen and (max-width: 620px){.btn a{display:block!important;font-size:14px!important;line-height:24px!important;padding:13px 10px 12px!important}}.two-col .column{width:300px}.two-col .first .padded{padding-left:32px;padding-right:16px}.two-col .second .padded{padding-left:16px;padding-right:32px}.three-col .column{width:200px}.three-col .first .padded{padding-left:32px;padding-right:8px}.three-col .second .padded{padding-left:20px;padding-right:20px}.three-col .third .padded{padding-left:8px;padding-right:32px}.wider{width:400px}.narrower{width:200px}@media only screen and (min-width: 0){.wrapper{text-rendering:optimizeLegibility}}@media only screen and (max-width: 620px){[class=wrapper]{min-width:320px!important;width:100%!important}[class=wrapper] .one-col,[class=wrapper] .two-col,[class=wrapper] .three-col{width:320px!important}[class=wrapper] .column,[class=wrapper] .gutter{display:block;float:left;width:320px!important}[class=wrapper] .padded{padding-left:20px!important;padding-right:20px!important}[class=wrapper] .block{display:block!important}[class=wrapper] .hide{display:none!important}[class=wrapper] .image img{height:auto!important;width:100%!important}}.footer{width:100%}.footer .inner{padding:58px 0 29px;width:600px}.footer .left td,.footer .right td{font-size:12px;line-height:22px}.footer .left td{text-align:left;width:400px}.footer .right td{max-width:200px;mso-line-height-rule:at-least}.footer .links{line-height:26px;margin-bottom:26px;mso-line-height-rule:at-least}.footer .links a:hover{opacity:.8}.footer .links img{vertical-align:middle}.footer .address{margin-bottom:18px}.footer .campaign{margin-bottom:18px}.footer .campaign a{font-weight:700;text-decoration:none}.footer .sharing div{margin-bottom:5px}.wrapper .footer .fblike,.wrapper .footer .tweet,.wrapper .footer .linkedinshare,.wrapper .footer .forwardtoafriend{background-repeat:no-repeat;background-size:200px 56px;border-radius:2px;color:#fff;display:block;font-size:11px;font-weight:700;line-height:11px;padding:8px 11px 7px 28px;text-align:left;text-decoration:none}.wrapper .footer .fblike:hover,.wrapper .footer .tweet:hover,.wrapper .footer .linkedinshare:hover,.wrapper .footer .forwardtoafriend:hover{color:#fff!important;opacity:.8}.footer .fblike{background-image:url(https://i3.createsend1.com/static/eb/master/03-fresh/imgf/fblike.png)}.footer .tweet{background-image:url(https://i4.createsend1.com/static/eb/master/03-fresh/imgf/tweet.png)}.footer .linkedinshare{background-image:url(https://i5.createsend1.com/static/eb/master/03-fresh/imgf/lishare.png)}.footer .forwardtoafriend{background-image:url(https://i6.createsend1.com/static/eb/master/03-fresh/imgf/forward.png)}@media only screen and (-webkit-min-device-pixel-ratio: 2),only screen and (min--moz-device-pixel-ratio: 2),only screen and (-o-min-device-pixel-ratio: 21),only screen and (min-device-pixel-ratio: 2),only screen and (min-resolution: 192dpi),only screen and (min-resolution: 2dppx){.footer .fblike{background-image:url(https://i7.createsend1.com/static/eb/master/03-fresh/imgf/fblike@2x.png)!important}.footer .tweet{background-image:url(https://i8.createsend1.com/static/eb/master/03-fresh/imgf/tweet@2x.png)!important}.footer .linkedinshare{background-image:url(https://i9.createsend1.com/static/eb/master/03-fresh/imgf/lishare@2x.png)!important}.footer .forwardtoafriend{background-image:url(https://i10.createsend1.com/static/eb/master/03-fresh/imgf/forward@2x.png)!important}}@media only screen and (max-width: 620px){.footer{width:320px!important}.footer td{display:none}.footer .inner,.footer .inner td{display:block;text-align:center!important;max-width:320px!important;width:320px!important}.footer .sharing{margin-bottom:40px}.footer .sharing div{display:inline-block}.footer .fblike,.footer .tweet,.footer .linkedinshare,.footer .forwardtoafriend{display:inline-block!important}}.wrapper h1,.wrapper h2,.wrapper h3,.wrapper p,.wrapper ol,.wrapper ul,.wrapper li,.wrapper blockquote,.image,.btn,.divider{margin-bottom:0;margin-top:0}.wrapper .column h1 + *{margin-top:20px}.wrapper .column h2 + *{margin-top:16px}.wrapper .column h3 + *{margin-top:12px}.wrapper .column p + *,.wrapper .column ol + *,.wrapper .column ul + *,.wrapper .column blockquote + *,.image + .contents td > :first-child{margin-top:24px}.contents:nth-last-child(n+3) h1:last-child,.no-padding .contents:nth-last-child(n+2) h1:last-child{margin-bottom:20px}.contents:nth-last-child(n+3) h2:last-child,.no-padding .contents:nth-last-child(n+2) h2:last-child{margin-bottom:16px}.contents:nth-last-child(n+3) h3:last-child,.no-padding .contents:nth-last-child(n+2) h3:last-child{margin-bottom:12px}.contents:nth-last-child(n+3) p:last-child,.no-padding .contents:nth-last-child(n+2) p:last-child,.contents:nth-last-child(n+3) ol:last-child,.no-padding .contents:nth-last-child(n+2) ol:last-child,.contents:nth-last-child(n+3) ul:last-child,.no-padding .contents:nth-last-child(n+2) ul:last-child,.contents:nth-last-child(n+3) blockquote:last-child,.no-padding .contents:nth-last-child(n+2) blockquote:last-child,.contents:nth-last-child(n+3) .image,.no-padding .contents:nth-last-child(n+2) .image,.contents:nth-last-child(n+3) .divider,.no-padding .contents:nth-last-child(n+2) .divider,.contents:nth-last-child(n+3) .btn,.no-padding .contents:nth-last-child(n+2) .btn{margin-bottom:24px}.two-col .column p + *,.wider .column p + *,.two-col .column ol + *,.wider .column ol + *,.two-col .column ul + *,.wider .column ul + *,.two-col .column blockquote + *,.wider .column blockquote + *,.two-col .image + .contents td > :first-child,.wider .image + .contents td > :first-child{margin-top:21px}.two-col .contents:nth-last-child(n+3) p:last-child,.wider .contents:nth-last-child(n+3) p:last-child,.no-padding .two-col .contents:nth-last-child(n+2) p:last-child,.no-padding .wider .contents:nth-last-child(n+2) p:last-child,.two-col .contents:nth-last-child(n+3) ol:last-child,.wider .contents:nth-last-child(n+3) ol:last-child,.no-padding .two-col .contents:nth-last-child(n+2) ol:last-child,.no-padding .wider .contents:nth-last-child(n+2) ol:last-child,.two-col .contents:nth-last-child(n+3) ul:last-child,.wider .contents:nth-last-child(n+3) ul:last-child,.no-padding .two-col .contents:nth-last-child(n+2) ul:last-child,.no-padding .wider .contents:nth-last-child(n+2) ul:last-child,.two-col .contents:nth-last-child(n+3) blockquote:last-child,.wider .contents:nth-last-child(n+3) blockquote:last-child,.no-padding .two-col .contents:nth-last-child(n+2) blockquote:last-child,.no-padding .wider .contents:nth-last-child(n+2) blockquote:last-child,.two-col .contents:nth-last-child(n+3) .image,.wider .contents:nth-last-child(n+3) .image,.no-padding .two-col .contents:nth-last-child(n+2) .image,.no-padding .wider .contents:nth-last-child(n+2) .image,.two-col .contents:nth-last-child(n+3) .divider,.wider .contents:nth-last-child(n+3) .divider,.no-padding .two-col .contents:nth-last-child(n+2) .divider,.no-padding .wider .contents:nth-last-child(n+2) .divider,.two-col .contents:nth-last-child(n+3) .btn,.wider .contents:nth-last-child(n+3) .btn,.no-padding .two-col .contents:nth-last-child(n+2) .btn,.no-padding .wider .contents:nth-last-child(n+2) .btn{margin-bottom:21px}.three-col .column p + *,.narrower .column p + *,.three-col .column ol + *,.narrower .column ol + *,.three-col .column ul + *,.narrower .column ul + *,.three-col .column blockquote + *,.narrower .column blockquote + *,.three-col .image + .contents td > :first-child,.narrower .image + .contents td > :first-child{margin-top:18px}.three-col .contents:nth-last-child(n+3) p:last-child,.narrower .contents:nth-last-child(n+3) p:last-child,.no-padding .three-col .contents:nth-last-child(n+2) p:last-child,.no-padding .narrower .contents:nth-last-child(n+2) p:last-child,.three-col .contents:nth-last-child(n+3) ol:last-child,.narrower .contents:nth-last-child(n+3) ol:last-child,.no-padding .three-col .contents:nth-last-child(n+2) ol:last-child,.no-padding .narrower .contents:nth-last-child(n+2) ol:last-child,.three-col .contents:nth-last-child(n+3) ul:last-child,.narrower .contents:nth-last-child(n+3) ul:last-child,.no-padding .three-col .contents:nth-last-child(n+2) ul:last-child,.no-padding .narrower .contents:nth-last-child(n+2) ul:last-child,.three-col .contents:nth-last-child(n+3) blockquote:last-child,.narrower .contents:nth-last-child(n+3) blockquote:last-child,.no-padding .three-col .contents:nth-last-child(n+2) blockquote:last-child,.no-padding .narrower .contents:nth-last-child(n+2) blockquote:last-child,.three-col .contents:nth-last-child(n+3) .image,.narrower .contents:nth-last-child(n+3) .image,.no-padding .three-col .contents:nth-last-child(n+2) .image,.no-padding .narrower .contents:nth-last-child(n+2) .image,.three-col .contents:nth-last-child(n+3) .divider,.narrower .contents:nth-last-child(n+3) .divider,.no-padding .three-col .contents:nth-last-child(n+2) .divider,.no-padding .narrower .contents:nth-last-child(n+2) .divider,.three-col .contents:nth-last-child(n+3) .btn,.narrower .contents:nth-last-child(n+3) .btn,.no-padding .three-col .contents:nth-last-child(n+2) .btn,.no-padding .narrower .contents:nth-last-child(n+2) .btn{margin-bottom:18px}@media only screen and (max-width: 620px){.wrapper p + *,.wrapper ol + *,.wrapper ul + *,.wrapper blockquote + *,.image + .contents td > :first-child{margin-top:24px!important}.contents:nth-last-child(n+3) p:last-child,.no-padding .contents:nth-last-child(n+2) p:last-child,.contents:nth-last-child(n+3) ol:last-child,.no-padding .contents:nth-last-child(n+2) ol:last-child,.contents:nth-last-child(n+3) ul:last-child,.no-padding .contents:nth-last-child(n+2) ul:last-child,.contents:nth-last-child(n+3) blockquote:last-child,.no-padding .contents:nth-last-child(n+2) blockquote:last-child,.contents:nth-last-child(n+3) .image:last-child,.no-padding .contents:nth-last-child(n+2) .image:last-child,.contents:nth-last-child(n+3) .divider:last-child,.no-padding .contents:nth-last-child(n+2) .divider:last-child,.contents:nth-last-child(n+3) .btn:last-child,.no-padding .contents:nth-last-child(n+2) .btn:last-child{margin-bottom:24px!important}}td.border{width:1px}tr.border{background-color:#e3e3e3;height:1px}tr.border td{line-height:1px}.sidebar{width:600px}.first.wider .padded{padding-left:32px;padding-right:20px}.second.wider .padded{padding-left:20px;padding-right:32px}.first.narrower .padded{padding-left:32px;padding-right:8px}.second.narrower .padded{padding-left:8px;padding-right:32px}.wrapper h1{font-size:40px;line-height:48px}.wrapper h2{font-size:24px;line-height:32px}.wrapper h3{font-size:18px;line-height:24px}.wrapper h1 a,.wrapper h2 a,.wrapper h3 a{text-decoration:none}.wrapper a:hover{text-decoration:none}.wrapper p,.wrapper ol,.wrapper ul{font-size:15px;line-height:24px}.wrapper ol,.wrapper ul{margin-left:20px}.wrapper li{padding-left:2px}.wrapper blockquote{margin-left:0;padding-left:18px}.one-col,.two-col,.three-col,.sidebar{background-color:#fff;table-layout:fixed}.wrapper .two-col blockquote,.wrapper .wider blockquote{padding-left:16px}.wrapper .three-col ol,.wrapper .narrower ol,.wrapper .three-col ul,.wrapper .narrower ul{margin-left:14px}.wrapper .three-col li,.wrapper .narrower li{padding-left:1px}.wrapper .three-col blockquote,.wrapper .narrower blockquote{padding-left:12px}.wrapper h2{font-weight:700}.wrapper blockquote{font-style:italic}.preheader a,.header a{text-decoration:none}.header{margin-left:auto;margin-right:auto;width:600px}.preheader td{padding-bottom:24px;padding-top:24px}.logo{width:280px}.logo div{font-weight:700}.logo div.logo-center{text-align:center}.logo div.logo-center img{margin-left:auto;margin-right:auto}.preheader td{text-align:right;width:280px}.preheader td{letter-spacing:.01em;line-height:17px;font-weight:400}.preheader a{letter-spacing:.03em;font-weight:700}.preheader td{font-size:11px}@media only screen and (max-width: 620px){[class=wrapper] .header,[class=wrapper] .preheader td,[class=wrapper] .logo,[class=wrapper] .sidebar{width:320px!important}[class=wrapper] .header .logo{padding-left:10px!important;padding-right:10px!important}[class=wrapper] .header .logo img{max-width:280px!important;height:auto!important}[class=wrapper] .header .preheader td{padding-top:3px!important;padding-bottom:22px!important}[class=wrapper] .header .title{display:none!important}[class=wrapper] .header .webversion{text-align:center!important}[class=wrapper] h1{font-size:40px!important;line-height:48px!important}[class=wrapper] h2{font-size:24px!important;line-height:32px!important}[class=wrapper] h3{font-size:18px!important;line-height:24px!important}[class=wrapper] .column p,[class=wrapper] .column ol,[class=wrapper] .column ul{font-size:15px!important;line-height:24px!important}[class=wrapper] ol,[class=wrapper] ul{margin-left:20px!important}[class=wrapper] li{padding-left:2px!important}[class=wrapper] blockquote{border-left-width:4px!important;padding-left:18px!important}[class=wrapper] table.border{width:320px!important}[class=wrapper] .second .column-top,[class=wrapper] .third .column-top{display:none}}@media only screen and (max-width: 320px){td.border{display:none}}";
body = body + @"        </style>";
body = body + @"          <!--[if !mso]><!--><style type='text/css'>";
body = body + @"        @import url(https://fonts.googleapis.com/css?family=Raleway:400,300,600);";
body = body + @"        </style><link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600' rel='stylesheet' type='text/css'><!--<![endif]--><style type='text/css'>";
body = body + @"        .wrapper h1{}.wrapper h1{font-family:Raleway,Tahoma,sans-serif}.mso .wrapper h1{font-family:Tahoma,sans-serif !important}.wrapper h2{}.wrapper h2{font-family:Raleway,Tahoma,sans-serif}.mso .wrapper h2{font-family:Tahoma,sans-serif !important}.wrapper h3{}.wrapper h3{font-family:Raleway,Tahoma,sans-serif}.mso .wrapper h3{font-family:Tahoma,sans-serif !important}.wrapper p,.wrapper ol,.wrapper ul,.wrapper .image{}.wrapper p,.wrapper ol,.wrapper ul,.wrapper .image{font-family:sans-serif}.wrapper .btn a{}.wrapper .btn a{font-family:sans-serif}.logo div{}.logo div{font-family:'PT Sans','Trebuchet MS',sans-serif}.mso .logo div{font-family:'Trebuchet MS',sans-serif !important}.title,.webversion,.fblike,.tweet,.linkedinshare,.forwardtoafriend,.link,.address,.permission,.campaign{}.title,.webversion,.fblike,.tweet,.linkedinshare,.forwardtoafriend,.link,.address,.permission,.campaign{font-family:Raleway,Tahoma,sans-serif}.mso .title,.mso .webversion,.mso .fblike,.mso .tweet,.mso .linkedinshare,.mso .forwardtoafriend,.mso .link,.mso .address,.mso .permission,.mso .campaign{font-family:Tahoma,sans-serif !important}body,.wrapper,.emb-editor-canvas{background-color:#ecf3f5}.border{background-color:#d4dbdd}.wrapper h1{color:#557187}.wrapper h2{color:#557187}.wrapper h3{color:#557187}.wrapper p,.wrapper ol,.wrapper ul{color:#757575}.wrapper .image{color:#757575}.wrapper a{color:#454545}.wrapper a:hover{color:#2b2b2b !important}.wrapper blockquote{border-left:4px solid #ecf3f5}.wrapper .three-col blockquote,.wrapper .narrower blockquote{border-left:2px solid #ecf3f5}.wrapper .btn a{background-color:#557187;color:#fff}.wrapper .btn a:hover{color:#fff !important}.logo div{color:#c3ced9}.logo div a{color:#c3ced9}.logo div a:hover{color:#c3ced9 !important}.title,.webversion,.footer .inner td{color:#b9b9b9}.wrapper .title a,.wrapper .webversion a,.wrapper .footer a{color:#b9b9b9}.wrapper .title a:hover,.wrapper .webversion a:hover,.wrapper .footer a:hover{color:#939393 !important}.wrapper .footer .fblike,.wrapper .footer .tweet,.wrapper .footer .linkedinshare,.wrapper .footer .forwardtoafriend{background-color:#767a7b}";
body = body + @"        </style><!--[if mso]>";
body = body + @"        <style type='text/css'>";
body = body + @"        @import url(https://fonts.googleapis.com/css?family=Raleway:400,300,600);";
body = body + @"        </style>";
body = body + @"        <![endif]--><meta name='robots' content='noindex,nofollow' />";
body = body + @"        <meta property='og:title' content='Desbloqueo - Portal Training' />";
body = body + @"        </head>";
body = body + @"        <!--[if mso]>";
body = body + @"          <body class='mso'>";
body = body + @"        <![endif]-->";
body = body + @"        <!--[if !mso]><!-->";
body = body + @"          <body class='full-padding' style='margin: 0;padding: 0;min-width: 100%;background-color: #ecf3f5;'>";
body = body + @"        <!--<![endif]-->";
body = body + @"            <center class='wrapper' style='display: table;table-layout: fixed;width: 100%;min-width: 620px;-webkit-text-size-adjust: 100%;-ms-text-size-adjust: 100%;background-color: #E8E8E8;'>";
body = body + @"              <table class='header centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;'>";
body = body + @"                <tbody>";
body = body + @"                  <tr>";
body = body + @"                    <td style='padding: 0;vertical-align: top;'>";
body = body + @"                      <table class='preheader' style='border-collapse: collapse;border-spacing: 0;' align='right'>";
body = body + @"                        <tbody>";
body = body + @"                          <tr>";
body = body + @"                            <td class='emb-logo-padding-box' style='padding: 0;vertical-align: top;padding-bottom: 24px;padding-top: 45px;text-align: left;width: 600px;letter-spacing: 0.01em;line-height: 17px;font-weight: 600;font-size: 25px;'>";
body = body + @"                              <div class='spacer' style='font-size: 1px;line-height: 2px;width: 100%;'>&nbsp;</div>";
body = body + @"                              <div class='webversion' style='font-family: Raleway,Tahoma,sans-serif;color: #31708F;'>Desbloqueo - Portal Training</div>";
body = body + @"                            </td>";
body = body + @"                          </tr>";
body = body + @"                        </tbody>";
body = body + @"                      </table>";
body = body + @"                    </td>";
body = body + @"                  </tr>";
body = body + @"                </tbody>";
body = body + @"              </table>";
body = body + @"              ";
body = body + @"              <table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #d4dbdd;Margin-left: auto;Margin-right: auto;' width='602'>";
body = body + @"                <tbody>";
body = body + @"                  <tr>";
body = body + @"                    <td style='padding: 0;vertical-align: top;'>&#8203;</td>";
body = body + @"                  </tr>";
body = body + @"                </tbody>";
body = body + @"              </table>";
body = body + @"                ";
body = body + @"           <table class='centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;'>"; 
 body = body + @"                    <tbody>"; 
 body = body + @"                      <tr>"; 
 body = body + @"                        <td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #d4dbdd;width: 1px;'>&#8203;</td>"; 
 body = body + @"                        <td style='padding: 0;vertical-align: top;'>"; 
 body = body + @"                          <table class='two-col' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 600px;background-color: #ffffff;table-layout: fixed;' emb-background-style>"; 
 body = body + @"                            <tbody>"; 
 body = body + @"                              <tr>"; 
 body = body + @"                                "; 
 body = body + @"                                <td class='column first' style='padding: 0;vertical-align: top;text-align: left;width: 300px;'>"; 
 body = body + @"                                  <div>"; 
 body = body + @"                                    <div class='column-top' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;'>&nbsp;</div>"; 
 body = body + @"                                  </div>"; 
 body = body + @"                                  <table class='contents' style='border-collapse: collapse;border-spacing: 0;table-layout: fixed;width: 100%;'>"; 
 body = body + @"                                    <tbody>"; 
 body = body + @"                                      <tr>"; 
 body = body + @"                                        <td class='padded' style='padding: 0;vertical-align: top;padding-left: 32px;padding-right: 32px;word-break: break-word;word-wrap: break-word;'>"; 
 body = body + @"                                          <h3 style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0;font-size: 18px;line-height: 24px;font-family: Raleway,Tahoma,sans-serif;color: #557187;'>"+miMail.Nombre + " " + miMail.Apellido+"</h3>"; 
 body = body + @"                                          <p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 12px;font-size: 16px;line-height: 24px;font-family: sans-serif;color: #757575;'>" + miMail.Agencia + " </p>";
 body = body + @"                                          <p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 12px;font-size: 16px;line-height: 24px;font-family: sans-serif;color: #757575;'>Usuario: " + miMail.User + "</p>";
 body = body + @"                                          <p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 0px;font-size: 14px;line-height: 24px;font-family: sans-serif;color: #669A23;'><span style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 12px;font-size: 16px;line-height: 24px;font-family: sans-serif;color: #757575;'>Password: </span>" + miMail.Password + "</p>"; 
 body = body + @"                                          <p style='font-style: normal;font-weight: 400;Margin-bottom: 0;Margin-top: 12px;font-size: 16px;line-height: 24px;font-family: sans-serif;color: #757575;'>Hace click <a href='https://sidepro.com.ar/portalMovistar/'> aqui</a> para acceder al sitio</p>"; 
 body = body + @"                                        </td>"; 
 body = body + @"                                      </tr>"; 
 body = body + @"                                    </tbody>"; 
 body = body + @"                                  </table>"; 
 body = body + @"                                  <div class='column-bottom' style='font-size: 32px;line-height: 32px;transition-timing-function: cubic-bezier(0, 0, 0.2, 1);transition-duration: 150ms;transition-property: all;'>&nbsp;</div>"; 
 body = body + @"                                </td>"; 
 body = body + @"                              </tr>"; 
 body = body + @"                            </tbody>"; 
 body = body + @"                          </table>"; 
 body = body + @"                        </td>"; 
 body = body + @"                        <td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #d4dbdd;width: 1px;'>&#8203;</td>"; 
 body = body + @"                      </tr>"; 
 body = body + @"                    </tbody>"; 
 body = body + @"                  </table>"; 
 body = body + @"                "; 
 body = body + @"                  <table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #d4dbdd;Margin-left: auto;Margin-right: auto;' width='602'>"; 
 body = body + @"                    <tbody><tr class='border' style='font-size: 1px;line-height: 1px;background-color: #e3e3e3;height: 1px;'>"; 
 body = body + @"                      <td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #d4dbdd;width: 1px;'>&#8203;</td>"; 
 body = body + @"                      <td style='padding: 0;vertical-align: top;line-height: 1px;'>&#8203;</td>"; 
 body = body + @"                      <td class='border' style='padding: 0;vertical-align: top;font-size: 1px;line-height: 1px;background-color: #d4dbdd;width: 1px;'>&#8203;</td>"; 
 body = body + @"                    </tr>"; 
 body = body + @"                  </tbody></table>"; 
 body = body + @"                "; 
 body = body + @""; 
 body = body + @"                  <table class='border' style='border-collapse: collapse;border-spacing: 0;font-size: 1px;line-height: 1px;background-color: #d4dbdd;Margin-left: auto;Margin-right: auto;' width='602'>"; 
 body = body + @"                    <tbody><tr><td style='padding: 0;vertical-align: top;'>&nbsp;</td></tr>"; 
 body = body + @"                  </tbody></table>"; 
 body = body + @"                "; 
 body = body + @"              <table class='footer centered' style='border-collapse: collapse;border-spacing: 0;Margin-left: auto;Margin-right: auto;width: 100%;'>"; 
 body = body + @"                <tbody><tr>"; 
 body = body + @"                  <td style='padding: 0;vertical-align: top;'>&nbsp;</td>"; 
 body = body + @"                  <td class='inner' style='padding: 15px 0 0 0;vertical-align: top;width: 600px;'>"; 
 body = body + @"        "; 
 body = body + @"                    <table class='left' style='border-collapse: collapse;border-spacing: 0;' align='left'>"; 
 body = body + @"                      <tbody><tr>"; 
 body = body + @"                        <td style='padding: 0;vertical-align: top;color: #b9b9b9;font-size: 12px;line-height: 22px;text-align: left;width: 400px;'>"; 
 body = body + @"                          "; 
 body = body + @"                          <div class='campaign' style='font-family: Raleway,Tahoma,sans-serif;Margin-bottom: 18px;'>"; 
 body = body + @"                            <span>"; 
 body = body + @"                              Desarrollado por <a href='http://www.amedia.com.ar/'>Grupo Amedia S.R.L.</a>"; 
 body = body + @"                            </span>"; 
 body = body + @"                            "; 
 body = body + @"                          </div>"; 
 body = body + @"                        </td>"; 
 body = body + @"                      </tr>"; 
 body = body + @"                    </tbody></table>"; 
 body = body + @"                  </td>"; 
 body = body + @"                  <td style='padding: 0;vertical-align: top;'>&nbsp;</td>"; 
 body = body + @"                </tr>"; 
 body = body + @"              </tbody></table>"; 
 body = body + @"            </center>"; 
 body = body + @"          "; 
 body = body + @"        </body></html>";
            return body;
        }
        public int obtenerUsuario()
        {
            HttpCookie myCookie = Request.Cookies["UserSettings"];
            int id_usuario = HelpersManagementService.getCookie(myCookie).ID_Usuario;
                /*
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
                 */
            return id_usuario;
        }

    }
}
