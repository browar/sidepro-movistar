﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sidepro.Application;
using Sidepro.ViewModels;
using Sidepro.Entities;
using System.Web.Script.Serialization;
using Sidepro.Application.Helpers;

namespace Sidepro.Controllers
{
    public class HomeController : Controller
    {
        CursosViewModelController cursosVieweModelController = new CursosViewModelController();
        public ActionResult home()
        {
            int usuario = obtenerUsuario();
            var miViewModel = new HomeViewModelController().getHomeViewModel(usuario);
            return View(miViewModel);
        }
        public ActionResult profile()
        {
            ViewBag.Message = "Página inicial";

            return View(new indexViewModel());
        }
        public ActionResult HistorialCertificacion()
        {
            HomeManagementService miManagementService = new HomeManagementService();
            int id_usuario = obtenerUsuario();
            var misCertificaciones = miManagementService.getCertificaciones(id_usuario);
            var json = new JavaScriptSerializer().Serialize(misCertificaciones);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Index3()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }
        public ActionResult index()
        {


            if (Request.Cookies["UserSettings"] != null)
            {
                HttpCookie myCookie = new HttpCookie("UserSettings");
                myCookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(myCookie);
            }

            HttpContext.Session["IDUsuario"] = null;
            HttpContext.Session["Menu"] = null;
            HttpContext.Session["Historial"] = null;


            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
            return View();

        }
        public ActionResult Login(string user, string pass)
        {

            HomeManagementService miManagementService = new HomeManagementService();

            Usuario miUser = miManagementService.getUser(user, pass);
            if (miUser.Mensaje == "") { 

            //HttpCookie myCookie1 = new HttpCookie("UserSettings");
            //myCookie1["Codigo"] = miUser.Id_usuario.ToString();//"1";
            //myCookie1["Nombre"] = miUser.Nombre;// "Eddard";
            //myCookie1["Apellido"] = miUser.Apellido;// "Stark Cook";
            //myCookie1["Tipo"] = miUser.Cod_tipo_usuario.ToString() ;// "Stark Cook";
            //myCookie1["Rol"] = miUser.Cod_tipo_usuario.ToString();// "Stark Cook";
            //myCookie1["Agencia"] = miUser.Id_agencia.ToString();// "Stark Cook";
            //myCookie1.Expires = DateTime.Now.AddDays(1d);

            //Response.Cookies.Add(myCookie1);
            //HttpContext.Session["IDUsuario"] = miUser.Id_usuario;
            //HttpContext.Session["Menu"] = null;
            //HttpContext.Session["Historial"] = null;
            //HttpContext.Session["Tipo"] = miUser.Cod_tipo_usuario;
            //HttpContext.Session["Agencia"] = miUser.Id_agencia;



            
            Seguridad miSeg = new Seguridad();

            miSeg = HelpersManagementService.getEncriptada(miUser.Nombre);

            string EncriptedName = Convert.ToBase64String(miSeg.Encriptado);
            string key = Convert.ToBase64String(miSeg.Key);
            string iv = Convert.ToBase64String(miSeg.IV);

            HttpCookie myCookie = new HttpCookie("UserSettings");
            myCookie["k"] = key;
            myCookie["i"] = iv;

            myCookie["NM"] = EncriptedName;
            HttpContext.Session["NM"] = EncriptedName;
            
            string encrypt = Convert.ToBase64String(HelpersManagementService.getEncriptada(((miUser.Apellido == "") ? " ": miUser.Apellido), miSeg));
            //string encrypt = Convert.ToBase64String(HelpersManagementService.getEncriptada(miUser.Apellido, miSeg));            
            myCookie["AP"] = encrypt;
            HttpContext.Session["AP"] = encrypt;
            encrypt = Convert.ToBase64String(HelpersManagementService.getEncriptada(miUser.Cod_tipo_usuario.ToString(), miSeg));
            myCookie["TU"] = encrypt;
            HttpContext.Session["TU"] = encrypt;
            encrypt = Convert.ToBase64String(HelpersManagementService.getEncriptada(miUser.Id_usuario.ToString(), miSeg));
            myCookie["ID"] = encrypt;
            HttpContext.Session["ID"] = encrypt;
            encrypt = Convert.ToBase64String(HelpersManagementService.getEncriptada("-1", miSeg));
            myCookie["SC"] = encrypt;
            HttpContext.Session["SC"] = encrypt;
            encrypt = Convert.ToBase64String(HelpersManagementService.getEncriptada(miUser.Id_agencia.ToString(), miSeg));
            myCookie["AG"] = encrypt;
            HttpContext.Session["AG"] = encrypt;

            myCookie.Expires = DateTime.Now.AddDays(7d);
            Response.Cookies.Add(myCookie);
            }
            return Json(new { login = (miUser.Mensaje == ""), tipo_usuario = miUser.Cod_tipo_usuario, mensaje = miUser.Mensaje }, JsonRequestBehavior.AllowGet);

        }
        public int obtenerUsuario()
        {
            HttpCookie myCookie = Request.Cookies["UserSettings"];
            int id_usuario = HelpersManagementService.getCookie(myCookie).ID_Usuario;
            /*
            int id_usuario = -1;
            if (HttpContext.Session["IDUsuario"] != null)
            {
                id_usuario = (int)HttpContext.Session["IDUsuario"];
            }
            else
            {
                id_usuario = new indexViewModel().id_usuario;
            }
             * */
            return id_usuario;
        }

    }


}
