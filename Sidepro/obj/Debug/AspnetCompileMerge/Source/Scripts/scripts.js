var viewportWidth = null;
var viewportHeight = null;
var contentHeight = null;

$(window).on("resize", function(){
	viewportWidth = $(window).width();
	viewportHeight = $(window).height();
	contentHeight = $('.js-site-content').height();
}).resize();

$(document).ready(function() {
	window.addEventListener('load', function() {
	  var status = document.getElementById("status");

	  function updateOnlineStatus(event) {
		//var condition = navigator.onLine ? "online" : "offline";
			debugger
		if (!navigator.onLine){
			alert('Se ha cortado la conexión con internet!!');
			 window.location.assign(window.location.origin + "/PortalMovistar/");
		}
	  }

	  window.addEventListener('online',  updateOnlineStatus);
	  window.addEventListener('offline', updateOnlineStatus);
	});
	$(".js-open-menu").click(function(){
		if ($('body').hasClass('ie')) {
			if ($(".header-menu").hasClass("is-opened")) {
				$(".header-menu").removeClass("is-opened").hide();
			} else {
				$(".header-menu").addClass("is-opened").show();
			}
		} else {
			if (!$(".header-menu").hasClass("is-animating")) {
				if ($(".header-menu").hasClass("is-opened")) {
					$(".header-menu").addClass("is-animating").removeClass("is-opened");
					setTimeout(function(){
						$(".header-menu").hide().removeClass("is-animating");
					}, 510);
				} else {
					$(".header-menu").addClass("is-animating");
					$(".header-menu").show(0, function(){
						$(".header-menu").addClass("is-opened");
					});
					setTimeout(function(){
						$(".header-menu").removeClass("is-animating");
					}, 510);
				}
			}
		}
	});

	// TODO:
	// Cerrar el menu al hacer click fuera de el.
	// stopPropagation() bugea la apertura del lightbox
	// del botón "mi perfil".

	$(window).on("resize", function(){
		$('.js-sidebar-container').css('maxHeight', viewportHeight - 72);
		$('.js-site-content-wrapper').css('height', viewportHeight - 71);
		
		// Sidebar responsive: se esconde en pantallas pequeñas

		if (!$('.js-sidebar').hasClass('toggle-override')) {
			if ( viewportWidth <= 1200 ) {
				$('body').addClass('sidebar-is-closed');
				$('.js-site-content-wrapper')
					.width(viewportWidth)
					.css('left', 'auto');
			} else {
				$('body').removeClass('sidebar-is-closed');
				$('.js-site-content-wrapper')
					.width(viewportWidth - 280)
					.css('left', 280);
			}
		}
	}).resize();


	// Sidebar: evitando que se vea antes de esconderse en mobile

	if (viewportWidth < 1200) {
		setTimeout(function(){
			$('.js-sidebar').css('visibility', 'visible');
		}, 500);
	} else {
		$('.js-sidebar').css('visibility', 'visible');
	}


	// Sidebar: botón toggle

	$('.js-sidebar-toggle').click(function(){
		$('.js-sidebar').addClass('toggle-override');

		if ($('body').hasClass('sidebar-is-closed')) {
			$('body').removeClass('sidebar-is-closed');

			$(window).on("resize", function(){
				$('.js-site-content-wrapper')
					.width(viewportWidth - 280)
					.css('left', 280);
			}).resize();
		} else {
			$('body').addClass('sidebar-is-closed');
			$(window).on("resize", function(){
				$('.js-site-content-wrapper')
					.width(viewportWidth)
					.css('left', 'auto');
			}).resize();	
		}
	});

	$('.js-course-hover').hover(function(){
		$(this).children().children('.js-course-btn-container').toggleClass('is-visible');
	});

	$('.js-slider-featured').slick({
		slidesToShow: 1,
		dots: true,
		arrows: false,
		cssEase: 'cubic-bezier(0.645, 0.045, 0.355, 1.000)',
		autoplay: true,
		speed: 1000
	});

	$('html').on('change', '.js-pic-update-upload-input', function(){
		$('.js-pic-update-upload, .js-pic-update-instructions').fadeOut(350, function(){
			$('.js-pic-update-loading').fadeIn(350);
		});
	});

	$('.js-module-grid').masonry({
		itemSelector: '.module__course'
	});

	$('.js-course-description-ellipsis').dotdotdot({
		watch: 'window'
	});

	if (!$('body').hasClass('ie')) {
		$('.js-sidebar-container, .js-history-container').perfectScrollbar();
		$('.js-history-container').css('paddingRight', 15);
	}

	$('html').on('click', '.answers li', function(){
        $(this).children('input').prop('checked', true);
    });
    
    $('.js-tooltip-right').tooltipster({
        animation: 'grow',
        delay: 0,
        trigger: 'hover',
        position: 'right'
    });
});





// Charts de la sección "Tu progreso".
/*
var progressTotalData = [
	{
		value: 0,
		color:"#7AB901"
	},
	{
		value: 100,
		color:"#EEEEEE"
	}
];
var progressModInicialValue = 0+'%';
var progressEquiposValue = 0+'%';
var progressOfertaValue = 0+'%';
var progressSaberMasValue = 0+'%';
var progressCertifValue = 0+'%';

window.onload = function(){
	var progressTotalChart = $("#js-progress-total").get(0).getContext("2d");
	window.myDoughnut = new Chart(progressTotalChart)
		.Doughnut(progressTotalData, {
			showTooltips: false,
			segmentShowStroke: false,
			animationSteps: 75,
			animationEasing: "easeOutQuart",
			percentageInnerCutout: 88
		});
	$("#js-total-percent").prepend(progressTotalData[0].value + '%');

	$('#js-mod-inicial-value').animate({width: progressModInicialValue}, 750);
	$("#js-mod-inicial-percent").prepend(progressModInicialValue);

	$('#js-equipos-value').animate({width: progressEquiposValue}, 750);
	$("#js-equipos-percent").prepend(progressEquiposValue);

	$('#js-oferta-value').animate({width: progressOfertaValue}, 750);
	$("#js-oferta-percent").prepend(progressOfertaValue);

	$('#js-saber-mas-value').animate({width: progressSaberMasValue}, 750);
	$("#js-saber-mas-percent").prepend(progressSaberMasValue);

	$('#js-certif-value').animate({width: progressCertifValue}, 750);
	$("#js-certif-percent").prepend(progressCertifValue);
};

*/



// Sticky footer
$(window).bind("load", function() {

	var footer = $(".js-footer");
	var container = $(".js-site-content-container");
	var wrapper = $(".js-site-content-wrapper");

	positionFooter();

	function positionFooter() {
		footerHeight = footer.height();

		if ( (container.height()+footerHeight*2) > wrapper.height() ) {
			footer.css({
				position: "relative"
			});
		} else {
			footer.css({
				position: "absolute"
			});
		}
	}

	$(window).scroll(positionFooter)
			.resize(positionFooter);
});