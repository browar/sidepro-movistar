﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Seguridad
    {
        private byte[] _Encriptado;

        public byte[] Encriptado
        {
            get { return _Encriptado; }
            set { _Encriptado = value; }
        }
        private byte[] _Key;

        public byte[] Key
        {
            get { return _Key; }
            set { _Key = value; }
        }
        private byte[] _IV;

        public byte[] IV
        {
            get { return _IV; }
            set { _IV = value; }
        }
    }
}