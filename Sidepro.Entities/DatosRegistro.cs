﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class DatosRegistro
    {
        private int _saveCredentials;

        public int SaveCredentials
        {
            get { return _saveCredentials; }
            set { _saveCredentials = value; }
        }
        private string _Hash;

        public string Hash
        {
            get { return _Hash; }
            set { _Hash = value; }
        }
        private string _Mensaje;

        public string Mensaje
        {
            get { return _Mensaje; }
            set { _Mensaje = value; }
        }
        private int _Cod_Error;

        public int Cod_Error
        {
            get { return _Cod_Error; }
            set { _Cod_Error = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private string _Apellido;

        public string Apellido
        {
            get { return _Apellido; }
            set { _Apellido = value; }
        }
        private int _Cod_Tipo_Usuario;

        public int Cod_Tipo_Usuario
        {
            get { return _Cod_Tipo_Usuario; }
            set { _Cod_Tipo_Usuario = value; }
        }
        private string _EMail;

        public string EMail
        {
            get { return _EMail; }
            set { _EMail = value; }
        }
        private int _ID_Usuario;

        public int ID_Usuario
        {
            get { return _ID_Usuario; }
            set { _ID_Usuario = value; }
        }
        private int _Cod_Rol;

        public int Cod_Rol
        {
            get { return _Cod_Rol; }
            set { _Cod_Rol = value; }
        }
        private int _ID_Agencia;

        public int ID_Agencia
        {
            get { return _ID_Agencia; }
            set { _ID_Agencia = value; }
        }
 
    }
}