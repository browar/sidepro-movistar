﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class TipoDestacado
    {
        private int _Cod_tipo_destacado;

        public int Cod_tipo_destacado
        {
            get { return _Cod_tipo_destacado; }
            set { _Cod_tipo_destacado = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
    }
}