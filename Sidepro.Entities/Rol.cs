﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Rol
    {
        private int _cod_canal;
        private string _Canal;

        public string Canal
        {
            get { return _Canal; }
            set { _Canal = value; }
        }
        public int cod_canal
        {
            get { return _cod_canal; }
            set { _cod_canal = value; }
        }
        private int _ID_Rol;

        public int ID_Rol
        {
            get { return _ID_Rol; }
            set { _ID_Rol = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
    }
}