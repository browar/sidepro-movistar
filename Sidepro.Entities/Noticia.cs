﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Noticia
    {
        private string _FileName;

        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        private string _Path;

        public string Path
        {
            get { return _Path; }
            set { _Path = value; }
        }
    }
}