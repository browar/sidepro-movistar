﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Pregunta
    {

        private int _Id_modulo;
        private int _Id_pregunta;
        private decimal _Cant_puntos;
        private string _Nombre;
        private string _Correcto;
        private string _Incorrecto;
        private int _Sn_activo;
        private int? _Categoria;
        private string _NombreCategoria;

        private int _ID_ModuloCert;

        public int ID_ModuloCert
        {
            get { return _ID_ModuloCert; }
            set { _ID_ModuloCert = value; }
        }
        private string _ModuloCert;

        public string ModuloCert
        {
            get { return _ModuloCert; }
            set { _ModuloCert = value; }
        }
        private int _ID_CursoCert;

        public int ID_CursoCert
        {
            get { return _ID_CursoCert; }
            set { _ID_CursoCert = value; }
        }
        private string _CursoCert;

        public string CursoCert
        {
            get { return _CursoCert; }
            set { _CursoCert = value; }
        }
        private int _ID_LeccionCert;

        public int ID_LeccionCert
        {
            get { return _ID_LeccionCert; }
            set { _ID_LeccionCert = value; }
        }
        private string _LeccionCert;

        public string LeccionCert
        {
            get { return _LeccionCert; }
            set { _LeccionCert = value; }
        }

        public string NombreCategoria
        {
            get { return _NombreCategoria; }
            set { _NombreCategoria = value; }
        }

        public int? Categoria
        {
            get { return _Categoria; }
            set { _Categoria = value; }
        }
        public int Sn_activo
        {
            get { return _Sn_activo; }
            set { _Sn_activo = value; }
        }

        public string Incorrecto
        {
            get { return _Incorrecto; }
            set { _Incorrecto = value; }
        }

        public string Correcto
        {
            get { return _Correcto; }
            set { _Correcto = value; }
        }
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        public decimal Cant_puntos
        {
            get { return _Cant_puntos; }
            set { _Cant_puntos = value; }
        }
        public int Id_pregunta
        {
            get { return _Id_pregunta; }
            set { _Id_pregunta = value; }
        }

        public int Id_modulo
        {
            get { return _Id_modulo; }
            set { _Id_modulo = value; }
        }
        private int _Id_curso;

        public int Id_curso
        {
            get { return _Id_curso; }
            set { _Id_curso = value; }
        }
        private int _Id_leccion;

        public int Id_leccion
        {
            get { return _Id_leccion; }
            set { _Id_leccion = value; }
        }

    }
}