﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Respuesta
    {

        private int _Id_modulo;
        private int _Id_pregunta;
        private int _Sn_correcto;
        private int _Id_respuesta;

        public int Id_respuesta
        {
            get { return _Id_respuesta; }
            set { _Id_respuesta = value; }
        }
        public int Sn_correcto
        {
            get { return _Sn_correcto; }
            set { _Sn_correcto = value; }
        }
        private string _Nombre;
        private int _Sn_activo;

        public int Sn_activo
        {
            get { return _Sn_activo; }
            set { _Sn_activo = value; }
        }


        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        public int Id_pregunta
        {
            get { return _Id_pregunta; }
            set { _Id_pregunta = value; }
        }

        public int Id_modulo
        {
            get { return _Id_modulo; }
            set { _Id_modulo = value; }
        }
        private int _Id_curso;

        public int Id_curso
        {
            get { return _Id_curso; }
            set { _Id_curso = value; }
        }
        private int _Id_leccion;

        public int Id_leccion
        {
            get { return _Id_leccion; }
            set { _Id_leccion = value; }
        }

    }
}