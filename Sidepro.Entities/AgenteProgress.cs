﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class AgenteProgress
    {

        private int _Agencia;

        public int Agencia
        {
            get { return _Agencia; }
            set { _Agencia = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private decimal _Progreso;

        public decimal Progreso
        {
            get { return _Progreso; }
            set { _Progreso = value; }
        }
        private decimal _Nota;

        public decimal Nota
        {
            get { return _Nota; }
            set { _Nota = value; }
        }

    }
}