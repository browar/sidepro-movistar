﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class ModuloCursos
    {
        private List<Curso> _Cursos;
        private int _Id_modulo;
        private string _Nombre;
        private string _URL;
        private string _Style;
        private string _Controller;
        private string _View;
        private string _HTMLTag;
        private int _Sn_activo;
        private int _PuedeRealizar;

        public int PuedeRealizar
        {
            get { return _PuedeRealizar; }
            set { _PuedeRealizar = value; }
        }
        public int Sn_activo
        {
            get { return _Sn_activo; }
            set { _Sn_activo = value; }
        }
        public string HTMLTag
        {
            get { return _HTMLTag; }
            set { _HTMLTag = value; }
        }
        public string View
        {
            get { return _View; }
            set { _View = value; }
        }
        public string Controller
        {
            get { return _Controller; }
            set { _Controller = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string URL
        {
            get { return _URL; }
            set { _URL = value; }
        }
        public List<Curso> Cursos
        {
            get { return _Cursos; }
            set { _Cursos = value; }
        }
        public int Id_modulo
        {
            get { return _Id_modulo; }
            set { _Id_modulo = value; }
        }
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

    }
}