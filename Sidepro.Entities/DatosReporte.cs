﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class DatosReporte
    {
        private int _CantidadAgencias;
        private int _CantidadOtros;

        public int CantidadOtros
        {
            get { return _CantidadOtros; }
            set { _CantidadOtros = value; }
        }
        public int CantidadAgencias
        {
            get { return _CantidadAgencias; }
            set { _CantidadAgencias = value; }
        }
        private int _CantidadCecs;

        public int CantidadCecs
        {
            get { return _CantidadCecs; }
            set { _CantidadCecs = value; }
        }
        private int _CantidadUsuarios;

        public int CantidadUsuarios
        {
            get { return _CantidadUsuarios; }
            set { _CantidadUsuarios = value; }
        }
        private int _CantidadActivos;

        public int CantidadActivos
        {
            get { return _CantidadActivos; }
            set { _CantidadActivos = value; }
        }
        private int _CantidadInactivos;

        public int CantidadInactivos
        {
            get { return _CantidadInactivos; }
            set { _CantidadInactivos = value; }
        }
    }
}