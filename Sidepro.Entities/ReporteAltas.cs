﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class ReporteAltas
    {
        private string _MesTexto;

        public string MesTexto
        {
            get { return _MesTexto; }
            set { _MesTexto = value; }
        }
        private int _ID_Agencia;

        public int ID_Agencia
        {
            get { return _ID_Agencia; }
            set { _ID_Agencia = value; }
        }
        private string _Agencia;

        public string Agencia
        {
            get { return _Agencia; }
            set { _Agencia = value; }
        }
        private int _Ano;

        public int Ano
        {
            get { return _Ano; }
            set { _Ano = value; }
        }
        private int _Mes;

        public int Mes
        {
            get { return _Mes; }
            set { _Mes = value; }
        }
        private int _Cantidad;

        public int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }
    }
}