﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class UserProgress
    {
        private int _Activo;

        public int Activo
        {
            get { return _Activo; }
            set { _Activo = value; }
        }
        private int _Cod_usuario;

        public int Cod_usuario
        {
            get { return _Cod_usuario; }
            set { _Cod_usuario = value; }
        }
        private string _Nombre;
        private string _Registro;

        public string Registro
        {
            get { return _Registro; }
            set { _Registro = value; }
        }
        private string _Apellido;

        public string Apellido
        {
            get { return _Apellido; }
            set { _Apellido = value; }
        }
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private string _Documento;

        public string Documento
        {
            get { return _Documento; }
            set { _Documento = value; }
        }
        private string _Agencia;

        public string Agencia
        {
            get { return _Agencia; }
            set { _Agencia = value; }
        }
        private string _UltimaActividad;

        public string UltimaActividad
        {
            get { return _UltimaActividad; }
            set { _UltimaActividad = value; }
        }
        private int _Accesos;

        public int Accesos
        {
            get { return _Accesos; }
            set { _Accesos = value; }
        }
        private int _CursosAprobados;

        public int CursosAprobados
        {
            get { return _CursosAprobados; }
            set { _CursosAprobados = value; }
        }
        private decimal _Progreso;

        public decimal Progreso
        {
            get { return _Progreso; }
            set { _Progreso = value; }
        }
        private string _Certificacion;

        public string Certificacion
        {
            get { return _Certificacion; }
            set { _Certificacion = value; }
        }
        private decimal _ModuloInicial;

        public decimal ModuloInicial
        {
            get { return _ModuloInicial; }
            set { _ModuloInicial = value; }
        }
        private decimal _Equipos;

        public decimal Equipos
        {
            get { return _Equipos; }
            set { _Equipos = value; }
        }
        private decimal _Total;

        public decimal Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
        private decimal _OfertaYCampana;

        public decimal OfertaYCampana
        {
            get { return _OfertaYCampana; }
            set { _OfertaYCampana = value; }
        }
        private int _Cantidad;

        public int Cantidad
        {
            get { return _Cantidad; }
            set { _Cantidad = value; }
        }
        private string _User;

        public string User
        {
            get { return _User; }
            set { _User = value; }
        }
        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        private string _Estado;

        public string Estado
        {
            get { return _Estado; }
            set { _Estado = value; }
        }
    }
}