﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Quiz
    {
        public Info info { get; set; }
        public Question[] questions { get; set; }
        public bool sn_inicial { get; set; }
        public bool aprobado { get; set; }
        public string video { get; set; }
        public string imagen { get; set; }
        public string nombre { get; set; }
    }
    public class Info
    {
        public string name { get; set; }
        public string main { get; set; }
        public string results { get; set; }
    }
    public class Question
    {
        public string q { get; set; }
        public Answer[] a { get; set; }
        public string correct { get; set; }
        public string incorrect { get; set; }
        public int id { get; set; }
        public decimal valor { get; set; }
    }
    public class Answer
    {
        public string option { get; set; }
        public bool correct { get; set; }
    }
}