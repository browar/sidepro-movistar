﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class MailUsuario
    {
        private int _cod_usuario;

        public int cod_usuario
        {
            get { return _cod_usuario; }
            set { _cod_usuario = value; }
        }
        private string _Mensaje;

        public string Mensaje
        {
            get { return _Mensaje; }
            set { _Mensaje = value; }
        }
        private string _MailAgencia;

        public string MailAgencia
        {
            get { return _MailAgencia; }
            set { _MailAgencia = value; }
        }
        private string _Mail;

        public string Mail
        {
            get { return _Mail; }
            set { _Mail = value; }
        }
        private string _User;

        public string User
        {
            get { return _User; }
            set { _User = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private string _Apellido;

        public string Apellido
        {
            get { return _Apellido; }
            set { _Apellido = value; }
        }
        private string _Password;

        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private string _Agencia;

        public string Agencia
        {
            get { return _Agencia; }
            set { _Agencia = value; }
        }
        private int _sn_enviado;

        public int sn_enviado
        {
            get { return _sn_enviado; }
            set { _sn_enviado = value; }
        }
        private string _FechaEnviado;

        public string FechaEnviado
        {
            get { return _FechaEnviado; }
            set { _FechaEnviado = value; }
        }
        private string _TipoMail;

        public string TipoMail
        {
            get { return _TipoMail; }
            set { _TipoMail = value; }
        }
    }
}