﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Agencia
    {
        private int _ID_Agencia;

        public int ID_Agencia
        {
            get { return _ID_Agencia; }
            set { _ID_Agencia = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private string _Documento;

        public string Documento
        {
            get { return _Documento; }
            set { _Documento = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private int _Cod_tipo_agencia;

        public int Cod_tipo_agencia
        {
            get { return _Cod_tipo_agencia; }
            set { _Cod_tipo_agencia = value; }
        }
        private string _TipoAgencia;

        public string TipoAgencia
        {
            get { return _TipoAgencia; }
            set { _TipoAgencia = value; }
        }
    }
}