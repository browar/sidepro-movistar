﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class CourseProgress
    {
        private decimal _CantidadUsuarios;
        private int _Mes;
        private string _Url_imagen;
        private string _Periodo;
        private int _UsuariosActivos;
        private int _UsuariosInactivos;

        public int UsuariosInactivos
        {
            get { return _UsuariosInactivos; }
            set { _UsuariosInactivos = value; }
        }
        private int _UsuariosTotales;

        public int UsuariosTotales
        {
            get { return _UsuariosTotales; }
            set { _UsuariosTotales = value; }
        }
        private int _Ano;

        public int Ano
        {
            get { return _Ano; }
            set { _Ano = value; }
        }
        public int UsuariosActivos
        {
            get { return _UsuariosActivos; }
            set { _UsuariosActivos = value; }
        }

        public string Periodo
        {
            get { return _Periodo; }
            set { _Periodo = value; }
        }
        public string Url_imagen
        {
            get { return _Url_imagen; }
            set { _Url_imagen = value; }
        }
        private string _NombreAgencia;

        public string NombreAgencia
        {
            get { return _NombreAgencia; }
            set { _NombreAgencia = value; }
        }

        public int Mes
        {
            get { return _Mes; }
            set { _Mes = value; }
        }
        private string _MesTexto;

        public string MesTexto
        {
            get { return _MesTexto; }
            set { _MesTexto = value; }
        }
        
        public decimal CantidadUsuarios
        {
            get { return _CantidadUsuarios; }
            set { _CantidadUsuarios = value; }
        }
        private decimal _ModuloInicial;

        public decimal ModuloInicial
        {
            get { return _ModuloInicial; }
            set { _ModuloInicial = value; }
        }
        private decimal _Equipos;

        public decimal Equipos
        {
            get { return _Equipos; }
            set { _Equipos = value; }
        }
        private decimal _OfertaYCampana;

        public decimal OfertaYCampana
        {
            get { return _OfertaYCampana; }
            set { _OfertaYCampana = value; }
        }
        private decimal _Certificacion;

        public decimal Certificacion
        {
            get { return _Certificacion; }
            set { _Certificacion = value; }
        }
        private decimal _Total;

        public decimal Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
    }
}