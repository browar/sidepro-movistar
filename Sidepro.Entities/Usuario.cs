﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Usuario
    {
        private int _Id_usuario;
        private string _Nombre;
        private string _Apellido;
        private string _URL_imagen;
        private int _cod_tipo_usuario;
        private string _Mensaje;
        private int _Id_agencia;
        private string _Agencia;
        private string _Rol;

        public string Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }

        public string Agencia
        {
            get { return _Agencia; }
            set { _Agencia = value; }
        }
        

        public int Id_agencia
        {
            get { return _Id_agencia; }
            set { _Id_agencia = value; }
        }
        public string Mensaje
        {
            get { return _Mensaje; }
            set { _Mensaje = value; }
        }
        public int Cod_tipo_usuario
        {
            get { return _cod_tipo_usuario; }
            set { _cod_tipo_usuario = value; }
        }
        public string URL_imagen
        {
            get { return _URL_imagen; }
            set { _URL_imagen = value; }
        }
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        public string Apellido
        {
            get { return _Apellido; }
            set { _Apellido = value; }
        }
        public int Id_usuario
        {
            get { return _Id_usuario; }
            set { _Id_usuario = value; }
        }



        
    }
}