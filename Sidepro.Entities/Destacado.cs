﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Destacado
    {
        private string _Rol;

        public string Rol
        {
            get { return _Rol; }
            set { _Rol = value; }
        }
        private string _Modulo;

        public string Modulo
        {
            get { return _Modulo; }
            set { _Modulo = value; }
        }
        private string _Curso;

        public string Curso
        {
            get { return _Curso; }
            set { _Curso = value; }
        }
        private string _Leccion;

        public string Leccion
        {
            get { return _Leccion; }
            set { _Leccion = value; }
        }
        private int _ID_Destacado;

        public int ID_Destacado
        {
            get { return _ID_Destacado; }
            set { _ID_Destacado = value; }
        }
        private int _ID_Rol;

        public int ID_Rol
        {
            get { return _ID_Rol; }
            set { _ID_Rol = value; }
        }
        private int _ID_Modulo;

        public int ID_Modulo
        {
            get { return _ID_Modulo; }
            set { _ID_Modulo = value; }
        }
        private int? _ID_Curso;

        public int? ID_Curso
        {
            get { return _ID_Curso; }
            set { _ID_Curso = value; }
        }

        private int? _ID_Leccion;

        public int? ID_Leccion
        {
            get { return _ID_Leccion; }
            set { _ID_Leccion = value; }
        }

        private string _TipoDescripcion;

        public string TipoDescripcion
        {
            get { return _TipoDescripcion; }
            set { _TipoDescripcion = value; }
        }

        private int _TipoDestacado;

        public int TipoDestacado
        {
            get { return _TipoDestacado; }
            set { _TipoDestacado = value; }
        }
        private int _Orden;

        public int Orden
        {
            get { return _Orden; }
            set { _Orden = value; }
        }
        private string _Imagen;

        public string Imagen
        {
            get { return _Imagen; }
            set { _Imagen = value; }
        }
        private string _URL;

        public string URL
        {
            get { return _URL; }
            set { _URL = value; }
        }
        private string _Titulo;

        public string Titulo
        {
            get { return _Titulo; }
            set { _Titulo = value; }
        }
        private string _Descripcion;

        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
    }
}