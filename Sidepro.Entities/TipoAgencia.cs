﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class TipoAgencia
    {
        private int _Cod_tipo_agencia;

        public int Cod_tipo_agencia
        {
            get { return _Cod_tipo_agencia; }
            set { _Cod_tipo_agencia = value; }
        }
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
    }
}