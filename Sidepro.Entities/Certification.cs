﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Certification
    {
        private int _Id_certification;

        public int Id_certification
        {
            get { return _Id_certification; }
            set { _Id_certification = value; }
        }
        private string _Nombre;

        private bool _Aprobado;

        public bool Aprobado
        {
            get { return _Aprobado; }
            set { _Aprobado = value; }
        }
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private int _Nota;

        public int Nota
        {
            get { return _Nota; }
            set { _Nota = value; }
        }
    }
}