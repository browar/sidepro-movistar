﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Curso
    {
        private int _Id_curso;
        private string _Nombre;
        private string _Descripcion;
        private int _Sn_Activo;
        private string _URL;
        private string _Nombre_Modulo;
        private string _Style;
        private string _Controller;
        private string _View;
        private string _URLImagen;
        private int _Id_modulo;
        private int _Padre;
        private string _Nombre_Padre;
        private int _Sigue;
        private int _PuedeRealizar;

        public int PuedeRealizar
        {
            get { return _PuedeRealizar; }
            set { _PuedeRealizar = value; }
        }
        public int Sigue
        {
            get { return _Sigue; }
            set { _Sigue = value; }
        }
        public string Nombre_Padre
        {
            get { return _Nombre_Padre; }
            set { _Nombre_Padre = value; }
        }
        public int Padre
        {
            get { return _Padre; }
            set { _Padre = value; }
        }
        public int Id_modulo
        {
            get { return _Id_modulo; }
            set { _Id_modulo = value; }
        }
        public string URLImagen
        {
            get { return _URLImagen; }
            set { _URLImagen = value; }
        }

        public string View
        {
            get { return _View; }
            set { _View = value; }
        }
        public string Controller
        {
            get { return _Controller; }
            set { _Controller = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        public string URL
        {
            get { return _URL; }
            set { _URL = value; }
        }
        public string Nombre_Modulo
        {
            get { return _Nombre_Modulo; }
            set { _Nombre_Modulo = value; }
        }
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        public int Sn_Activo
        {
            get { return _Sn_Activo; }
            set { _Sn_Activo = value; }
        }
        public int Id_curso
        {
            get { return _Id_curso; }
            set { _Id_curso = value; }
        }

    }
}