﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class ReporteNovedades
    {
        private string _Nombre;

        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        private string _Apellido;

        public string Apellido
        {
            get { return _Apellido; }
            set { _Apellido = value; }
        }
        private string _Novedad;

        public string Novedad
        {
            get { return _Novedad; }
            set { _Novedad = value; }
        }
        private string _Descargado;

        public string Descargado
        {
            get { return _Descargado; }
            set { _Descargado = value; }
        }
        private string _Aprobado;

        public string Aprobado
        {
            get { return _Aprobado; }
            set { _Aprobado = value; }
        }
        private int _Reprobados;

        public int Reprobados
        {
            get { return _Reprobados; }
            set { _Reprobados = value; }
        }
    }
}