﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sidepro.Entities
{
    public class Leccion
    {
        private int _Id_leccion;
        private int _Id_Modulo;
        private int _Id_Curso;

        private string _Nombre;
        private string _Descripcion;
        private string _Style;

        private string _URLVideo;
        
        private int _Sn_Activo;
        private int _Sn_visto;

        private string _URL_Imagen;

        private string _NombreCurso;
        private string _ImagenCurso;
        private int? _Orden;

        private int _PuedeRealizar;

        public int PuedeRealizar
        {
            get { return _PuedeRealizar; }
            set { _PuedeRealizar = value; }
        }
        public int? Orden
        {
            get { return _Orden; }
            set { _Orden = value; }
        }
        public string ImagenCurso
        {
            get { return _ImagenCurso; }
            set { _ImagenCurso = value; }
        }
        public string NombreCurso
        {
            get { return _NombreCurso; }
            set { _NombreCurso = value; }
        }
        public string URL_Imagen
        {
            get { return _URL_Imagen; }
            set { _URL_Imagen = value; }
        }
        private decimal _pje_aprobacion;

        public decimal Pje_aprobacion
        {
            get { return _pje_aprobacion; }
            set { _pje_aprobacion = value; }
        }
        private int _aprobo;

        public int Aprobo
        {
            get { return _aprobo; }
            set { _aprobo = value; }
        }
        private DateTime? _fec_realizado;

        public DateTime? Fec_realizado
        {
            get { return _fec_realizado; }
            set { _fec_realizado = value; }
        }
        /*
            private string _Nombre_Curso;
            private string _Controller;
            private string _View;
            private string _URL;

        */

        public int Sn_visto
        {
            get { return _Sn_visto; }
            set { _Sn_visto = value; }
        }
        public string URL_Video
        {
            get { return _URLVideo; }
            set { _URLVideo = value; }
        }
        public string Style
        {
            get { return _Style; }
            set { _Style = value; }
        }
        
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
        public string Descripcion
        {
            get { return _Descripcion; }
            set { _Descripcion = value; }
        }
        public int Sn_Activo
        {
            get { return _Sn_Activo; }
            set { _Sn_Activo = value; }
        }
        public int Id_leccion
        {
            get { return _Id_leccion; }
            set { _Id_leccion = value; }
        }
        public int Id_modulo
        {
            get { return _Id_Modulo; }
            set { _Id_Modulo = value; }
        }


        public int Id_curso
        {
            get { return _Id_Curso; }
            set { _Id_Curso = value; }
        }
        /*
            public string View
            {
                get { return _View; }
                set { _View = value; }
            }
            public string Controller
            {
                get { return _Controller; }
                set { _Controller = value; }
            }
        
            public string URL
            {
                get { return _URL; }
                set { _URL = value; }
            }
            public string Nombre_Modulo
            {
                get { return _Nombre_Modulo; }
                set { _Nombre_Modulo = value; }
            }
        */
    }
}