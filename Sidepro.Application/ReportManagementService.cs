﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Business;
namespace Sidepro.Application
{
    public class ReportManagementService
    {
        ReportBusiness reportBusiness = new ReportBusiness();
        public CourseProgress getProgresoReportes(int id_usuario)
        {
            return reportBusiness.getProgresoReportes(id_usuario);
        }
        public CourseProgress getProgresoAgenciaReportes(int id_usuario, int id_agencia) {
            return reportBusiness.getProgresoReportesAgencia(id_usuario, id_agencia);
        }   
        public List<CourseProgress> getProgresoMensual(int id_usuario)
        {
            return reportBusiness.getProgresoMensual(id_usuario);
        }
        public List<CourseProgress> getCertMensuales()
        {
            return reportBusiness.getCertMensuales();
        }
        
        public List<CourseProgress> getCertificacionMensual(int id_usuario)
        {
            return reportBusiness.getCertificacionMensual(id_usuario);
        }

        
        public List<CourseProgress> getLogeoUsuarios()
        {
            return reportBusiness.getLogeoUsuarios();
        }
        
        public List<CourseProgress> getProgresoMensualAgencia(int id_agencia)
        {
            return reportBusiness.getProgresoMensualAgencia(id_agencia);
        }
        public List<CourseProgress> getProgresoMensualUsuariosAgencia(int id_agencia)
        {
            return reportBusiness.getProgresoMensualUsuariosAgencia(id_agencia);
        }
        public List<AgenteProgress> getProgresoAgente(int id_usuario)
        {
            return reportBusiness.getProgresoAgente(id_usuario);
        }
        public AgenteProgress getProgresoAgencia(int id_usuario)
        {
            return reportBusiness.getProgresoAgencia(id_usuario);
        }
        public AgenteProgress getProgresoCec(int id_usuario)
        {
            return reportBusiness.getProgresoCec(id_usuario);
        }
        public List<AgenteProgress> getProgresoAgente(int desde, int hasta)
        {
            return reportBusiness.getProgresoAgente(desde, hasta);
        }
        public List<UserProgress> getProgresoUsuarios()
        {
            return reportBusiness.getProgresoUsuarios();
        }
        public List<UserProgress> getProgresoUsuarios(int id_agencia)
        {
            return reportBusiness.getProgresoUsuarios(id_agencia);
        }
        public UserProgress getProgresoUsuario(int id_agencia)
        {
            return reportBusiness.getProgresoUsuario(id_agencia);
        }        
        public List<UserProgress> getProgresoUsuarios(string usuario)
        {
            return reportBusiness.getProgresoUsuarios(usuario);
        }
        public List<UserProgress> getProgresoUsuarios(string usuario, int id_agencia)
        {
            return reportBusiness.getProgresoUsuarios(usuario, id_agencia);
        }
        public List<UserProgress> getCertificacionesDeUsuariosDeAgenciaDelMes(int id_agencia, int mes, int ano)
        {
            return reportBusiness.getCertificacionesDeUsuariosDeAgenciaDelMes(id_agencia, mes, ano);
        }
        
        public DatosReporte getDatosReporte( )
        {
            return reportBusiness.getDatosReporte();
        }
        public List<ReporteNovedades> GetReporteNovedades(string usuario)
        {
            return reportBusiness.GetReporteNovedades(usuario);
        }
        public List<UserProgress> getCertificacionesAgencias(int mes, int ano)
        {
            return reportBusiness.getCertificacionesAgencias(mes, ano);
        }
        public List<UserProgress> getCertificacionesUsuariosAgencias(int mes, int ano)
        {
            return reportBusiness.getCertificacionesUsuariosAgencias(mes, ano);
        }
        public List<UserProgress> getCantidadUsuariosAgencias(int mes, int ano)
        {
            return reportBusiness.getCantidadUsuariosAgencias(mes, ano);
        }
        public List<MailUsuario> getEstadoEmails()
        {
            return reportBusiness.getEstadoEmails();
        }
        public List<MailUsuario> getEstadoEmailsFiltro(string usuario)
        {
            return reportBusiness.getEstadoEmailsFiltro(usuario);
        }
        public List<ReporteAltas> GetHistorialAltas(int id_agencia)
        {
            return reportBusiness.GetHistorialAltas(id_agencia);
        }
    }
}