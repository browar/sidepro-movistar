﻿using Sidepro.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace Sidepro.Application.Helpers
{
    public class HelpersManagementService
    {
        private static byte[] Encriptar(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an AesCryptoServiceProvider object
            // with the specified key and IV.
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }
        private static string Desencriptar(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an AesCryptoServiceProvider object
            // with the specified key and IV.
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

        public static Seguridad getEncriptada(string toEncript)
        {
            Seguridad miSeguridad = new Seguridad();

            using (AesCryptoServiceProvider myAes = new AesCryptoServiceProvider())
            {
                // Encrypt the string to an array of bytes.
                miSeguridad.Key = myAes.Key;
                miSeguridad.IV = myAes.IV;
                miSeguridad.Encriptado = Encriptar(toEncript, myAes.Key, myAes.IV);
            }
            return miSeguridad;

        }
        public static byte[] getEncriptada(string toEncript, Seguridad miSeg)
        {
            return Encriptar(toEncript, miSeg.Key, miSeg.IV);

        }
        public static string getDesencriptada(Seguridad segToDesencript)
        {
            return Desencriptar(segToDesencript.Encriptado, segToDesencript.Key, segToDesencript.IV);
        }
        public static DatosRegistro getCookie(HttpCookie myCookie)
        {
            DatosRegistro misDatos = new DatosRegistro();

            //Byte[] EncriptedMail = Convert.FromBase64String(myCookie["Email"]);
            Byte[] EncriptedNombre = Convert.FromBase64String(myCookie["NM"]); //Name
            Byte[] EncriptedApellido = Convert.FromBase64String(myCookie["AP"]); //Apellido
            Byte[] EncriptedTipoUsuario = Convert.FromBase64String(myCookie["TU"]); //TipoUsuario
            
            Byte[] EncriptedAgencia = Convert.FromBase64String(myCookie["AG"]); //Agencia
            Byte[] EncriptedID = Convert.FromBase64String(myCookie["ID"]); //Cod_Usuario
            Byte[] EncriptedSC = Convert.FromBase64String(myCookie["SC"]); //SC

            Byte[] key = Convert.FromBase64String(myCookie["k"]);
            Byte[] iv = Convert.FromBase64String(myCookie["i"]);
            Seguridad miSeg = new Seguridad();
            //miSeg.Encriptado = EncriptedMail;
            miSeg.Key = key;
            miSeg.IV = iv;
            //misDatos.EMail = HelpersManagementService.getDesencriptada(miSeg);
            miSeg.Encriptado = EncriptedNombre;
            misDatos.Nombre = HelpersManagementService.getDesencriptada(miSeg);
            miSeg.Encriptado = EncriptedApellido;
            misDatos.Apellido = HelpersManagementService.getDesencriptada(miSeg);
            miSeg.Encriptado = EncriptedTipoUsuario;
            misDatos.Cod_Tipo_Usuario = Convert.ToInt32(HelpersManagementService.getDesencriptada(miSeg));
            miSeg.Encriptado = EncriptedID;
            misDatos.ID_Usuario = Convert.ToInt32(HelpersManagementService.getDesencriptada(miSeg));
            miSeg.Encriptado = EncriptedSC;
            misDatos.SaveCredentials = Convert.ToInt32(HelpersManagementService.getDesencriptada(miSeg));
            //miSeg.Encriptado = EncriptedRol;
            //misDatos.Cod_Rol = Convert.ToInt32(HelpersManagementService.getDesencriptada(miSeg));
            miSeg.Encriptado = EncriptedAgencia;
            misDatos.ID_Agencia = Convert.ToInt32(HelpersManagementService.getDesencriptada(miSeg));
            return misDatos;
        }
    }
}