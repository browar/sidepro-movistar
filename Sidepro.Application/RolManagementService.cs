﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Business;
using Sidepro.Entities;
namespace Sidepro.Application
{
    public class RolManagementService
    {
        RolBusiness rolBusiness = new RolBusiness();
        public List<Rol> getRoles()
        {
            return rolBusiness.getRoles();
        }
        public string agregarRol(int cod_canal, string rol)
        {
            return rolBusiness.agregarRol(cod_canal, rol);
        }
        public string modificarRol(int cod_canal, int id_rol, string rol)
        {
            return rolBusiness.modificarRol(cod_canal, id_rol, rol);
        }

        public List<ModuloCursos> getRolModulos(int id_rol)
        {
            return rolBusiness.getRolModulos(id_rol);
        }
        public List<Curso> getRolCursos(int id_rol, int id_modulo, int? id_curso)
        {
            return rolBusiness.getRolCursos(id_rol, id_modulo, id_curso);
        }
        public List<ModuloCursos> getModulos(int id_rol)
        {
            return rolBusiness.getModulos(id_rol);
        }
        public List<ModuloCursos> getModulosDestacados(int cod_rol)
        {
            return rolBusiness.getModulosDestacados(cod_rol);
        }        
        public string agregarRolModulo(int id_rol, int id_modulo)
        {
            return rolBusiness.agregarRolModulo(id_rol, id_modulo);
        }
        public string eliminarRolModulo(int id_modulo, int id_rol)
        {
            return rolBusiness.eliminarRolModulo(id_modulo, id_rol);
        }
        public string getNombrePadre(int id_rol, int id_modulo, int? id_curso)
        {
            return rolBusiness.getNombrePadre(id_rol, id_modulo, id_curso);
        }
        public List<Curso> getCursos(int id_rol, int id_modulo, int? id_curso)
        {
            return rolBusiness.getCursos(id_rol, id_modulo, id_curso);
        }
        public List<Curso> getCursosDestacados(int cod_rol, int id_modulo, int? id_curso)
        {
            return rolBusiness.getCursosDestacados(cod_rol, id_modulo, id_curso); 
        }
        
        public string agregarRolCurso(int id_rol, int id_modulo, int id_curso, int? parent)
        {
            return rolBusiness.agregarRolCurso(id_rol, id_modulo, id_curso, parent);
        }
        public string eliminarRolCurso(int id_modulo, int id_curso, int id_rol)
        {
            return rolBusiness.eliminarRolCurso(id_modulo, id_curso, id_rol);
        }
        public List<Destacado> getDestacados()
        {
            return rolBusiness.getDestacados();
        }
        public List<TipoDestacado> getTipoDestacados()
        {
            return rolBusiness.getTipoDestacados();
        }
        public string AgregarDestacado(int id_modulo, int? id_curso, int cod_rol, int cod_tipo_destacado, string url_imagen)
        {
            return rolBusiness.AgregarDestacado(id_modulo, id_curso, cod_rol, cod_tipo_destacado, url_imagen);
        }
        public string EliminarDestacado(int id_destacado) {
            return rolBusiness.EliminarDestacado(id_destacado);
        }
        public string ModificarDestacado(int id_destacado, int cod_tipo_destacado, string url_imagen, int orden){
            return rolBusiness.ModificarDestacado(id_destacado, cod_tipo_destacado, url_imagen, orden);
        }
        public List<Rol> getRolesDestacados()
        {
            return rolBusiness.getRolesDestacados();
        }
        

    }
}