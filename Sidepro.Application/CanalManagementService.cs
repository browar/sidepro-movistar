﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Business;
namespace Sidepro.Application
{
    public class CanalManagementService
    {
        CanalBusiness canalBusiness = new CanalBusiness();
        public List<Canal> getCanales()
        {
            return canalBusiness.getCanales();
        }
        public string agregarCanal(string Descripcion)
        {
            return canalBusiness.agregarCanal(Descripcion);
        }
        public string modificarCanal(int cod_canal, string Descripcion)
        {
            return canalBusiness.modificarCanal(cod_canal, Descripcion);
        }
    }
}