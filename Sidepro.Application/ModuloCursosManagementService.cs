﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Business;
namespace Sidepro.Application
{
    public class ModuloCursosManagementService
    {
        private ModuloCursosBusiness modulosBusiness = new ModuloCursosBusiness();

        public List<ModuloCursos> getGrupoCursos()
        {
            return modulosBusiness.getGrupos();
        }
        public List<ModuloCursos> getGrupoMenu(int id_usuario)
        {

            return modulosBusiness.getGrupoMenu(id_usuario);
        }
        public List<ModuloCursos> getModulos()
        {
            return modulosBusiness.getModulos();
        }
        public string agregarModulo(string Nombre, string Controller, string View, string Style, string Tags) {
            return modulosBusiness.agregarModulo(Nombre, Controller, View, Style, Tags);
        }
        public string modificarModulo(int ID_Modulo, string Nombre, string Controller, string View, string Style,string Tags)
        {
            return modulosBusiness.modificarModulo(ID_Modulo, Nombre, Controller, View, Style, Tags);
        }
        public string inhabilitarModulo(int ID_Modulo)
        {
            return modulosBusiness.inhabilitarModulo(ID_Modulo);
        }
        public string habilitarModulo(int ID_Modulo)
        {
            return modulosBusiness.habilitarModulo(ID_Modulo);
        }        
    }
}