﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Business;
namespace Sidepro.Application
{
    public class LeccionManagementService
    {
        private LeccionBusiness leccionBusiness = new LeccionBusiness();
        public List<Leccion> getLeccionesDeCurso(int id_modulo, int id_curso, int id_usuario)
        {
            return leccionBusiness.getLeccionesDeCurso( id_modulo, id_curso, id_usuario);
        }
        public Leccion getLeccion (int id_modulo, int id_curso, int id_leccion){
            return leccionBusiness.getLeccion( id_modulo, id_curso, id_leccion);
        }
        public Quiz getQuiz(int id_modulo, int id_curso, int id_leccion, int id_usuario)
        {
            return leccionBusiness.getQuiz(id_modulo, id_curso, id_leccion, id_usuario);
        }
        public Quiz getQuizCertificaciones(int id_usuario)
        {
            return leccionBusiness.getQuizCertificaciones(id_usuario);
        }
        public void grabarRespuesta(int id_usuario, int question, int answer){
            leccionBusiness.grabarRespuesta(id_usuario, question, answer);
        }
        public bool grabarResultado(int id_modulo, int id_curso, int id_leccion, decimal nota, int id_usuario)
        {
            return leccionBusiness.grabarResultado(id_modulo, id_curso, id_leccion, nota, id_usuario);
        }
        public Certification getResultado(int id_modulo, int id_curso, int id_leccion, int id_usuario)
        {
            return leccionBusiness.getResultado(id_modulo, id_curso, id_leccion, id_usuario);
        }
        public List<Leccion> getLecciones(int id_modulo, int id_curso)
        {
            return leccionBusiness.getLecciones(id_modulo, id_curso);
        }
        public string AgregarLeccion(int Id_modulo, int Id_curso, string Nombre, string Descripcion, string Imagen, string Video, decimal Pje_aprobacion)
        {
            return leccionBusiness.AgregarLeccion(Id_modulo, Id_curso, Nombre, Descripcion, Imagen, Video, Pje_aprobacion);
        }
        public string ModificarLeccion(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Descripcion, string Imagen, string Video, decimal Pje_aprobacion, int? Orden)
        {
            return leccionBusiness.ModificarLeccion(Id_modulo, Id_curso, Id_leccion, Nombre, Descripcion, Imagen, Video, Pje_aprobacion, Orden);
        }
        public string InhabilitarLeccion(int ID_Modulo, int Id_curso, int Id_leccion)
        {
             return leccionBusiness.InhabilitarLeccion(ID_Modulo, Id_curso, Id_leccion);
        }
        public string HabilitarLeccion(int ID_Modulo, int Id_curso, int Id_leccion)
        {
            return leccionBusiness.HabilitarLeccion(ID_Modulo, Id_curso, Id_leccion);
        }
        public List<Pregunta> GetPregunta(int id_modulo, int id_curso, int Id_leccion)
        {
            return leccionBusiness.GetPregunta(id_modulo, id_curso, Id_leccion);
        }
        public string InhabilitarPregunta(int ID_Modulo, int Id_curso, int Id_leccion, int id_pregunta)
        {
            return leccionBusiness.InhabilitarPregunta(ID_Modulo, Id_curso, Id_leccion,  id_pregunta);
        }
        public string HabilitarPregunta(int ID_Modulo, int Id_curso, int Id_leccion, int id_pregunta)
        {
            return leccionBusiness.HabilitarPregunta(ID_Modulo, Id_curso, Id_leccion,  id_pregunta);
        }
        public string AgregarPregunta(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Correcto, string Incorrecto)
        {
            return leccionBusiness.AgregarPregunta(Id_modulo, Id_curso, Id_leccion, Nombre, Correcto, Incorrecto);
        }
        public string ModificarPregunta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, string Correcto, string Incorrecto, decimal Cant_puntos)
        {
            return leccionBusiness.ModificarPregunta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Correcto, Incorrecto, Cant_puntos);
        }

        public List<Respuesta> GetRespuesta(int id_modulo, int id_curso, int Id_leccion, int id_pregunta)
        {
            return leccionBusiness.GetRespuesta(id_modulo, id_curso, Id_leccion, id_pregunta);
        }
        public string AgregarRespuesta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, int Sn_correcto)
        {
            return leccionBusiness.AgregarRespuesta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Sn_correcto);
        }
        public string ModificarRespuesta(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta, string Nombre, int Sn_correcto)
        {
            return leccionBusiness.ModificarRespuesta(Id_modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta, Nombre, Sn_correcto);
        }

        public string InhabilitarRespuesta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta)
        {
            return leccionBusiness.InhabilitarRespuesta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta);
        }
        public string HabilitarRespuesta(int ID_Modulo, int Id_curso, int Id_leccion, int Id_pregunta, int id_respuesta)
        {
            return leccionBusiness.HabilitarRespuesta(ID_Modulo, Id_curso, Id_leccion, Id_pregunta, id_respuesta);
        }
        public string AgregarPreguntaCertificacion(int Id_modulo, int Id_curso, int Id_leccion, string Nombre, string Correcto, string Incorrecto, int categoria, int moduloCert, int cursoCert, int leccionCert)
        {
            return leccionBusiness.AgregarPreguntaCertificacion(Id_modulo, Id_curso, Id_leccion, Nombre, Correcto, Incorrecto, categoria,moduloCert, cursoCert, leccionCert);
        }
        public string ModificarPreguntaCertificacion(int Id_modulo, int Id_curso, int Id_leccion, int Id_pregunta, string Nombre, string Correcto, string Incorrecto, decimal Cant_puntos, int categoria, int moduloCert, int cursoCert, int leccionCert)
        {
            return leccionBusiness.ModificarPreguntaCertificacion(Id_modulo, Id_curso, Id_leccion, Id_pregunta, Nombre, Correcto, Incorrecto, Cant_puntos, categoria,moduloCert, cursoCert, leccionCert);
        }
    }
}