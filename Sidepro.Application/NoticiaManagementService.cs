﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Business;
using Sidepro.Entities;
namespace Sidepro.Application
{
    public class NoticiaManagementService
    {
        public Noticia GetNoticia(int id_modulo, int id_curso, int cod_usuario)
        {
            return new NoticiaBusiness().GetNoticia(id_modulo, id_curso, cod_usuario);
        }
    }
}