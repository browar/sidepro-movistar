﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Business;
namespace Sidepro.Application
{
    public class AgenciaManagementService
    {
        AgenciaBusiness agenciaBusiness = new AgenciaBusiness();
        public List<Agencia> getAgencias()
        {
            return agenciaBusiness.getAgencias();
        }
        public List<TipoAgencia> getTipoAgencias()
        {
            return agenciaBusiness.getTipoAgencias();
        }
        public string agregarAgencia(string Agencia, string Documento, string Email, int TipoAgencia) 
        {
            return agenciaBusiness.agregarAgencia(Agencia, Documento, Email, TipoAgencia);
        }
        public string modificarAgencia(int id_agencia, string Agencia, string Documento, string Email, int TipoAgencia)
        {
            return agenciaBusiness.modificarAgencia(id_agencia, Agencia, Documento, Email, TipoAgencia);
        }
    }
}