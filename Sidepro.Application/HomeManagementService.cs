﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Business;
using Sidepro.Entities;
namespace Sidepro.Application
{
  
    public class HomeManagementService
    {
        HomeBusiness homeBusiness = new HomeBusiness();
        public Usuario getUser(string user, string pass)
        {
            return homeBusiness.getUser(user, pass);
        }
        public List<Certification> getCertificaciones(int id_usuario) {
            return homeBusiness.getCertificaciones(id_usuario);
        }
        public List<Destacado> getDestacados(int id_usuario)
        {
            return homeBusiness.getDestacados(id_usuario);
        }
    }
}