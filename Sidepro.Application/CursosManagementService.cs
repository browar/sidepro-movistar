﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Business;
using Sidepro.Entities;
namespace Sidepro.Application
{
    public class CursosManagementService
    {
        private CursosBusiness cursosBusiness = new CursosBusiness();

        public List<Curso> getCursosHistorial(int id_usuario)
        {
            return cursosBusiness.getCursosHistorial(id_usuario);
        }
        public List<Curso> getCursosDelModulo(int id_modulo, int? id_curso, int id_usuario)
        {
            return cursosBusiness.getCursosDelModulo(id_modulo, id_curso, id_usuario);
        }
        public CourseProgress getProgreso(int id_usuario)
        {
            return cursosBusiness.getProgreso(id_usuario);
        }
        public List<Curso> getCursos(int id_modulo, int? id_curso)
        {
            return cursosBusiness.getCursos(id_modulo,  id_curso);
        }
        public string InhabilitarCurso(int id_modulo, int id_curso)
        {
            return cursosBusiness.InhabilitarCurso(id_modulo, id_curso);
        }
        public string HabilitarCurso(int id_modulo, int id_curso)
        {
            return cursosBusiness.HabilitarCurso(id_modulo, id_curso);
        }
        public string agregarCurso(int id_modulo, string Nombre, string Descripcion, string Controller, string View, string Style, string Imagen, int? parent)
        {
            return cursosBusiness.agregarCurso(id_modulo, Nombre, Descripcion, Controller, View, Style, Imagen, parent);
        }
        
        public string modificarCurso(int ID_Modulo, int Id_curso, string Nombre, string Descripcion, string Controller, string View, string Style, string Imagen)
        {
            return cursosBusiness.modificarCurso(ID_Modulo, Id_curso, Nombre, Descripcion, Controller, View, Style, Imagen);
        }
    }

}