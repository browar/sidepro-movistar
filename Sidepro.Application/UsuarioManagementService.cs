﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sidepro.Entities;
using Sidepro.Business;
namespace Sidepro.Application
{
    public class UsuarioManagementService
    {
        public MailUsuario DesbloquearUsuario(int cod_usuario){
            return new UsuarioBusiness().DesbloquearUsuario(cod_usuario);
        }
        public string getEmail(int cod_usuario){
            return new UsuarioBusiness().getEmail(cod_usuario);
        }

    }
}